import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global';
import { RestProvider } from '../../providers/rest';
import { Subscription } from 'rxjs/Subscription';
import { NotificationDetailPage } from './_detail/detail';

@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  criteria:any = {
    show : this.global.config("showPerPage"),
    order_by: 'created_at',
    type_sort: 'DESC',
    page: 1,
  }

  page = 1;
  
  list_data:any = new Array();

  dataUser:any = (window.localStorage.getItem("profile_data") !== undefined && window.localStorage.getItem("profile_data") !== null) ? JSON.parse(window.localStorage.getItem("profile_data")) : {};
  loginEmitter:Subscription;
  retrySubs:any;
  loadMore:boolean=true;
  spinner:boolean = false;
  errorGetNotif:boolean = false;


  constructor(
    public navCtrl      : NavController, 
    public navParams    : NavParams,
    public viewCtrl     : ViewController,
    public global       : GlobalProvider,
    public rest         : RestProvider,
    public modalCtrl: ModalController,

  ) {
    this.getData();

    this.global.checkLogin()
    .then(s => {
      if (s == true) 
      {
        this.getData();
      }
    })
  }

  ionViewDidEnter()
  {
    this.retrySubs = this.global.retryEmitter.subscribe(data => {
      if (data == 'notification') 
      {
        this.getData();
      }
    })
  
    this.loginEmitter = this.global.loginEmitter.subscribe(s => {
      if (s == true) 
      {
        this.global.isLogin = true;
        setTimeout(() => {
          this.dataUser = JSON.parse(window.localStorage.getItem("profile_data")); 
        }, 3000);
        
        this.getData();  
      }
    })
  }

  ionViewWillLeave()
  {
    this.retrySubs.unsubscribe();
    this.loginEmitter.unsubscribe()
  }

  doRefresh(refresher)
  {
    this.criteria.page = this.page = 1;
    this.getData(refresher);
  }

  getData(refresher=undefined)
  {
    this.errorGetNotif = false;
    this.spinner = true;
    this.rest.getNotification(this.criteria)
    .then(result => {
      this.spinner = false;
      if (refresher !== undefined) 
      {
        refresher.complete();  
      }
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.criteria.page = this.page = result['data']['next_page']
        this.list_data = result['data']['data'];

        if(result['data']['data'].length < this.criteria.show)
        {
          this.loadMore = false;
        }
        else
        {
          this.loadMore = true;
        }
        
      }
      else if(typeof result['status'] !== undefined && result['status'] == 401)
      {
        window.localStorage.clear();
        this.global.isLogin = false;
      }
      else
      {
        this.errorGetNotif = true;
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
  }

  doLoadMore(infiniteScroll, page)
  {
    this.criteria.page = page
    this.rest.getNotification(this.criteria)
    .then(result => {
      infiniteScroll.complete();
      this.criteria.page = result['data']['next_page']
      for( let content of result['data']['data'])
      {
        this.list_data.push(content);
      }
      if(result['data']['data'].length < this.criteria.show)
      {
        this.loadMore = false;
      }
      else
      {
        this.loadMore = true;
      }
    })
  }

  detail(uuid)
  {
    let modal = this.modalCtrl.create(NotificationDetailPage, {data_uuid: uuid});
    modal.present();
    modal.onDidDismiss(data => {

    });

    this.rest.updateNotificationRead(uuid).then(() => console.log("success update read notif")).catch(()=> console.log("error update read notif"))
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

}
