import { Component, ViewChild, Output, EventEmitter } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { ActivityPendingListPage } from './_tab/pending';
import { ActivityCompleteListPage } from './_tab/complete';
import { ActivityOnProgressListPage } from './_tab/onprogress';
import { ActivityCanceledListPage } from './_tab/canceled';
import { SuperTabs } from 'ionic2-super-tabs';
import { RestProvider } from '../../providers/rest';
import { GlobalProvider } from '../../providers/global';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'page-activity',
  templateUrl: 'activity.html',
})
export class ActivityPage {

  pages = [
    { pageName: 'ActivityPendingListPage', title: "Tertunda"},
    { pageName: 'ActivityOnProgressListPage', title: "Diproses"},
    { pageName: 'ActivityCompleteListPage', title: "Selesai"},
    { pageName: 'ActivityCanceledListPage', title: "Dibatalkan"},
  ];
  @ViewChild(SuperTabs) superTabs: SuperTabs;

  loginEmitter:Subscription;

  @Output() refreshTab: EventEmitter<any> = new EventEmitter();


  constructor(
    public navCtrl    : NavController, 
    public navParams  : NavParams,
    public rest       : RestProvider,
    public modalCtrl  : ModalController,
    public global     : GlobalProvider,
  ) {
  }
  
  ionViewDidEnter()
  {
    this.global.currentComponent = 'activity';
    this.global.checkLogin()
    .then(s => {
      if (s == true) 
      {
        this.refreshTab.emit(true);
      }
    })
    
    this.loginEmitter = this.global.loginEmitter.subscribe(s => {
      if (s == true) 
      {
        this.global.isLogin = true;
        this.refreshTab.emit(true);
      }
    })

  }

  detail()
  {

  }


}
