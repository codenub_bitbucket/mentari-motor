import { Component } from '@angular/core';
import { AlertController, NavController, NavParams, ViewController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest';
import { GlobalProvider } from '../../providers/global';

@Component({
  selector: 'page-extra',
  templateUrl: 's_autocomplete.html',
})
export class SAutoCompletePage {

  fieldsName:string="";
  fieldsLabel:string="";
  module:string="";

  list_dropdown:any = [];

  criteria:any = {
    show : this.global.config("showPerPage"),
    order_by: 'created_at',
    type_sort: 'DESC',
    page: 1,
  }

  loadMore:boolean=true;
  spinner:boolean=false;
  searchSpinner:boolean = false;

  /* IDs */
  brandId:number = 0;

  /* Optional Label */
  brandName:string="";

  keyword:string = "";

  constructor(
    public navCtrl:       NavController, 
    public navParams:     NavParams,
    public viewCtrl: ViewController,
    public rest : RestProvider,
    public global : GlobalProvider,
    public alertCtrl : AlertController,
  ) {
    this.fieldsName = navParams.get("fields_name");
    this.fieldsLabel = navParams.get("fields_label");
    this.module = navParams.get("module");
    this.brandName = navParams.get("brand_name");
    this.brandId = navParams.get("brand_id");
    this.setGetData();
  }

  dismiss(value:any='', label:any='')
  {
    if (this.fieldsName == 'str_id' && value !== 1) 
    {
      return this.alertCtrl.create({
        message : "Bengkel ini belum menyediakan servis silahkan hubungi langsung melalui menu Kontak.",
        buttons : [
          {
            text : "Oke",
            role : "cancel",
          }
        ]
      }).present();
    }

    if (this.fieldsName == 'crs_model_id') 
    {
      let md = (label.crs_model_name!==null) ? label.crs_model_name : '';
      let cc = (label.crs_model_cc) ? label.crs_model_cc : '';
      let tr = (label.crs_model_transmission) ? label.crs_model_transmission : '';
      let ty = (label.crs_model_type) ? label.crs_model_type : '';
      label = md+' '+cc+' '+tr+' '+ty;
    }
    let data = { value: value, label: label};
    this.viewCtrl.dismiss(data);
  }

  setGetData()
  {
    if (this.fieldsName !== '') 
    {
      if (this.fieldsName == 'crs_model_id') 
      {
        this.criteria.brand_id = this.brandId;
      }
      this.getDropdown();  
    }
    
  }

  getDropdown(refresher:any=undefined)
  {
    this.spinner = true;
    let filter = "";
    if (this.fieldsName == 'crs_model_id') 
    {
      filter = `&filter[crs_brand_id]=${this.criteria.brand_id}`;
    }

    let search = ''
    if (this.keyword !== '') 
    {
      search = `&search[${this.module}_name]=${this.keyword}`;
    }

    if(this.fieldsName == 'str_id')
    {
      this.criteria.type_sort = 'ASC';
    }
    
    this.rest.getDropdown(this.fieldsName, this.criteria, filter, search)
    .then(result => {
      this.spinner = false;
      this.searchSpinner = false;
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.criteria.page = result['data']['next_page']
        this.list_dropdown = result['data']['data'];
        if(result['data']['data'].length < this.criteria.show)
        {
          // Disable load more
          this.loadMore = false;
        }
        else
        {
          // Enable load more
          this.loadMore = true;
        }
        
        if (refresher !== undefined) 
        {
          refresher.complete();  
        }
      }
    })
  }

  doLoadMore(infiniteScroll)
  {
    let filter = "";
    if (this.fieldsName == 'crs_model_id') 
    {
      filter = `&filter[crs_brand_id]=${this.criteria.brand_id}`;
    }

    let search = ''
    if (this.keyword !== '') 
    {
      search = `&search[${this.module}_name]=${this.keyword}`;
    }
    this.rest.getDropdown(this.fieldsName, this.criteria, filter, search)
    .then(result => {
      infiniteScroll.complete();
      this.criteria.page = result['data']['next_page']
      for( let content of result['data']['data'])
      {
        this.list_dropdown.push(content);
      }
      if(result['data']['data'].length < this.criteria.show)
      {
        this.loadMore = false;
      }
      else
      {
        this.loadMore = true;
      }
    })
  }

  doRefresh(refresher)
  {
    this.criteria.page = 1;
    this.getDropdown(refresher);
  }

  onInputSearch()
  {
    this.searchSpinner = true;
    setTimeout(() => {
      this.searchSpinner = false;
    }, 1000);
  }

  search()
  {
    if (this.keyword.length >= 1) 
    {
      setTimeout(() => {
        this.criteria.page = 1;
        this.getDropdown();
      }, 350);
    }
    else if (this.keyword.length == 0) 
    {
      this.criteria.page = 1;
      this.getDropdown();
    }
  }

}
