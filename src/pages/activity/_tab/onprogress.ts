import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { ActivityDetailPage } from '../_detail/detail';
import { Subscription } from 'rxjs/Subscription';
import { RestProvider } from '../../../providers/rest';
import { GlobalProvider } from '../../../providers/global';
import { ActivityPage } from '../activity';
import { OrderDetailPage } from '../../extra/order-detail';

@Component({
  selector: 'page-activity',
  templateUrl: 'onprogress.html',
})
export class ActivityOnProgressListPage {

  tab:string="Dalam Proses";

  criteria:any = {
    show : this.global.config("showPerPage"),
    order_by: 'created_at',
    type_sort: 'DESC',
    page: 1,
  }
  status = 2;

  list_data:any = new Array();

  loginEmitter:Subscription;
  retrySubs:any;

  spinner:boolean = false;
  errorGetActivity:boolean = false;

  loadMore:boolean=true;

  users_id : any = JSON.parse(window.localStorage.getItem('users_id'));
  orderReciever:any;

  constructor(
    public navCtrl:       NavController, 
    public navParams:     NavParams,
    public modalCtrl: ModalController,
    public rest       : RestProvider,
    public global     : GlobalProvider,
    public activityPage : ActivityPage,
  ) {
  
  }

  ionViewDidEnter()
  {
    this.getTransaction();
    this.retrySubs = this.global.retryEmitter.subscribe(data => {
      if (data == 'onprogress_trx') 
      {
        this.getTransaction();
      }
    })

    this.loginEmitter = this.global.loginEmitter.subscribe(s => {
      if (s == true) 
      {
        this.getTransaction();
      }
    })

    this.orderReciever = this.global.orderReciever.subscribe(res => {
      let to_user = res.to_user;

      if (to_user == this.users_id) 
      {
        this.getTransaction();
      }
    }) 
  }
  
  ionViewWillLeave()
  {
    try {
      this.orderReciever.unsubscribe();
      this.retrySubs.unsubscribe();
    } catch (error) {
      console.log(error);
    }
  }

  doRefresh(refresher)
  {
    this.criteria.page = 1;
    this.getTransaction(refresher);
  }

  getTransaction(refresher:any=undefined)
  {
    this.errorGetActivity = false;
    this.spinner = true;
    this.rest.transaction(this.criteria, this.status)
    .then(result => {
      this.spinner = false;
      if (refresher !== undefined) 
      {
        refresher.complete();  
      }
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.list_data = result['data']['services_transaction']['data'];
      }
      else if(typeof result['status'] !== undefined && result['status'] == 401)
      {
        window.localStorage.clear();
        this.global.isLogin = false;
      }
      else
      {
        this.errorGetActivity = true;
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
  }

  doLoadMore(infiniteScroll)
  {
    this.rest.transaction(this.criteria, this.status)
    .then(result => {
      infiniteScroll.complete();
      this.criteria.page = result['data']['services_transaction']['next_page'];
      for( let content of result['data']['services_transaction']['data'])
      {
        this.list_data.push(content);
      }
      if(result['data']['services_transaction']['data'].length < this.criteria.show)
      {
        this.loadMore = false;
      }
      else
      {
        this.loadMore = true;
      }
    })
  }

  detail(data_uuid)
  {
    let myModal = this.modalCtrl.create(OrderDetailPage, {data_uuid : data_uuid});

    myModal.present();

    myModal.onDidDismiss(data => {
      this.getTransaction();
    });
  }

}
