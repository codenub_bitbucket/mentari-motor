import { Component, Input } from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapOptions,
  GoogleMapsEvent,
  MarkerOptions,
  Marker,
  CameraPosition,
  ILatLng,
} from '@ionic-native/google-maps';
import { ModalController, Platform } from 'ionic-angular';
import { RestProvider } from '../../providers/rest';
import { GlobalProvider } from '../../providers/global';

@Component({
  selector: 'mapsview',
  templateUrl: 'mapsview.html'
})
export class MapsviewComponent {

  map: GoogleMap;

  @Input('list_data') list_data:any;

  onTriggerMapsStore:any;

  constructor(
    public rest : RestProvider,
    public modalCtrl: ModalController,
    public global: GlobalProvider,
    public platform : Platform
  ) {
    if (this.platform.is("cordova")) 
    {
      this.loadMap();

      this.onTriggerMapsStore = this.global.onTriggerMapsStore.subscribe((data) => {
        this.moveCamera(data);
      })
    }
  }

  ngOnDestroy()
  {
    console.log('leave');
    
    this.onTriggerMapsStore.unsubscribe();
  }

  moveCamera(data)
  {
    console.log(data);
    
    this.map.clear();
    let cameraPosition:CameraPosition<ILatLng[]> = {
      target: [
        {
          lat: data.lat,
          lng: data.lng,
        }
      ],
    };

    this.map.moveCamera(cameraPosition);

    setTimeout(() => {
      let options:MarkerOptions = {
        title: data.str_name,
        position: {
          lat: data.lat,
          lng: data.lng,
        }
      }
      this.map.addMarker(options).then((marker: Marker) => {

        marker.showInfoWindow();
      
      }); 
    }, 1000);
  }

  loadMap()
  {
    this.platform.ready()
    .then(() => {
      let mapOptions: GoogleMapOptions = {
        camera: {
           target: {
             lat: -6.2710415,
             lng: 106.8307131,
           },
           zoom : 15
         },
         controls: {
          'compass': true,
          'myLocationButton': true,
          'myLocation': true,   // (blue dot)
          'indoorPicker': true,
          'zoom': true,          // android only
          'mapToolbar': true     // android only
        },
      };
  
      this.map = GoogleMaps.create('map_canvas', mapOptions);

      this.map.on(GoogleMapsEvent.MAP_READY).subscribe((result) =>
      {
        console.log('ready');
        let cameraPosition:CameraPosition<ILatLng[]> = {
          target: [],
        };
        cameraPosition.target = [];
        setTimeout(() => {
          this.list_data.forEach(item => {

            console.log(item);
            
            var mapUrl = item.str_address_map;
            if (mapUrl !== undefined && mapUrl !== '' && mapUrl !== null)
            {
              var url = mapUrl.split('@');
              var at = url[1].split('z');
              var zero = at[0].split(',');
              var lat = zero[0];
              var lng = zero[1];
              
              let options:MarkerOptions = {
                title: item.str_name,
                position: {
                  lat: lat,
                  lng: lng,
                }
              }
              this.map.addMarker(options).then((marker: Marker) => {
      
                marker.showInfoWindow();
              
              });  
            }
          }); 
        }, 3000);

        cameraPosition.target.push({
          lat: -6.2710415,
          lng: 106.8307131,
        });
        this.map.moveCamera(cameraPosition);
        
      });
    })
  }

}
