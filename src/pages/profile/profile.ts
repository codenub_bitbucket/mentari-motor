import { Component, ViewChild, EventEmitter, Output } from '@angular/core';
import { NavController, NavParams, ModalController, Slides, AlertController, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest';
import { VehicleAddPage } from '../extra/vehicle-add';
import { GlobalProvider } from '../../providers/global';
import { Subscription } from 'rxjs/Subscription';
import { DetailCarsPage } from './_cars/detail_cars';
import { ProfileEditPasswordPage } from './_edit/edit_password';
import { Socket } from 'ng-socket-io';
import { ActionSheetOptions, ActionSheet } from '@ionic-native/action-sheet';
import { AttachmentPage } from '../extra/attachments';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { OrderDetailPage } from '../extra/order-detail';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  criteria:any = {
    show : this.global.config("showPerPage"),
    order_by: 'created_at',
    type_sort: 'DESC',
    page: 1,
  }

  @ViewChild(Slides) slides: Slides;
  prev:boolean = false;
  next:boolean = true;
  
  myProfile:any = new Array();
  myVehicle:any = new Array();
  loginEmitter:Subscription;
  profileEmitter:Subscription;
  imageCarsUpdated:Subscription;
  imageProfileUpdated:Subscription;

  spinnerVehicle:boolean=false;
  spinnerProfile:boolean=false;
  spinnerLastTrx:boolean=false;

  lastTransaction:any = new Array();

  errorRequestProfile:boolean=false;
  errorRequestVehicle:boolean=false;
  errorRequestLastTrx:boolean=false;

  loadingImage:boolean = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public rest : RestProvider,
    public modalCtrl: ModalController,
    public global: GlobalProvider,
    public socket: Socket,
    public actionSheet: ActionSheet,
    public photoViewer: PhotoViewer,
    public alertCtrl : AlertController,
    public loadingCtrl : LoadingController,
  ) {
  }
  
  ionViewDidEnter()
  {
    this.global.currentComponent = 'profile';
    this.imageProfileUpdated = this.global.imageUpdated.subscribe(r => {
      if(r['uuid'] == this.myProfile.usr_uuid)
      {
        this.myProfile.usr_photo = r['url'];
      }
    })
    
    this.global.checkLogin()
    .then(s => {
      if (s == true) 
      {
        this.getMyProfile();
        this.getMyVehicle();
        this.getLastTransaction();

        this.global.retryEmitter.subscribe(data => {
          if (data == 'profile') 
          {
            this.getMyProfile();  
          }
          else if(data == 'vehicle')
          {
            this.getMyVehicle()
          }
          else if(data == 'transaction')
          {
            this.getLastTransaction()
          }
        })
      }
    })
    
    this.loginEmitter = this.global.loginEmitter.subscribe(s => {
      if (s == true) 
      {
        this.global.isLogin = true;
        this.getMyProfile();
        this.getMyVehicle();  
        this.getLastTransaction();
      }
    })

    this.profileEmitter = this.global.profileEmitter.subscribe(r => {
      if (r == true) 
      {
        this.getMyProfile();
      }
    })

    this.imageCarsUpdated = this.global.imageUpdated.subscribe(r => {
      let find = this.myVehicle.findIndex(re => re['cst_crs_uuid'] == r['uuid']);

      if (find >= 0) 
      {
        this.myVehicle[find]['cst_crs_photo'] = r['url'];  
      }
    })
  }

  doRefresh(refresher)
  {
    this.getMyProfile();
    this.getMyVehicle();  
    this.getLastTransaction();

    setTimeout(() => {
      refresher.complete()
    }, 1000);
  }

  ionViewWillLeave()
  {
    this.loginEmitter.unsubscribe();
    this.profileEmitter.unsubscribe();
  }

  getMyVehicle()
  {
    this.errorRequestVehicle = false;
    this.spinnerVehicle = true;
    this.criteria.type_sort = "ASC"; 
    let content = this.criteria;
    content.show = 15;
    this.rest.getVehicle(content)
    .then(result => {
    this.spinnerVehicle = false;
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        window.localStorage.setItem("fields_vehicle", JSON.stringify(result['data']['fields']))
        this.myVehicle = result['data']['customers_cars']['data']
      }
      else
      {
        // this.global.toastDefault(this.global.config("errorMsg"));
        this.errorRequestVehicle = true;
      }
    })
  }

  addVehicle()
  {
    let modal = this.modalCtrl.create(VehicleAddPage);
    modal.present();
    modal.onDidDismiss(data => {
      if (data !== undefined && data == 'saved') 
      {
        this.getMyVehicle();
      }
    });
  }

  logout()
  {
    this.alertCtrl.create({
      title : "Yakin ingin keluar?",
      message : "Anda perlu login kembali jika ingin melakukan order atau melihat pesanan sebelumnya.",
      buttons : [
        {
          text : "Batal",
          role : "cancel",
        },
        {
          text : "Keluar",
          handler : () => {
            let loading = this.loadingCtrl.create({
              spinner: "crescent",
              content : "Sedang mencoba keluar...",
              enableBackdropDismiss : false,
            });

            loading.present();
            
            this.rest.logout()
            .then(result => {
              loading.dismiss();
              if(typeof result['status'] !== undefined && result['status'] == 200)
              {
                window.localStorage.clear();
                this.global.checkLogin();
                this.socket.disconnect();
              }
              else
              {
                this.global.toastDanger("Gagal keluar.Silahkan coba lagi!")
              }
            })
          }
        }
      ]
    }).present();
  }

  slidePrev()
  {
    this.slides.slidePrev(1000);
    if (this.slides.isBeginning())
    {
      this.prev = false;
    }
    else
    {
      this.prev = true;
    }
  }

  slideNext()
  {
    this.slides.slideNext(1000);
    if (this.slides.isEnd())
    {
      this.next = false;
    }
    else
    {
      this.next = true;
    }
  }

  detailCars(data_uuid)
  {
    let modal = this.modalCtrl.create(DetailCarsPage, {data_uuid: data_uuid});
    modal.present();
    modal.onDidDismiss(data => {
      if (data !== undefined) 
      {
        if (data == 'deleted') 
        {
          this.getMyVehicle();  
        }  
      }
    });
  }

  getMyProfile()
  {
    this.errorRequestProfile = false;
    this.spinnerProfile = true;
    this.rest.getProfile()
    .then(result => {      
      this.spinnerProfile = false;
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        window.localStorage.setItem("profile_data", JSON.stringify(result['data']));
        this.myProfile = result['data'];
      }
      else if(typeof result['status'] !== undefined && result['status'] == 401)
      {
        window.localStorage.clear();
        this.global.isLogin = false;
      }
      else
      {
        // this.global.toastDefault(this.global.config("errorMsg"));
        this.errorRequestProfile = true;
      }
    })
  }

  editPassword()
  {
    let modal = this.modalCtrl.create(ProfileEditPasswordPage);
    modal.present();
    modal.onDidDismiss(data => {

    });
  }

  getLastTransaction()
  {
    this.errorRequestLastTrx = false;
    this.spinnerLastTrx = true;
    let content = this.criteria;
    content.show = 1;
    this.rest.transaction(content)
    .then(result => {
      this.spinnerLastTrx = false;
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.lastTransaction = result['data']['services_transaction']['data'];
        console.log(this.lastTransaction.length);
        
      }
      else
      {
        // this.global.toastDefault(this.global.config("errorMsg"))
        this.errorRequestLastTrx = true;
      }
    })
  }

  addImage()
  {
    let modal = this.modalCtrl.create(AttachmentPage);

    modal.present();

    modal.onDidDismiss(data => {
      if (data !== undefined && data.action == 'submit_image' && data.imageData !== undefined) 
      {
        this.loadingImage = true;
        this.rest.uploadImage(data.imageData)
        .then(res => {
          if (typeof res['status'] !== undefined && res['status'] == 200) 
          {
            let cont = {
              usr_photo :res['file']
            }
            this.rest.updateProfile(cont)
            .then(result => {
              this.loadingImage = false;
              if (typeof result['status'] !== undefined && result['status'] == 200) 
              {
                let data = {
                  url : res['file'],
                  uuid : this.myProfile.usr_uuid
                }
                this.global.updateImageDirect(data);
                this.global.toastDefault("Photo Profil Berhasil Di Upload");
              }
              else
              {
                this.global.toastDefault(this.global.config("errorMsg"))
              }
            })
          }
          else
          {
            this.global.toastDanger("Photo Profil Gagal Di Upload");
          }
        }); 
      }
    })
  }

  viewOption()
  {
    let buttonLabels = ['Ganti Foto', 'Lihat Foto'];

    const options: ActionSheetOptions = {
      buttonLabels: buttonLabels,
      addCancelButtonWithLabel: 'Cancel',
      destructiveButtonLast: true
    };

    this.actionSheet.show(options).then((buttonIndex: number) => {
      if (buttonIndex == 1) 
      {
        this.addImage();  
      }
      else if(buttonIndex == 2)
      {
        let imgName = this.myProfile.usr_photo.substring(this.myProfile.usr_photo.lastIndexOf('/') + 1)
        this.photoViewer.show(this.global.config('urlApi')+this.myProfile.usr_photo, imgName, {share: false});
      }
    });
  }

  detailSrv(uuid)
  {
    let modal = this.modalCtrl.create(OrderDetailPage, {data_uuid : uuid});
    modal.present();
    modal.onDidDismiss(data => {
      this.getLastTransaction();
    });
  }

}
