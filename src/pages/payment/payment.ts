import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { IonicPage, LoadingController, NavController, NavParams, ViewController } from 'ionic-angular';
import { Socket } from 'ng-socket-io';
import { Subscription } from 'rxjs/Subscription';
import { GlobalProvider } from '../../providers/global';
import { RestProvider } from '../../providers/rest';

@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  dataPayment:any = {};
  dataService:any = {};
  resultCreatePayment:any = {};

  page:string = 'choose';

  QRUrl:any;
  showQRImage:boolean = false;

  users_id : any = JSON.parse(window.localStorage.getItem('users_id'));

  bank:string = "";

  orderReciever:any;
  paymentReciever:any;

  serviceBulkList:any= [];
  serviceRegulerList:any= [];
  ProductList:any= [];

  expand:any = {
    detail_service: false,
    tos: true,
    payment_list: true
  }
  
  constructor(
    public navCtrl      : NavController, 
    public navParams    : NavParams,
    public viewCtrl     : ViewController,
    public rest         : RestProvider,
    public global       : GlobalProvider,
    private iab: InAppBrowser,
    public loadingCtrl : LoadingController,
    public socket              : Socket,

  ) {
    this.dataPayment  = this.navParams.get("data");
    this.dataService     = this.navParams.get("data_service");

    if (this.dataService.srv_t_r.length > 0) 
    {
      this.dataService.srv_t_r.forEach(item => {
        if (item['srv_t_d_type'] == 1) 
        {
          this.serviceRegulerList.push(item);
        }
        else if (item['srv_t_d_type'] == 2) 
        {
          this.serviceBulkList.push(item);
        }
        else if (item['srv_t_d_type'] == 3) 
        {
          this.ProductList.push(item);
        }
      });  
    }

    this.orderReciever = this.global.orderReciever.subscribe(res => {
      let data = res.data;
      let to_user = res.to_user;

      if (to_user == this.users_id) 
      {
        if (data.srv_t_status == 3 && data.srv_t_uuid == this.dataService.srv_t_uuid) 
        {
          this.global.toastDefault("Pembayaran Telah Diterima");
          this.dismiss();
        }
      }

    }) 

    this.paymentReciever = this.global.emitSuccessPayment.subscribe(uuid => {
      if(uuid.replace('/', '') == this.dataService.srv_t_uuid)
      {
        this.global.toastDefault("Pembayaran Telah Diterima");
        this.dismiss();
      }
    }) 
  }

  ionViewWillLeave()
  {
    try {
      this.paymentReciever.unsubscribe();
      this.orderReciever.unsubscribe();
    } catch (error) {
      console.log(error);
    }
  }

  pay(payment_method: string, bank: string = "")
  {
    let loading = this.loadingCtrl.create({
      content : "Please wait..."
    })
    loading.present();

    if(payment_method == 'bank_transfer')
    {
      this.dataPayment.bank_transfer = {
        bank : bank,
      }
      this.bank = bank;
    }

    if(payment_method == 'echannel')
    {
      this.dataPayment.echannel = {
          "bill_info1" : "Payment For: Chevycare",
          "bill_info2" : "Booking fee"
      }
    }

    this.dataPayment.payment_type = payment_method;
    this.dataPayment.uuid = this.dataService.srv_t_uuid;
    
    this.rest.createPayment(this.dataPayment)
    .then((result:any) => {
      loading.dismissAll();

      if (result['status_code'] !== "201") 
      {
        this.page = 'error';
        return;  
      }
      else
      {
        result.srv_t_id = this.dataService.srv_t_id;
        this.rest.savePayment(result)
        .then(rs => {
          if(payment_method == 'gopay')
          {
            this.page = 'processPayment';
            this.resultCreatePayment = result;
          } 
          else
          {
            this.dismiss();
          } 
        })  
      }
    });
  }

  showQR()
  {
    this.showQRImage = !this.showQRImage;
    if (this.showQRImage) 
    {
      let loading = this.loadingCtrl.create({
        content: "Loading..."
      })
      loading.present();
      let findQRLink = this.resultCreatePayment['actions'].findIndex(r => r['name'] == 'generate-qr-code');
    
      if (findQRLink >= 0) 
      {
        this.QRUrl = this.resultCreatePayment['actions'][findQRLink]['url'];
      }  

      loading.dismiss();
    }
  }

  openGojek()
  {
    let findLink = this.resultCreatePayment['actions'].findIndex(r => r['name'] == 'deeplink-redirect');
    console.log("🚀 ~ file: payment.ts ~ line 164 ~ PaymentPage ~ findLink", findLink)

    console.log("🚀 ~ file: payment.ts ~ line 168 ~ PaymentPage ~ this.resultCreatePayment['actions']", this.resultCreatePayment['actions'])
    if (findLink >= 0) 
    {
      console.log(this.resultCreatePayment['actions'][findLink]['url']);
      
      this.iab.create(this.resultCreatePayment['actions'][findLink]['url'], '_system')
    }
  }

  dismiss() 
  {
    this.viewCtrl.dismiss();
  }

}
