import { Component, Input } from '@angular/core';
import { GlobalProvider } from '../../../providers/global';

@Component({
  selector: 'error-500',
  templateUrl: 'error_500.html'
})
export class Error_500Component {

  @Input('retry_id') retry_id:any;

  constructor(
    public global : GlobalProvider,
  ) {
  }

  retry(retry_id)
  {
    this.global.retryEmitter.emit(retry_id);
  }

}
