import { Component, Input } from '@angular/core';
import { GlobalProvider } from '../../../providers/global';

@Component({
  selector: 'no-network',
  templateUrl: 'no_network.html'
})
export class NoNetworkComponent {

  @Input('retry_id') retry_id:any;

  constructor(
    public global : GlobalProvider,
  ) {
  }

  retry(retry_id)
  {
    this.global.retryEmitter.emit(retry_id);
  }

}
