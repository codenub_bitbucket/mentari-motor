import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, Slides, ModalController, Loading, LoadingController } from 'ionic-angular';
import { OrderDetailPage } from '../extra/order-detail';
import { VehicleAddPage } from '../extra/vehicle-add';
import { GlobalProvider } from '../../providers/global';
import moment from 'moment';
import { Subscription } from 'rxjs/Subscription';
import { Time } from '@angular/common';
import { SAutoCompletePage } from '../extra/s_autocomplete';
import { RestProvider } from '../../providers/rest';
import { MapsPage } from '../extra/maps';
import { Socket } from 'ng-socket-io';


@Component({
  selector: 'page-home-service',
  templateUrl: 'home-service.html',
})
export class HomeServicePage {

  page:string = "order";

  @ViewChild(Slides) slides: Slides;

  prev:boolean = false;
  next:boolean = true;
  changed:boolean = false;
  currentVehicle:any;
  vehicle:any;

  /* FORM */
  type_service:number = null;
  /* END FORM */

  routine_service :boolean = false;
  usual_service :boolean = false;

  myVehicle:any = new Array();

  loginEmitter:Subscription;

  criteria:any = {
    show : this.global.config("showPerPage"),
    order_by: 'created_at',
    type_sort: 'DESC',
    page: 1,
  }

  dropdownServices:any = new Array();
  storeDropdown:any = new Array();
  loadMore:boolean = true;

  selectedServices:any = [];
  containerSelected:any = [];
  estimatePrice:number = 0;
  estimateTime:Time = {
    hours : 0,
    minutes: 0
  }

  data:any = {};
  data_label:any                  = {};

  minDay = moment().utc(true).add(1, 'days').format("YYYY-MM-DD");
  minTime = '08:00';
  maxTime = '15:59';
  hourValues = ['08','09','10','11','12','13','14','15'];

  spinnerVehicle:boolean=false;
  spinnerService:boolean=false;

  btnOrder:boolean=false;

  loading:Loading;

  data_uuid:string = ""; // uuid if finish order 

  dataUser:any = (window.localStorage.getItem("profile_data") !== undefined && window.localStorage.getItem("profile_data") !== null) ? JSON.parse(window.localStorage.getItem("profile_data")) : {};

  takeBy:string = "self";

  imageCarsUpdated:Subscription;

  availableSchedule:any = new Array();
  isDayOff:boolean = false;

  kuotaService:boolean = false;
  isDateOrderPicked:boolean = false;

  isCheckLimit:boolean = false;

  
  keywordRS:string = "";
  keywordUS:string = "";

  isLoadMore:boolean = false;

  constructor(
    public navCtrl:       NavController, 
    public navParams:     NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public global: GlobalProvider,
    public rest : RestProvider,
    public loadingCtrl : LoadingController,
    public socket              : Socket,
  ) {
    this.global.checkLogin()
    .then(s => {
      if (s == true) 
      {
        this.getMyVehicle();
        this.data.srv_t_cat = 2;

        this.data.srv_t_fullname = (this.dataUser.usr_fullname !== null && this.dataUser.usr_fullname !== undefined) ? this.dataUser.usr_fullname : '';
        this.data.srv_t_phone = (this.dataUser.usr_phone !== null && this.dataUser.usr_phone !== undefined) ? this.dataUser.usr_phone : '';
      }
    })
  }

  ionViewDidEnter()
  {
    this.imageCarsUpdated = this.global.imageUpdated.subscribe(r => {
      let find = this.myVehicle.findIndex(re => re['cst_crs_uuid'] == r['uuid']);

      if (find >= 0) 
      {
        this.myVehicle[find]['cst_crs_photo'] = r['url'];  
      }
    })

    this.global.checkLogin()
    .then(s => {
      if (s == true) 
      {
        this.getMyVehicle();
        this.data.srv_t_cat = 2;

        this.data.srv_t_fullname = (this.dataUser.usr_fullname !== null && this.dataUser.usr_fullname !== undefined) ? this.dataUser.usr_fullname : '';
        this.data.srv_t_phone = (this.dataUser.usr_phone !== null && this.dataUser.usr_phone !== undefined) ? this.dataUser.usr_phone : '';
      }
    })
  
    this.loginEmitter = this.global.loginEmitter.subscribe(s => {
      if (s == true) 
      {
        this.global.isLogin = true;
        setTimeout(() => {
          this.dataUser = JSON.parse(window.localStorage.getItem("profile_data")); 
        }, 3000);
        
        this.getMyVehicle();  
      }
    })
  }

  ionViewWillLeave()
  {
    this.loginEmitter.unsubscribe()
  }

  getMyVehicle(refresher = undefined)
  {
    this.spinnerVehicle = true;
    this.rest.getVehicle(this.criteria)
    .then(result => {
      this.spinnerVehicle = false;
      if (refresher !== undefined) 
      {
        refresher.complete();  
      }
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        window.localStorage.setItem("fields_vehicle", JSON.stringify(result['data']['fields']))
        this.myVehicle = result['data']['customers_cars']['data'];

        if (this.myVehicle.length > 0) 
        {
          /* If vehicle is already picked no need set default */
          if (this.vehicle == undefined && this.currentVehicle == undefined) 
          {
            /* Variable slide,disable enable form and etc */
            this.vehicle = this.myVehicle[0]['cst_crs_id'];
            this.currentVehicle = this.myVehicle[0]['cst_crs_id'];

            /* Set model id for save data */
            this.data.crs_model_id = this.myVehicle[0]['crs_model_id'];
            this.data.cst_crs_id = this.myVehicle[0]['cst_crs_id'];    
          }
        }
      }
      else if(typeof result['status'] !== undefined && result['status'] == 401)
      {
        window.localStorage.clear();
        this.global.isLogin = false;
      }
      else
      {

      }
    })
  }

  validateUserInfo()
  {
    let fieldsUserInfo = ['srv_fullname', 'srv_phone'];

    fieldsUserInfo.forEach(item => {
      if (this.data[item] == "" || this.data[item] == undefined) 
      {
        this.data[item] = this.dataUser[item];  
      }
    });
  }

  addVehicle()
  {
    let modal = this.modalCtrl.create(VehicleAddPage);
    modal.present();
    modal.onDidDismiss(data => {
      if (data !== undefined && data == 'saved') 
      {
        this.getMyVehicle();
      }
    });
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

  onChangeUserPick()
  {
    console.log(this.dataUser);
    
    if (this.takeBy == 'self') 
    {
      this.data.srv_t_fullname = this.dataUser.usr_fullname;
      this.data.srv_t_phone = this.dataUser.usr_phone;
    }
    else
    {
      this.data.srv_t_fullname = "";
      this.data.srv_t_phone = "";
    }
    
  }

  doRefresh(refresher)
  {
    this.getMyVehicle(refresher);
  }

  slideChanged()
  {
    
  }

  setVehicle(cars_id, model_id)
  {
    /* Variable slide,disable enable form and etc */
    this.vehicle = cars_id;
    this.currentVehicle = cars_id;

    /* Set model id for save data */
    this.data.crs_model_id = model_id;
    this.data.cst_crs_id = cars_id;

    /* Service variable, kebutuhan untuk disable enable list servis */
    this.selectedServices = new Array();
    this.routine_service = false;
    this.usual_service = false;
    this.type_service = null;

    /* Estimasi harga dan waktu */
    this.estimatePrice = 0;
    this.estimateTime = {
      hours : 0,
      minutes: 0
    }
  }

  slidePrev()
  {
    this.slides.slidePrev(1000);
    if (this.slides.isBeginning())
    {
      this.prev = false;
    }
    else
    {
      this.prev = true;
    }
  }

  slideNext()
  {
    this.slides.slideNext(1000);
    if (this.slides.isEnd())
    {
      this.next = false;
    }
    else
    {
      this.next = true;
    }
  }

  submitOrder()
  {
    if (this.validateForm()) 
    {
      this.loading = this.loadingCtrl.create({
        content : "Submit Order..."
      });

      this.loading.present();

      this.validateUserInfo();

      this.rest.saveService(this.data)
      .then(result => {
        this.loading.dismiss();
        if(typeof result['status'] !== undefined && result['status'] == 200)
        {
          if (typeof result['data']['services_transaction_status'] !== undefined && result['data']['services_transaction_status'] == false) 
          {
            this.global.toastDefault(result['data']['services_transaction']);
          }
          else
          {
            this.data_uuid = result['data']['services_transaction']['srv_t_uuid'];
            this.page = "orderSubmitted";
            this.socket.emit('new-order', result['data'])
          }
        }
        else
        {
          this.global.toastDefault("Something when wrong!");
        }
      })
    }
    
  }

  orderDetail()
  {
    let modal = this.modalCtrl.create(OrderDetailPage, {data_uuid : this.data_uuid});
    modal.present();
    modal.onDidDismiss(data => {

    });
  }

  setTypeService(e)
  {
    if (this.data.str_id == undefined || this.data.str_id == null) 
    {
      return this.global.toastDanger("Mohon pilih bengkel terlebih dahulu!");
    }

    this.estimatePrice = 0;
    this.estimateTime = {
      hours : 0,
      minutes: 0
    };
    this.selectedServices = [];
    this.data.srv_t_type = e;
    this.spinnerService = true;
    this.criteria.page = 1;
    if (e == 2) 
    {
      this.routine_service = true;
      this.usual_service = false;

      /* Get Dropdown Routine Service */
      this.getService('srv_b_id', this.criteria);
    }
    else if (e == 1) 
    {
      this.routine_service = false;
      this.usual_service = true;

      /* Get Dropdown Usual Service */
      this.getService('srv_id', this.criteria);
    }
    else
    {
      this.routine_service = false;
      this.usual_service = false;
      this.spinnerService = false;
    }
  }

  getService(type, criteria)
  {
    this.spinnerService = true;
    let filter = `&filter[crs_model_id]=${this.data.crs_model_id}`;
    let search = "";

    if (type == 'srv_b_id') 
    {
      if (this.keywordRS !== "")
      {
        search = `&search[srv_b_name]=${this.keywordRS}`;
      }
    }
    else if (type == 'srv_id') 
    {
      if (this.keywordUS !== "") 
      {
        search = `&search[srv_name]=${this.keywordUS}`;
      }
    }


    this.rest.getDropdown(type, criteria, filter, search)
    .then(result => {
      this.spinnerService = false;
      if(typeof result['status'] !== undefined && result['status'] == 200)
      {
        this.dropdownServices = result['data']['data'];
        if (result['data']['next_page'] <= result['data']['last_page']) 
        {
          this.criteria.page = result['data']['next_page'];
        }
        if (result['data']['total_data'] <= this.criteria.show) 
        {
          this.loadMore = false;  
        }
        else
        {
          this.loadMore = true;
        }
      }
    })
  }

  doSelectServices(srv)
  {
    if (this.type_service == 1) 
    {
      let find = this.selectedServices.findIndex(r=> r == srv.srv_id);

      if (find >= 0) 
      {
        delete this.selectedServices[find];

        this.selectedServices.filter(function (item) {
          return item != null;
        });

        this.calculatePrice();
        this.calculateTime();
      }
      else
      {
        this.selectedServices.push(srv.srv_id);
        this.containerSelected.push(srv);

        this.calculateTime();
        this.calculatePrice();
      }
      
      this.data.srv_id = this.selectedServices;
    }

    if (this.type_service == 2) 
    {
      this.selectedServices = srv.srv_b_id;
      this.containerSelected.push(srv);
      // let i = this.dropdownServices.findIndex(r => r['srv_b_id']==id);
      // if(i >= 0)
      // {
      //   this.data.srv_t_r_srv_b_list = JSON.stringify(this.dropdownServices[i]['srv_id']);
      // }

      this.calculateTime();
      this.calculatePrice();
      this.data.srv_b_id = this.selectedServices;
    }
    

  }

  checkSelectedServices(id)
  {
    let find = this.selectedServices.findIndex(r=> r == id);

    if (find >= 0) 
    {
      return true;  
    }

    return false;
  }

  calculatePrice()
  {
    let total = 0;

    if (this.type_service == 1) 
    {
      if (this.selectedServices.length > 0) 
      {
        this.selectedServices.forEach((id) => {
          let getId = this.containerSelected.findIndex(r=> r['srv_id'] == id);

          if (getId >= 0) 
          {
            total += this.containerSelected[getId]['srv_price']; 
          }
        });
      }
    }
    if (this.type_service == 2) 
    {
      let i = this.containerSelected.findIndex(r => r['srv_b_id']==this.selectedServices);
      if(i >= 0)
      {
        total = this.containerSelected[i]['srv_b_price']
      }
    }
    
    this.estimatePrice = total;
  }

  calculateTime()
  {
    let total:any = {
      hours : 0,
      minutes: 0,
    };

    
    if (this.type_service == 1) 
    {
      if (this.selectedServices.length > 0) 
      {
        this.selectedServices.forEach((id) => {
          let getId = this.containerSelected.findIndex(r=> r['srv_id'] == id);

          if (getId >= 0) 
          {
            total.hours += this.global.getHours(this.containerSelected[getId]['srv_time']);
            if (total.minutes + this.global.getMinutes(this.containerSelected[getId]['srv_time']) >= 60) 
            {
              total.hours += 1;
              if (this.global.getMinutes(this.containerSelected[getId]['srv_time']) > 30) 
              {
                total.minutes -= 30;
                total.minutes += this.global.getMinutes(this.containerSelected[getId]['srv_time']) - 30;
              }
              else
              {
                total.minutes -= 30;
              }
            }
            else
            {
              total.minutes += this.global.getMinutes(this.containerSelected[getId]['srv_time']);
            }
          }
        });
      }
    }
    if (this.type_service == 2) 
    {
      let i = this.containerSelected.findIndex(r => r['srv_b_id']==this.selectedServices);
      if(i >= 0)
      {
        total.minutes = this.global.getMinutes(this.containerSelected[i]['srv_b_time']);
        total.hours = this.global.getHours(this.containerSelected[i]['srv_b_time']);
      }
    }

    this.estimateTime = total;

    let temp_hour = (this.estimateTime.hours.toString().length == 1) ? '0' + this.estimateTime.hours.toString() : this.estimateTime.hours.toString();
    let temp_minute = (this.estimateTime.minutes.toString().length == 1) ? '0' + this.estimateTime.minutes.toString() : this.estimateTime.minutes.toString();
    this.data.srv_t_estimated_time = temp_hour +':'+ temp_minute +':00';
  }

  validateForm()
  {
    let result = true;
    let fields_required = [
      'str_id',
      'srv_t_date',
      'srv_t_hours_start',
    ];

    for (let i = 0; i < fields_required.length; i++) 
    {
      if (this.data[fields_required[i]] == undefined) 
      {
        if (fields_required[i] == 'str_id') 
        {
          result = false;
          this.global.toastDanger("Harap Pilih Lokasi Servis", 1500);
          break;
        } 
        else if (fields_required[i] == 'srv_t_date') 
        {
          result = false;
          this.global.toastDanger("Harap Pilih Tanggal Servis", 1500);
          break;
        }  
        else if (fields_required[i] == 'srv_t_hours_start') 
        {
          result = false;
          this.global.toastDanger("Harap Pilih Waktu Servis", 1500);
          break;
        } 
      }

      if (this.vehicle == undefined || this.vehicle == null) 
      {
        result = false;
        this.global.toastDanger("Harap Pilih Mobil", 1500);
        break;
      }

      if (this.type_service == null) 
      {
        result = false;
        this.global.toastDanger("Harap Pilih Tipe Servis", 1500);
        break;
      } 
      
      if (this.selectedServices.length == 0) 
      {
        result = false;
        this.global.toastDanger("Harap Pilih Item Servis", 1500);
        break;
      }
    }

    return result;
  }

  sAutoComplete(fields_name, fields_label)
  {
    let params:any = {
      fields_name   : fields_name,
      fields_label  : fields_label, 
    }

    let modal = this.modalCtrl.create(SAutoCompletePage, params);

    modal.present();

    modal.onDidDismiss(data => {
      if (fields_name == 'str_id') 
      {
        this.data.str_id = data.value;  
        this.data_label.str_id_label = data.label;  
        
        if (this.type_service !== undefined) 
        {
            this.setTypeService(this.type_service);
        }
      }
    })
  }

  loadMoreService(id)
  {
    this.isLoadMore = true;
    if(id == 1)
    {
      let filter = `&filter[crs_model_id]=${this.vehicle}`;

      this.rest.getDropdown('srv_id', this.criteria, filter)
      .then(result => {
        this.spinnerService = false;
        if(typeof result['status'] !== undefined && result['status'] == 200)
        {
          if (result['data']['next_page'] <= result['data']['last_page']) 
          {
            this.criteria.page = result['data']['next_page'];
          }
          let content = result['data']['data'];
          if (content.length > 0) 
          {
            content.forEach(item => {
              this.dropdownServices.push(item);
            });
          }

          if (content.length < this.criteria.show) 
          {
            this.loadMore = false;  
          }
          else
          {
            this.loadMore = true;
          }
          this.isLoadMore = false;
        }
        else
        {
          this.global.toastDefault(this.global.config("errorMsg"))
        }
      })
    }
    else if(id == 2)
    {
      let filter = `&filter[crs_model_id]=${this.vehicle}`;

      this.rest.getDropdown('srv_b_id', this.criteria, filter)
      .then(result => {
        this.spinnerService = false;
        if(typeof result['status'] !== undefined && result['status'] == 200)
        {
          if (result['data']['next_page'] <= result['data']['last_page']) 
          {
            this.criteria.page = result['data']['next_page'];
          }
          let content = result['data']['data'];
          if (content.length > 0) 
          {
            content.forEach(item => {
              this.dropdownServices.push(item);
            });
          }

          if (content.length < this.criteria.show) 
          {
            this.loadMore = false;  
          }
          else
          {
            this.loadMore = true;
          }
          this.isLoadMore = false;
        }
        else
        {
          this.global.toastDefault(this.global.config("errorMsg"))
        }
      })
    }
  }

  getSchedule()
  {
    let cont = {
      str_id : this.data.str_id,
      srv_t_date : this.data.srv_t_date,
    }

    this.rest.getSchedule(cont)
    .then(result => {
      if(typeof result['status'] !== undefined && result['status'] == 200)
      {
        let temp_schedule = [];
        if(Object.keys(result['data']['services_transaction']).length > 0)
        {
          Object.keys(result['data']['services_transaction']).forEach((i) => {
            temp_schedule.push({
              time : i,
              count : result['data']['services_transaction'][i],
            })
          }); 
        }

        this.availableSchedule = temp_schedule;
        this.isDayOff = result['data']['services_transaction_status'];
      }
      else
      {
        this.global.toastDefault("Gagal Mendapatkan Jadwal Servis!")
      }
    })
    
  }

  openMaps()
  {
    let modal = this.modalCtrl.create(MapsPage);
    
    modal.present();
    this.global.openMaps = true;

    modal.onDidDismiss(data => {
      this.global.openMaps = false;
      if (data !== undefined) 
      {
        this.data.srv_t_maps = `Lokasi Tersimpan. Lat : ${data.lat}, Lng : ${data.lng}`;  
      }
    });
  }

  checkLimit()
  {
    let cont = {
      srv_t_date : this.data.srv_t_date,
      str_id : this.data.str_id
    }

    /* Set Date order is picked */
    this.isDateOrderPicked = true;

    this.isCheckLimit = true;

    this.rest.checkLimit(cont)
    .then(result => {
      this.isCheckLimit = false;
      if(typeof result['status'] !== undefined && result['status'] == 200)
      {
        this.kuotaService = result['data']['services_transaction_status'];
      }
      else
      {
        this.global.toastDefault("Gagal Mendapatkan Kuota Servis!")
      }
    })
    
  }

  setIsDateTimeOrderPicked()
  {
    // this.isDateTimeOrderPicked = true;
    // console.log(this.isDateTimeOrderPicked);
    
  }

  search(ev, srv_type)
  {
    let totalWords = 0;
    if (srv_type == 2) 
    {
      this.keywordRS = ev.value;
      totalWords = this.keywordRS.length;
    }
    else if (srv_type == 1) 
    {
      this.keywordUS = ev.value;
      totalWords = this.keywordUS.length;
    }
    
    if(srv_type !== undefined)
    {
      if (totalWords >= 2) 
      {
        if (srv_type == 2) 
        {
          this.getService('srv_b_id', this.criteria)
        }
        else if (srv_type == 1) 
        {
          this.getService('srv_id', this.criteria)
        }
      }
      else if(totalWords == 0)
      {
        if (srv_type == 2) 
        {
          this.getService('srv_b_id', this.criteria)
        }
        else if (srv_type == 1) 
        {
          this.getService('srv_id', this.criteria)
        }
      }
    }
  }
  
}
