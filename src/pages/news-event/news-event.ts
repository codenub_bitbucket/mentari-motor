import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { NewsEventDetailPage } from './_detail/detail';
import { GlobalProvider } from '../../providers/global';
import { RestProvider } from '../../providers/rest';

@Component({
  selector: 'page-news-event',
  templateUrl: 'news-event.html',
})
export class NewsEventPage {

  criteria:any = {
    show : this.global.config("showPerPage"),
    order_by: 'created_at',
    type_sort: 'DESC',
    page: 1,
  }
  list_data:any = new Array();

  retrySubs:any;
  spinner:boolean = false;
  errorGetData:boolean = false;

  loadMore:boolean=true;

  constructor(
    public navCtrl        : NavController, 
    public navParams      : NavParams,
    public modalCtrl      : ModalController,
    public viewCtrl       : ViewController,
    public global         : GlobalProvider,
    public rest           : RestProvider,

  ) {
    this.getData();
    this.retrySubs = this.global.retryEmitter.subscribe(data => {
      if (data == 'news-event') 
      {
        this.getData();
      }
    })
  }

  doRefresh(refresher)
  {
    this.getData(refresher);
  }

  getData(refresher = undefined)
  {
    let filter = `&filter[blog_type][]=0&filter[blog_type][]=2&filter[is_publish]=1`;

    this.errorGetData = false;
    this.spinner = true;

    this.rest.getListBlog(this.criteria, filter)
    .then(result => {
      this.spinner = false;
      if (refresher !== undefined) 
      {
        refresher.complete();  
      }
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.criteria.page = result['data']['blog']['next_page']
        this.list_data = result['data']['blog']['data'];
      }
      else
      {
        this.errorGetData = true;
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
  }

  doLoadMore(infiniteScroll)
  {
    let filter = `&filter[blog_type][]=1&filter[blog_type][]=3&filter[is_publish]=1`;

    this.rest.getListBlog(this.criteria, filter)
    .then(result => {
      infiniteScroll.complete();
      this.criteria.page = result['data']['blog']['next_page']
      for( let content of result['data']['blog']['data'])
      {
        this.list_data.push(content);
      }
      if(result['data']['blog']['data'].length < this.criteria.show)
      {
        this.loadMore = false;
      }
      else
      {
        this.loadMore = true;
      }
    })
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

  detail(uuid)
  {
    let modal = this.modalCtrl.create(NewsEventDetailPage, {data_uuid : uuid});
    modal.present()
    modal.onDidDismiss(data => {
      
    })
  }
}
