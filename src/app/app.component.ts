import { Component, enableProdMode, ViewChild } from '@angular/core';
import { ModalController, Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MainPage } from '../pages/main/main';
import { HomePage } from '../pages/home/home';
import { Socket } from 'ng-socket-io';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { global } from '@angular/core/src/util';
import { GlobalProvider } from '../providers/global';
import { Deeplinks } from '@ionic-native/deeplinks';
import { PopupInfoPage } from '../pages/extra/popupinfo';

enableProdMode();

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any;

  users_id : any = JSON.parse(window.localStorage.getItem('users_id'));

  constructor(
    public platform            : Platform, 
    public statusBar           : StatusBar, 
    public splashScreen        : SplashScreen,
    public socket              : Socket,
    public localNotifications  : LocalNotifications,
    public globalProvider      : GlobalProvider,
    private deeplinks          : Deeplinks,
    public modalCtrl: ModalController,
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.rootPage = MainPage;
      statusBar.styleDefault();
      splashScreen.hide();

      this.deeplinks.route({}).subscribe(match => {
        // match.$route - the route we matched, which is the matched entry from the arguments to route()
        // match.$args - the args passed in the link
        // match.$link - the full link data
        console.log('Successfully matched route', match);
      }, nomatch => {
        // nomatch.$link - the full link data
        let uuid = (nomatch.$link !== undefined && nomatch.$link.path !== undefined && nomatch.$link.path !== '') ? nomatch.$link.path : null;
        if(uuid !== null)
        {
          this.globalProvider.emitSuccessPayment.emit(uuid);
        }
        console.error('Got a deeplink that didn\'t match', nomatch);
      });

      let localVersion = window.localStorage.getItem('local_version');
      let currentVersion = globalProvider.config('versionApp');

      if (localVersion == null || localVersion == undefined) 
      {
        window.localStorage.setItem('local_version', globalProvider.config('versionApp'));  
      }
      else
      {
        if (localVersion !== currentVersion) 
        {
          window.localStorage.clear();  
        }
      }

      let link:any = this.globalProvider.currentComponent;
      // link = link.split("#");

      if(link == "home")
      {
        this.showModalInfo();
      }

      console.log(localVersion);
      

      this.users_id = JSON.parse(window.localStorage.getItem('users_id'));

      
      if (this.users_id !== undefined && this.users_id !== null && this.users_id !== '') 
      {
        console.log("masuk if sini");
        this.socket.disconnect();
        this.socket.connect();
        this.socket.on('socket-id', (res) => {
          console.log('get socket-id');
  
          let socket_id = res.id;
          this.socket.emit('register-session', {
            users_id : this.users_id,
            socket_id : socket_id
          });
  
          window.localStorage.setItem("socket_id", JSON.stringify(socket_id));
        });

        this.socket.on('update-status-transaction', (res) => {
          console.log('get update-status-transaction', res);
          let data = res.data;
          let to_user = res.to_user;
          if (to_user == this.users_id) 
          {
            console.log(data.notif_description);
            this.localNotifications.schedule({
              title: 'Update Pesanan',
              text: data.notif_description,
            });  
          }

        });

        this.socket.on('emit-approve-home-service', (res) => {
          console.log('emit-approve-home-service', res);
          let data = res.data;
          let to_user = res.to_user;
          if (to_user == this.users_id) 
          {
            this.localNotifications.schedule({
              title: data.notif_name,
              text: data.notif_description,
            });  
          }

        });
  
        this.socket.on('payment-notification', (res) => {
          console.log('payment-notification', res);
          
          let data = res.data;
          let to_user = res.to_user;
          if (to_user == this.users_id) 
          {
            let text = '';
            if (data.srv_t_status == 3) 
            {
              text = `Pembayaran Booking Fee : ${data['srv_t_code']} telah diterima.`;
              this.globalProvider.orderReciever.emit(res);
            }
            else if (data.srv_t_status == 9) 
            {
              text = `Pembayaran Booking Fee : ${data['srv_t_code']} belum diterima, Pesanan sudah dibatalkan.`;
            }
            
            if (data.srv_t_status == 3 || data.srv_t_status == 9) 
            {
              this.localNotifications.schedule({
                title: 'Update Pembayaran',
                text: text,
              });  
            }
          }
        })  
      }
    });
  }

  showModalInfo()
  {    
    let modal = this.modalCtrl.create(PopupInfoPage,{}, {
      cssClass:"mymodalInfo"
    });

    modal.onDidDismiss(data => {
      console.log(data);
      
      if (data == 'daftar')
      {
        this.globalProvider.modalInfoEmitter.emit(data);
      }
    })

    modal.present();
  }
}

