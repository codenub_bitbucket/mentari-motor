import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register';
import { IonicModule } from 'ionic-angular';
import { ComingsoonComponent } from './comingsoon/comingsoon';
import { MapsviewComponent } from './mapsview/mapsview';
import { Error_500Component } from './errors/500/error_500';
import { NoNetworkComponent } from './errors/no_network/no_network';
import { FinishPaymentComponent } from './finish-payment/finish-payment';
import { TokopediaComponent } from './tokopedia/tokopedia';
import { TowingListComponent } from './towing-list/towing-list';
@NgModule({
    declarations: [
			LoginComponent,
			RegisterComponent,
    	ComingsoonComponent,
    	MapsviewComponent,
			Error_500Component,
			NoNetworkComponent,
			FinishPaymentComponent,
    	TokopediaComponent,
    	TowingListComponent
		],
    imports: [
			FormsModule, 
			IonicModule
		],
    exports: [
			LoginComponent,
			RegisterComponent,
    	ComingsoonComponent,
    	MapsviewComponent,
			Error_500Component,
			NoNetworkComponent,
			FinishPaymentComponent,
    	TokopediaComponent,
    	TowingListComponent
		]
})
export class ComponentsModule {}