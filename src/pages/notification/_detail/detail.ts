import { Component } from '@angular/core';
import { ModalController, NavController, NavParams, ViewController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { GlobalProvider } from '../../../providers/global';
import { RestProvider } from '../../../providers/rest';
import { OrderDetailPage } from '../../extra/order-detail';

@Component({
  selector: 'page-notification',
  templateUrl: 'detail.html',
})
export class NotificationDetailPage {

  retrySubs:any;
  spinner:boolean = false;
  errorGetData:boolean = false;

  data_uuid:string = "";

  content:any = {};
  content_srv:any = {};

  
  serviceBulkList:any= [];
  serviceRegulerList:any= [];
  ProductList:any= [];


  constructor(
    public navCtrl      : NavController, 
    public navParams    : NavParams,
    public viewCtrl     : ViewController,
    public global       : GlobalProvider,
    public rest         : RestProvider,
    public modalCtrl: ModalController,
  ) {
    this.data_uuid = this.navParams.get("data_uuid");
    this.getData();
    this.retrySubs = this.global.retryEmitter.subscribe(data => {
      if (data == 'notification') 
      {
        this.getData();
      }
    })
  }

  ionViewWillLeave()
  {
    this.retrySubs.unsubscribe();
  }

  doRefresh(refresher)
  {
    this.serviceBulkList = [];
    this.serviceRegulerList = [];
    this.ProductList = [];
    this.getData(refresher);
  }

  getData(refresher = undefined)
  {
    this.errorGetData = false;
    this.spinner = true;
    this.rest.detailNotification(this.data_uuid)
    .then(result => {
      this.spinner = false;
      if (refresher !== undefined) 
      {
        refresher.complete();  
      }
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.content = result['data']['notification'];
        this.content_srv = result['data']['services_transaction'];
        this.content_srv.pm_origin_data = JSON.parse(this.content_srv.pm_origin_data);

        if (this.content_srv.srv_t_r.length > 0) 
        {
          this.content_srv.srv_t_r.forEach(item => {
            if (item['srv_t_d_type'] == 1) 
            {
              this.serviceRegulerList.push(item);
            }
            else if (item['srv_t_d_type'] == 2) 
            {
              this.serviceBulkList.push(item);
            }
            else if (item['srv_t_d_type'] == 3) 
            {
              this.ProductList.push(item);
            }
          });  
        }
      }
      else if(typeof result['status'] !== undefined && result['status'] == 501)
      {
        this.global.toastDefault(this.global.config("errorMsg"))
        this.dismiss();
      }
      else
      {
        this.errorGetData = true;
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

  calculateTime(time)
  {
    let days = "0";
    let hours = "0";
    let minutes = "0";
    if(time !== '' && time !== null && time !== undefined)
    {
      let temp_hour = time.split(":")[0];
      if (parseInt(temp_hour) > 24) 
      {
        days = (Math.floor(temp_hour / 24)).toString();
        hours = ((parseInt(days) > 0) ? (parseInt(temp_hour) - (parseInt(days) * 24)) : parseInt(temp_hour)).toString();
        minutes = time.split(":")[1];
      }
      else
      {
        hours = parseInt(temp_hour).toString();
        minutes = time.split(":")[1];
      }
    }
    return `${days} Hari, ${hours} Jam, ${minutes} Menit`;
  }

  openTrx()
  {
    let myModal = this.modalCtrl.create(OrderDetailPage, {data_uuid : this.content.srv_t_uuid});

    myModal.present();

    myModal.onDidDismiss(data => {
      this.getData();
    });
  }

}
