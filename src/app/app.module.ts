import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { MainPage } from '../pages/main/main';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { GlobalProvider } from '../providers/global';
import { HttpClientModule } from '@angular/common/http';
import { VehicleAddPage } from '../pages/extra/vehicle-add';

import { AboutUsPage } from '../pages/about-us/about-us';
import { ActivityPage } from '../pages/activity/activity';
import { ActivityPendingListPage } from '../pages/activity/_tab/pending';
import { ActivityOnProgressListPage } from '../pages/activity/_tab/onprogress';
import { ActivityCompleteListPage } from '../pages/activity/_tab/complete';
import { ActivityCanceledListPage } from '../pages/activity/_tab/canceled';
import { SuperTabsModule, SuperTabsController } from 'ionic2-super-tabs';
import { ActivityDetailPage } from '../pages/activity/_detail/detail';
import { NotificationPage } from '../pages/notification/notification';
import { WorkshopServicePage } from '../pages/workshop-service/workshop-service';
import { OrderDetailPage } from '../pages/extra/order-detail';
import { EmergencyServicePage } from '../pages/emergency-service/emergency-service';
import { ProductsPage } from '../pages/products/products';
import { ProductsDetailPage } from '../pages/products/_detail/detail';
import { CartPage } from '../pages/extra/cart';
import { NewsEventPage } from '../pages/news-event/news-event';
import { NewsEventDetailPage } from '../pages/news-event/_detail/detail';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ProductsListPage } from '../pages/products/_list/products_list';
import { BookingServicePage } from '../pages/booking-service/booking-service';
import { HomeServicePage } from '../pages/home-service/home-service';

import { GoogleMaps } from '@ionic-native/google-maps'

import { LoginComponent } from '../components/login/login';
import { RestProvider } from '../providers/rest';
import { RegisterComponent } from '../components/register/register';
import { PromoPage } from '../pages/promo/promo';
import { PromoDetailPage } from '../pages/promo/_detail/detail';
import { ContactUsPage } from '../pages/contact-us/contact-us';
import { SAutoCompletePage } from '../pages/extra/s_autocomplete';
import { DetailCarsPage } from '../pages/profile/_cars/detail_cars';
import { CarsEditPage } from '../pages/profile/_cars/_edit/edit_cars';
import { ProfileEditPage } from '../pages/profile/_edit/profile_edit';
import { ProfileEditPasswordPage } from '../pages/profile/_edit/edit_password';
import { ComponentsModule } from '../components/components.module';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { PaymentsDPPage } from '../pages/extra/payments_dp';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Camera } from '@ionic-native/camera';
import { AttachmentPage } from '../pages/extra/attachments';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { FileTransfer } from '@ionic-native/file-transfer';
import { ActionSheet } from '@ionic-native/action-sheet';
import { NotificationDetailPage } from '../pages/notification/_detail/detail';
import { Clipboard } from '@ionic-native/clipboard';
import { MapsPage } from '../pages/extra/maps';
import { Geolocation } from '@ionic-native/geolocation';
import { AppAvailability } from '@ionic-native/app-availability';
import { Device } from '@ionic-native/device';
import { CallNumber } from '@ionic-native/call-number';
import { PaymentPage } from '../pages/payment/payment';
import { Deeplinks } from '@ionic-native/deeplinks';
import { InstructionPage } from '../pages/payment/instruction/instruction';
import { PopupInfoPage } from '../pages/extra/popupinfo';

const config: SocketIoConfig = { url: 'https://socket.mentarimotor.id:30443', options: {} };

@NgModule({
  declarations: [
    MyApp,

    /* COMPONENT */
    // LoginComponent,
    // RegisterComponent,
    // ComingsoonComponent,
    // MapsviewComponent,
    // Error_500Component,
    // NoNetworkComponent,
    /* END COMPONENT */
    
    //main page
    MainPage,

    // home page
    HomePage,

    // profile
    ProfilePage,
    ProfileEditPage,
    ProfileEditPasswordPage,

    /* Booking Service Declaration */
    BookingServicePage,
    WorkshopServicePage,
    HomeServicePage,
    /* End */

    // EXTRA
    VehicleAddPage,
    OrderDetailPage,
    SAutoCompletePage,
    AttachmentPage,
    MapsPage,

    // Promo
    AboutUsPage,

    // activity
    ActivityPage,
    ActivityPendingListPage,
    ActivityOnProgressListPage,
    ActivityCompleteListPage,
    ActivityCanceledListPage,
    ActivityDetailPage,

    // Notification
    NotificationPage,
    NotificationDetailPage,

    // Workshop
    EmergencyServicePage,

    // Products
    ProductsPage,
    ProductsListPage,
    ProductsDetailPage,

    // Cart
    CartPage,

    // News Event
    NewsEventPage,
    NewsEventDetailPage,

    // Promo
    PromoPage,
    PromoDetailPage,

    // Contact Us
    ContactUsPage,

    /* Vehicle */
    DetailCarsPage,
    CarsEditPage,
    /* End */

    PaymentsDPPage,
    PaymentPage,

    InstructionPage,
    PopupInfoPage,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicImageViewerModule,
    SuperTabsModule.forRoot(),
    SocketIoModule.forRoot(config),
    ComponentsModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding:false,
      monthNames: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember' ]
    }, {
      links: [
        { component: MainPage, name: 'MainPage', segment: 'home' },
        { component: ProfilePage, name: 'ProfilePage', segment: 'profile' },
        { component: ActivityPage, name: 'ActivityPage', segment: 'activity' },

        { component: ActivityPendingListPage, name: 'ActivityPendingListPage', segment: 'activity/pending' },
        { component: ActivityOnProgressListPage, name: 'ActivityOnProgressListPage', segment: 'activity/on-progress' },
        { component: ActivityCompleteListPage, name: 'ActivityCompleteListPage', segment: 'activity/complete' },
        { component: ActivityCanceledListPage, name: 'ActivityCanceledListPage', segment: 'activity/pending' },
        
      ]
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    
    //main page
    MainPage,

    // home page
    HomePage,

    // profile
    ProfilePage,
    ProfileEditPage,
    ProfileEditPasswordPage,
    
    /* Booking Service Declaration */
    BookingServicePage,
    WorkshopServicePage,
    HomeServicePage,
    /* End */
    
    // EXTRA
    VehicleAddPage,
    OrderDetailPage,
    SAutoCompletePage,
    AttachmentPage,
    MapsPage,

    // Promo
    AboutUsPage,

    // activity
    ActivityPage,
    ActivityPendingListPage,
    ActivityOnProgressListPage,
    ActivityCompleteListPage,
    ActivityCanceledListPage,
    ActivityDetailPage,

    // Notification
    NotificationPage,
    NotificationDetailPage,

    // Workshop
    EmergencyServicePage,

    // Products
    ProductsPage,
    ProductsListPage,
    ProductsDetailPage,

    // Cart
    CartPage,

    // News Event
    NewsEventPage,
    NewsEventDetailPage,

    // Promo
    PromoPage,
    PromoDetailPage,

    // Contact Us
    ContactUsPage,
    
    /* Vehicle */
    DetailCarsPage,
    CarsEditPage,
    /* End */
    
    PaymentsDPPage,

    PaymentPage,
    InstructionPage,
    PopupInfoPage,
  ],
  providers: [
    Deeplinks,
    StatusBar,
    SplashScreen,
    GlobalProvider,
    SuperTabsController,
    InAppBrowser,
    RestProvider,
    GoogleMaps,
    LocalNotifications,
    Camera,
    FilePath,
    File,
    FileTransfer,
    PhotoViewer,
    ActionSheet,
    Clipboard,
    Geolocation,
    AppAvailability,
    Device,
    CallNumber,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}
