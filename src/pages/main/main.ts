import { Component, ViewChild, Injectable } from '@angular/core';
import { NavController, Slides, ModalController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global';
import { ActivityPage } from '../activity/activity';
import { ProfilePage } from '../profile/profile';
import { HomePage } from '../home/home';
import { AboutUsPage } from '../about-us/about-us';
import { _ParseAST } from '@angular/compiler';
import { NotificationPage } from '../notification/notification';
import { CartPage } from '../extra/cart';
import { ProfileEditPage } from '../profile/_edit/profile_edit';
import { RestProvider } from '../../providers/rest';

@Injectable()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html'
})
export class MainPage {

  tabHome:any;
  tabProfile:any;
  tabEmergency:any;

  countNotif:number = 0;
  footerTab:string="home";

  constructor(
    public navCtrl: NavController,
    public globalProvider : GlobalProvider,
    public modalCtrl: ModalController,
    public global     : GlobalProvider,
    public rest       : RestProvider,
  ) {
    this.tabHome = HomePage;
    this.tabEmergency = ActivityPage;
    this.tabProfile = ProfilePage;

    this.global.notifCountEmiter.subscribe(data => {
      this.countNotif = data;
    })
  }

  ionViewWillEnter()
  {
    this.getNotifCount();
  }

  getNotifCount()
  {
    if(!this.global.isLogin) return;

    let criteria:any = {
      show : this.global.config("showPerPage"),
      order_by: 'created_at',
      type_sort: 'DESC',
      page: 1,
      get : "count",
    }

    let filter = `&filter[is_read]=0`;

    this.rest.getNotification(criteria, filter)
    .then(result => {
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.countNotif = result['data'];
      }
      else
      {
        
      }
    })
  }

  setTab(tab:string='home')
  {
    this.footerTab = tab;
  }

  aboutUs()
  {
    let modal = this.modalCtrl.create(AboutUsPage);

    modal.present();

    modal.onDidDismiss(data => {

    });
  }

  notification()
  {
    let modal = this.modalCtrl.create(NotificationPage);

    modal.present();

    modal.onDidDismiss(data => {
      this.getNotifCount();
    });
  }

  cart()
  {
    let modal = this.modalCtrl.create(CartPage);

    modal.present();

    modal.onDidDismiss(data => {

    });
  }

  editProfile()
  {
    let modal = this.modalCtrl.create(ProfileEditPage);

    modal.present();

    modal.onDidDismiss(data => {
      if (data !== undefined && data == 'saved') 
      {
        this.globalProvider.profileEmitter.emit(true);
      }
    });
  }


}
