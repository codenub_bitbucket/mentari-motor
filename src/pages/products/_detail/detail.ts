import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { OrderDetailPage } from '../../extra/order-detail';
import { GlobalProvider } from '../../../providers/global';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-products',
  templateUrl: 'detail.html',
})
export class ProductsDetailPage {

  page:string='detail';

  isAddingToCart:boolean=false;
  totalCart:number=0;

  constructor(
    public navCtrl:       NavController, 
    public navParams:     NavParams,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public global: GlobalProvider,
    private iab: InAppBrowser,
  ) {
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

  tokped()
  {
    let iabOption:InAppBrowserOptions = {
      hideurlbar : "yes",
    }
    var browser = this.iab.create("https://tokopedia.com", '_blank', );
  }

  addToCart()
  {
    this.global.toastDefault("Produk Dimasukan Keranjang");
  }

  directBuy()
  {
    this.page = "checkout";
  }

  makeOrder()
  {
    this.page = "done";
  }

  orderDetail()
  {
    let modal = this.modalCtrl.create(OrderDetailPage);
    modal.present();
    modal.onDidDismiss(data => {

    });
  }
}
