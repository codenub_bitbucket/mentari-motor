import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController, Platform } from 'ionic-angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapOptions,
  GoogleMapsEvent,
  Marker,
} from '@ionic-native/google-maps';
import { GlobalProvider } from '../../providers/global';
import { RestProvider } from '../../providers/rest';

@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
})
export class ContactUsPage {

  map: GoogleMap;

  list_data:any = Array();

  criteria:any = {
    show : this.global.config("showPerPage"),
    order_by: 'created_at',
    type_sort: 'ASC',
    page: 1,
  }

  page:number = 1;

  loadMore:boolean = true;

  constructor(
    public navCtrl:       NavController, 
    public navParams:     NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public platform : Platform,
    public global: GlobalProvider,
    public rest : RestProvider, 

  ) {
    this.getListStore();
  }

  getListStore()
  {
    this.page = this.criteria.page = 1;
    this.rest.getStore(this.criteria)
    .then(result => {
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.list_data = result['data']['store']['data'];
        this.page = result['data']['store']['next_page'];

        if(result['data']['store']['data'].length < this.criteria.show)
        {
          // Disable load more
          this.loadMore = false;
        }
        else
        {
          // Enable load more
          this.loadMore = true;
        }
      }
      else
      {
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
  }

  doLoadMore(event, page)
  {
    console.log(page);
    
    this.criteria.page = page;
    this.rest.getStore(this.criteria)
    .then(result => {
      event.complete();
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.page = result['data']['store']['next_page'];
        if (result['data']['store']['data'].length > 0) 
        {
          result['data']['store']['data'].forEach(item => {
            this.list_data.push(item);  
          });
        }

        if(result['data']['store']['data'].length < this.criteria.show)
        {
          // Disable load more
          this.loadMore = false;
        }
        else
        {
          // Enable load more
          this.loadMore = true;
        }
      }
      else
      {
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
  }

  checkLatLong(str)
  {
    console.log('jancuk');
    
    var mapUrl = str.str_address_map;
    if (mapUrl !== undefined && mapUrl !== null) 
    {
      var url = mapUrl.split('@');
      var at = url[1].split('z');
      var zero = at[0].split(',');
      var lat = zero[0];
      var lng = zero[1];

      let cont = { 
        lat : lat,
        lng : lng,
        str_name : str.str_name,
      }
      this.global.onTriggerMapsStore.emit(cont);  
    }
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }
}
