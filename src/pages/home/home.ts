import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, ModalController } from 'ionic-angular';
import { AboutUsPage } from '../about-us/about-us';
import { WorkshopServicePage } from '../workshop-service/workshop-service';
import { EmergencyServicePage } from '../emergency-service/emergency-service';
import { ProductsPage } from '../products/products';
import { NewsEventPage } from '../news-event/news-event';
import { BookingServicePage } from '../booking-service/booking-service';
import { PromoPage } from '../promo/promo';
import { ContactUsPage } from '../contact-us/contact-us';
import { GlobalProvider } from '../../providers/global';
import { RestProvider } from '../../providers/rest';
import { NewsEventDetailPage } from '../news-event/_detail/detail';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  @ViewChild(Slides) slides: Slides;

  slider:any = [];
  noSlider:boolean = false;

  openContactUs:boolean = false;

  criteria:any = {
    show : this.global.config("showPerPage"),
    order_by: 'created_at',
    type_sort: 'DESC',
    page: 1,
  }

  list_data_banner:any = new Array();

  retrySubsBanner:any;
  spinnerBanner:boolean = false;
  errorGetBanner:boolean = false;

  modalInfoEmitter:any;
  page:any;

  intervalBanner:any;
  loginEmitter:Subscription;

  constructor(
    public navCtrl    : NavController,
    public navParams  : NavParams,
    public modalCtrl  : ModalController,
    public global     : GlobalProvider,
    public rest       : RestProvider,
  ) {
  }
  
  ionViewDidLoad()
  {
    this.global.currentComponent = 'home';
    this.modalInfoEmitter = this.global.modalInfoEmitter.subscribe(r => {
      if(r == 'daftar')
      {
        this.page = 'register';
        clearInterval(this.intervalBanner);
      }
      else if(r == 'dismiss')
      {
        this.page = undefined;
        clearInterval(this.intervalBanner);
      }
      // this.modalInfoEmitter.unsubscribe();
    })

    this.loginEmitter = this.global.loginEmitter.subscribe(s => {
      if (s == true) 
      {
        this.page = undefined;
        this.startIntervalBanner();
      }
    })

    this.startIntervalBanner();

    this.loadBanner();
  }

  startIntervalBanner() 
  {
    clearInterval(this.intervalBanner);
    this.intervalBanner = setInterval(() => {

      if (this.slides.getActiveIndex() == (this.slider.length - 1))
      {
        this.slides.slideTo(0, 1000)
      }
      else
      {
        this.slides.slideNext(1000);
      }
    }, 3000);
  }

  ionViewWillLeave()
  {
    this.loginEmitter.unsubscribe();
  }

  doRefresh(refresher)
  {
    this.loadBanner(refresher);

    this.getNotifCount();
  }

  getNotifCount()
  {
    if(!this.global.isLogin) return;

    let criteria:any = {
      show : this.global.config("showPerPage"),
      order_by: 'created_at',
      type_sort: 'DESC',
      page: 1,
      get : "count",
    }

    let filter = `&filter[is_read]=0`;

    this.rest.getNotification(criteria, filter)
    .then(result => {
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.global.notifCountEmiter.emit(result['data']);
      }
      else
      {
        
      }
    })
  }

  loadBanner(refresher=undefined)
  {
    let filter = `&filter[is_publish]=1&filter[is_header]=1`;

    this.errorGetBanner = false;
    this.spinnerBanner = true;

    this.rest.getListBlog(this.criteria, filter)
    .then(result => {
      this.spinnerBanner = false;
      if (refresher !== undefined) 
      {
        refresher.complete();  
      }
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        let content = result['data']['blog']['data'];
        if (content.length > 0) 
        {
          this.slider = result['data']['blog']['data'];
        }
        else
        {
          this.noSlider = true;
        }
      }
      else
      {
        this.errorGetBanner = true;
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
  }

  detailBanner(uuid)
  {
    let modal = this.modalCtrl.create(NewsEventDetailPage, {data_uuid : uuid});

    modal.present();

    modal.onDidDismiss(data => {
      this.loadBanner();
    });
  }

  bookingService()
  {
    let modal = this.modalCtrl.create(BookingServicePage);

    modal.present();

    modal.onDidDismiss(data => {

    });
  }

  emergencyService()
  {
    let modal = this.modalCtrl.create(EmergencyServicePage);

    modal.present().then(()=>{
    });

    modal.onDidDismiss(data => {
    });
  }

  products()
  {
    let modal = this.modalCtrl.create(ProductsPage);

    modal.present();

    modal.onDidDismiss(data => {
      
    });
  }

  newsEvent()
  {
    let modal = this.modalCtrl.create(NewsEventPage);

    modal.present();

    modal.onDidDismiss(data => {
      
    });
  }

  promo()
  {
    let modal = this.modalCtrl.create(PromoPage);

    modal.present();

    modal.onDidDismiss(data => {
      
    });
  }

  contactUs()
  {
    let modal = this.modalCtrl.create(ContactUsPage);

    modal.present().then(()=>{
      this.global.openContactUs = true;
    });

    modal.onDidDismiss(data => {
      this.global.openContactUs = false;
    });
  }
}
