import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-news-event',
  templateUrl: 'detail.html',
})
export class PromoDetailPage {

  constructor(
    public navCtrl:       NavController, 
    public navParams:     NavParams,
    public viewCtrl: ViewController,

  ) {
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

}
