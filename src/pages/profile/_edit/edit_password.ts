import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, Slides, ViewController, Loading, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../../providers/rest';
import { GlobalProvider } from '../../../providers/global';

@Component({
  selector: 'page-profile',
  templateUrl: 'edit_password.html',
})
export class ProfileEditPasswordPage {

  data:any = {};

  validPassword = false;
  loading : Loading;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public rest : RestProvider,
    public modalCtrl: ModalController,
    public global: GlobalProvider,
    public viewCtrl: ViewController,
    public loadingCtrl : LoadingController,

  ) {
  }

  updatePassword()
  {
    this.loading = this.loadingCtrl.create({
      content : "Menyimpan..."
    });

    this.loading.present();
    this.rest.updatePassword(this.data)
    .then(result => {
      this.loading.dismiss();

      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.dismiss();
      }
      else if(typeof result['status'] !== undefined && result['status'] == 500 && result['error'] !== undefined)
      {
        try {
          Object.keys(result['error']).forEach(i=>{
            this.global.toastDanger(result['error'][i]);
          })
        } catch (error) {
          this.global.toastDefault("Ouch...")
        }
      }
      else
      {
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

  checkPassword()
  {
    let new_pass = this.data.new_password;
    let confirm = this.data.password_confirmation;

    if (new_pass == confirm) 
    {
      this.validPassword = true;
    }
  }

}
