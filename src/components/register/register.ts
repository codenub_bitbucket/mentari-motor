import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestProvider } from '../../providers/rest';
import { Loading, LoadingController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global';
import { Socket } from 'ng-socket-io';
import { LocalNotifications } from '@ionic-native/local-notifications';

/**
 * Generated class for the RegisterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'register',
  templateUrl: 'register.html'
})
export class RegisterComponent {

  @ViewChild('myForm') myForm: ElementRef;

  loginForm:boolean=false;
  agreeTc:boolean=true;

  data:any={};

  required:any        = []; // required or not required fields
  formFields:any      = {}; // container fields as array
  FormRegister:FormGroup;

  btnRegister:boolean=true;

  loading:Loading;
  link:any = this.global.currentComponent;

  constructor(
    public builder            : FormBuilder,
    public rest : RestProvider,
    public loadingCtrl : LoadingController,
    public global : GlobalProvider,
    public socket              : Socket,
    public localNotifications  : LocalNotifications

  ) {
    this.validateDefined()
  }

  ionViewDidEnter()
  {
    console.log('asw');
    this.validateDefined();
    
  }
  
  backtoHome()
  {
    this.global.modalInfoEmitter.emit('dismiss');
  }

  validateDefined()
  {
    this.FormRegister = this.builder.group({
      'usr_fullname': [
        '',
        Validators.compose([])
      ],
      'usr_phone': [
        '',
        Validators.compose([])
      ],
      'usr_email': [
        '',
        Validators.compose([Validators.required])
      ],
      'usr_password': [
        '',
        Validators.compose([Validators.required])
      ],
      'usr_password_confirm': [
        '',
        Validators.compose([Validators.required])
      ],
    }, {validator: this.checkMailPass});
  }

  checkMailPass(group: FormGroup) 
  {
    let ret:any = {};

    let pass = group.get('usr_password').value;
    let confirmPass = group.get('usr_password_confirm').value;

    let email = group.get('usr_email').value;
    var pattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;

    if (pass !== confirmPass) 
    {
      ret.notSame = true;
    }

    if (pattern.test(email) !== true) 
    {
      ret.notEmail = true;
    }

    return ret   
  }

  login()
  {
    this.loginForm = true;
  }

  register()
  {
    this.btnRegister = false;

    this.loading = this.loadingCtrl.create({
      content: "Loging you in..."
    });

    this.loading.present();
    this.rest.register(this.data)
    .then(result => {
      this.loading.dismiss();
      this.btnRegister = true;
      console.log(result);
      
      if (typeof result['token'] !== undefined && result['token'] !== '' && result['token'] !== null && result['status'] !== 500)
			{
        window.localStorage.setItem("isLogin", JSON.stringify(true))
        window.localStorage.setItem("_token", JSON.stringify(result['token']))

        this.getMyProfile();
        
        this.global.checkLogin();
      }
      else if (typeof result['error'] !== undefined)
			{
        let err = 'Sistem Error';
        if (result['error']['usr_email'] !== undefined) 
        {
          err = result['error']['usr_email']  
        }
        else if(result['error']['usr_password'][0] !== undefined)
        {
          err = result['error']['usr_password'][0]
        }
        this.global.toastDefault(err)
      }
      else
      {
        this.global.toastDefault("Something Wrong!")
      }
    })
  }

  getMyProfile()
  {
    this.rest.getProfile()
    .then(result => {      
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        window.localStorage.setItem("profile_data", JSON.stringify(result['data']));
        window.localStorage.setItem("users_id", JSON.stringify(result['data']['usr_id']))
        this.initSocket();
      }
      else if(typeof result['status'] !== undefined && result['status'] == 401)
      {
        window.localStorage.clear();
        this.global.isLogin = false;
      }
      else
      {
        this.global.toastDefault(this.global.config("errorMsg"));
      }
    })
  }

  initSocket()
  {
    this.socket.disconnect();
    this.socket.connect();
    this.socket.on('socket-id', (res) => {
      console.log('get socket-id', res);

      let socket_id = res.id;
      this.socket.emit('register-session', {
        users_id : JSON.parse(window.localStorage.getItem('users_id')),
        socket_id : socket_id
      });

      window.localStorage.setItem("socket_id", JSON.stringify(socket_id));
    });

    this.socket.on('payment-notification', (res) => {
      let data = res.data;
      let to_user = res.to_user;
      if (to_user == JSON.parse(window.localStorage.getItem('users_id'))) 
      {
        let text = '';
        if (data.srv_t_status == 3) 
        {
          text = `Pembayaran Booking Fee : ${data['srv_t_code']} telah diterima.`;
        }
        else if (data.srv_t_status == 9) 
        {
          text = `Pembayaran Booking Fee : ${data['srv_t_code']} belum diterima, Pesanan sudah dibatalkan.`;
        }
        
        if (data.srv_t_status == 3 || data.srv_t_status == 9) 
        {
          this.localNotifications.schedule({
            title: 'Update Pembayaran',
            text: text,
          });  
        }
      }
    })
  }

}
