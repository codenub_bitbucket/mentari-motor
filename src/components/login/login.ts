import { Component, ViewChild, ElementRef } from '@angular/core';
import { LoadingController, Loading, ModalController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global';
import { RestProvider } from '../../providers/rest';
import { RegisterComponent } from '../register/register';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Socket } from 'ng-socket-io';
import { LocalNotifications } from '@ionic-native/local-notifications';

@Component({
  selector: 'login',
  templateUrl: 'login.html'
})
export class LoginComponent {

  loading:Loading;

  data:any = {
    email : "",
    password : ""
  }

  dataReset = {
    password : '',
    password_confirmation : '',
    email : ''
  }

  OTP: any =  {
    first: '',
    second: '',
    third: '',
    forth: '',
    fifth: '',
  };

  registerForm:boolean=false;

  @ViewChild('myForm') myForm: ElementRef;
  @ViewChild('myFormForgot') myFormForgot: ElementRef;
  @ViewChild('myFormReset') myFormReset: ElementRef;
  FormLogin:FormGroup;
  FormForgot:FormGroup;
  FormReset:FormGroup;
  
  @ViewChild('first') first;
  @ViewChild('second') second;
  @ViewChild('third') third;
  @ViewChild('forth') forth;
  @ViewChild('fifth') fifth;
  btnSubmit:boolean = false;
  
  template:string = 'login';


  constructor(
    public loadingCtrl : LoadingController,
    public global : GlobalProvider,
    public rest : RestProvider,
    public modalCtrl: ModalController,
    public builder            : FormBuilder,
    public socket              : Socket,
    public localNotifications  : LocalNotifications
  ) {
    this.validateDefined()
  }

  validateDefined()
  {
    this.FormLogin = this.builder.group({
      'email': [
        '',
        Validators.compose([Validators.required])
      ],
      'password': [
        '',
        Validators.compose([Validators.required])
      ],
    }, {validator: this.checkEmail});

    this.FormForgot = this.builder.group({
      'email': [
        '',
        Validators.compose([Validators.required])
      ]
    }, {validator: this.checkEmail});

    this.FormReset = this.builder.group({
      'password': [
        '',
        Validators.compose([Validators.required, Validators.minLength(6)])
      ],
      'password_confirmation': [
        '',
        Validators.compose([Validators.required])
      ]
    }, {validator: this.checkPass});
  }

  checkPass(group: FormGroup) 
  {
    let ret:any = {};

    let pass = group.get('password').value;
    let confirmPass = group.get('password_confirmation').value;

    if (pass !== confirmPass) 
    {
      ret.notSame = true;
    }

    return ret   
  }

  checkEmail(group: FormGroup) 
  {
    let ret:any = null;

    let email = group.get('email').value;
    var pattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;

    if (pattern.test(email) !== true) 
    {
      ret = {notEmail : true};
    }

    return ret   
  }

  login()
  {
    
    this.loading = this.loadingCtrl.create({
      content: "Loging you in...",
    })
    
    this.loading.present();

    // Proses Login
    this.rest.login(this.data)
    .then(result => {
      this.loading.dismissAll();
      if (typeof result['status'] !== undefined && result['status'] == 200)
			{
        window.localStorage.setItem("isLogin", JSON.stringify(true))
        window.localStorage.setItem("_token", JSON.stringify(result['token']))
        window.localStorage.setItem("users_id", JSON.stringify(result['usr_id']))

        this.getMyProfile();
        
        this.global.checkLogin();

        this.initSocket();
      }
      else if (typeof result['status'] !== undefined && result['status'] == 500)
			{
        this.global.toastDefault(result['message'])
      }
      else if (typeof result['status'] !== undefined && result['status'] == 202)
			{
        this.global.toastDefault(result['message'])
      }
      else
      {
        this.global.toastDefault("Something Wrong!")
      }
    })
  }

  register()
  {
    this.registerForm = true;
  }

  getMyProfile()
  {
    this.rest.getProfile()
    .then(result => {      
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        window.localStorage.setItem("profile_data", JSON.stringify(result['data']));
      }
      else if(typeof result['status'] !== undefined && result['status'] == 401)
      {
        window.localStorage.clear();
        this.global.isLogin = false;
      }
      else
      {
        this.global.toastDefault(this.global.config("errorMsg"));
      }
    })
  }

  initSocket()
  {
    this.socket.disconnect();
    this.socket.connect();
    this.socket.on('socket-id', (res) => {
      console.log('get socket-id');

      let socket_id = res.id;
      this.socket.emit('register-session', {
        users_id : JSON.parse(window.localStorage.getItem('users_id')),
        socket_id : socket_id
      });

      window.localStorage.setItem("socket_id", JSON.stringify(socket_id));
    });

    this.socket.on('update-status-transaction', (res) => {
      console.log('get update-status-transaction', res);
      let data = res.data;
      let to_user = res.to_user;
      if (to_user == JSON.parse(window.localStorage.getItem('users_id'))) 
      {
        console.log(data.notif_description);
        
      }

    });

    this.socket.on('payment-notification', (res) => {
      let data = res.data;
      let to_user = res.to_user;
      if (to_user == JSON.parse(window.localStorage.getItem('users_id'))) 
      {
        let text = '';
        if (data.srv_t_status == 3) 
        {
          text = `Pembayaran Booking Fee : ${data['srv_t_code']} telah diterima.`;
        }
        else if (data.srv_t_status == 9) 
        {
          text = `Pembayaran Booking Fee : ${data['srv_t_code']} belum diterima, Pesanan sudah dibatalkan.`;
        }
        
        if (data.srv_t_status == 3 || data.srv_t_status == 9) 
        {
          this.localNotifications.schedule({
            title: 'Update Pembayaran',
            text: text,
          });  
        }
      }
    })
  }

  forgotPassword()
  {
    this.global.isForgot = true;
    this.template = 'forgot';
  }

  sendForgot()
  {
    let loading = this.loadingCtrl.create({
      content : "Please wait...",
    })
    loading.present();
    this.rest.forgotPassword(this.data)
    .then(result => {
      loading.dismiss();
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.template = 'token';
      }
      else
      {
        this.global.toastDefault(this.global.config("errorMsg"));
      }
    })
  }

  otpController(event,now,next)
  {
    var key = event.keyCode || event.charCode;
    console.log(key, event);
    
    if(now == 'fifth')
    {
      if(this.OTP['fifth'] !== '')
      {
        this.btnSubmit = true;
      }
      else
      {
        this.btnSubmit = false;
      }
      return true;
    }
    else
    {
      next.setFocus();
    }
  }

  checkCode()
  {
    let code = "";
    Object.keys(this.OTP).forEach(key => {
      code += this.OTP[key];
    })
    
    let content = { 
      email : this.data.email,
      code : code
    }
    let loading = this.loadingCtrl.create({
      content : "Please wait...",
    })
    loading.present();
    
    this.rest.checkCode(content)
    .then(result => {
      loading.dismiss();
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.template = 'reset';
      }
      else
      {
        this.global.toastDefault(this.global.config("errorMsg"));
      }
    })
  }

  reset() 
  {
    let loading = this.loadingCtrl.create({
      content : "Please wait...",
    })
    loading.present();

    this.dataReset.email = this.data.email;
    this.rest.resetPassword(this.dataReset)
    .then(result => {
      loading.dismiss();
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.template = 'login';
      }
      else
      {
        this.global.toastDefault(this.global.config("errorMsg"));
      }
    })
  }

  setLogin()
  {
    this.template = 'login';
  }

}
