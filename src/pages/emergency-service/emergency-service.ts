import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, ModalController, ViewController, LoadingController, Loading } from 'ionic-angular';
import { OrderDetailPage } from '../extra/order-detail';
import { VehicleAddPage } from '../extra/vehicle-add';
import { GlobalProvider } from '../../providers/global';
import { Subscription } from 'rxjs/Subscription';
import { RestProvider } from '../../providers/rest';
import { Socket } from 'ng-socket-io';
import moment from 'moment';
import { MapsPage } from '../extra/maps';
import { CallNumber } from '@ionic-native/call-number';
import { SAutoCompletePage } from '../extra/s_autocomplete';


@Component({
  selector: 'page-emergency-service',
  templateUrl: 'emergency-service.html',
})
export class EmergencyServicePage {
  page:string = "order";

  @ViewChild(Slides) slides: Slides;

  criteria:any = {
    show : this.global.config("showPerPage"),
    order_by: 'created_at',
    type_sort: 'DESC',
    page: 1,
  }

  data_uuid:string = ""; // uuid if finish order 

  currentVehicle:any;
  vehicle:any;

  prev:boolean = false;
  next:boolean = true;

  data:any = {};

  myVehicle:any = new Array();

  dataUser:any = (window.localStorage.getItem("profile_data") !== undefined && window.localStorage.getItem("profile_data") !== null) ? JSON.parse(window.localStorage.getItem("profile_data")) : {};

  loginEmitter:Subscription;

  spinnerVehicle:boolean=false;

  takeBy:string = "self";

  loading:Loading;

  tosAccepted:boolean= false;

  reqTOS:boolean = false;
  imageCarsUpdated:Subscription;

  orderByStore:boolean = false;
  orderByCall:boolean = false;

  /* New Params */
  isPickedOrderType:boolean = false;

  data_label:any                  = {};

  alreadyCall:boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public global: GlobalProvider,
    public rest : RestProvider,
    public loadingCtrl : LoadingController,
    public socket              : Socket,
    private callNumber: CallNumber
  ) {
      this.data.srv_t_cat = 3;
    // this.global.checkLogin()
    // .then(s => {
    //   if (s == true) 
    //   {
    //     this.getMyVehicle();
    //     this.data.srv_t_cat = 3;
    //     this.data.str_id = 1;

    //     this.data.srv_t_fullname = (this.dataUser.usr_fullname !== null && this.dataUser.usr_fullname !== undefined) ? this.dataUser.usr_fullname : '';
    //     this.data.srv_t_phone = (this.dataUser.usr_phone !== null && this.dataUser.usr_phone !== undefined) ? this.dataUser.usr_phone : '';
    //   }
    // })
  }

  ionViewDidEnter()
  {
    this.imageCarsUpdated = this.global.imageUpdated.subscribe(r => {
      let find = this.myVehicle.findIndex(re => re['cst_crs_uuid'] == r['uuid']);

      if (find >= 0) 
      {
        this.myVehicle[find]['cst_crs_photo'] = r['url'];  
      }
    })

    this.global.checkLogin()
    .then(s => {
      if (s == true) 
      {
        this.getMyVehicle();
      }
    })
  
    this.loginEmitter = this.global.loginEmitter.subscribe(s => {
      if (s == true) 
      {
        this.global.isLogin = true;
        setTimeout(() => {
          this.dataUser = JSON.parse(window.localStorage.getItem("profile_data")); 
          this.data.srv_t_fullname = (this.dataUser.usr_fullname !== null && this.dataUser.usr_fullname !== undefined) ? this.dataUser.usr_fullname : '';
          this.data.srv_t_phone = (this.dataUser.usr_phone !== null && this.dataUser.usr_phone !== undefined) ? this.dataUser.usr_phone : '';
        }, 3000);
        
        this.getMyVehicle();  
      }
    })
  }

  ionViewWillLeave()
  {
    this.loginEmitter.unsubscribe()
  }

  doRefresh(refresher)
  {
    this.getMyVehicle(refresher);
  }

  getMyVehicle(refresher = undefined)
  {
    this.spinnerVehicle = true;
    this.rest.getVehicle(this.criteria)
    .then(result => {
      this.spinnerVehicle = false;
      if (refresher !== undefined) 
      {
        refresher.complete();  
      }
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        window.localStorage.setItem("fields_vehicle", JSON.stringify(result['data']['fields']))
        this.myVehicle = result['data']['customers_cars']['data']
        // if (this.myVehicle.length > 0) 
        // {
        //   /* Variable slide,disable enable form and etc */
        //   this.vehicle = this.myVehicle[0]['cst_crs_id'];
        //   this.currentVehicle = this.myVehicle[0]['cst_crs_id'];

        //   /* Set model id for save data */
        //   this.data.crs_model_id = this.myVehicle[0]['crs_model_id'];
        //   this.data.cst_crs_id = this.myVehicle[0]['cst_crs_id'];  
        // }
      }
      else if(typeof result['status'] !== undefined && result['status'] == 401)
      {
        window.localStorage.clear();
        this.global.isLogin = false;
      }
      else
      {
        
      }
    })
  }

  setVehicle(k)
  {
    this.vehicle = k;
    this.currentVehicle = k;

    this.data.crs_model_id = this.myVehicle[0]['crs_model_id'];
    this.data.cst_crs_id = this.myVehicle[0]['cst_crs_id'];  
  }

  slideChanged()
  {
    let k = this.slides.getActiveIndex();

    this.currentVehicle = k;
  }

  slidePrev()
  {
    this.slides.slidePrev(1000);
    if (this.slides.isBeginning())
    {
      this.prev = false;
    }
    else
    {
      this.prev = true;
    }
  }

  slideNext()
  {
    this.slides.slideNext(1000);
    if (this.slides.isEnd())
    {
      this.next = false;
    }
    else
    {
      this.next = true;
    }
  }

  submitOrder()
  {
    if (this.validateForm()) 
    {
      this.loading = this.loadingCtrl.create({
        content : "Submit Order..."
      });

      this.loading.present();

      this.validateUserInfo();

      if (this.data.cst_crs_id == undefined || this.data.cst_crs_id == 0 || this.data.cst_crs_id == null) 
      {
        this.loading.dismiss();
        return this.global.toastDanger("Harap Pilih Mobil", 1500);
      }
      this.data.is_login = (this.global.isLogin) ? true : false;

      // if (this.orderByStore) 
      // {
      // }
      // else if(this.orderByCall)
      // {
      //   this.data.emergency_type = 'call_by_towing';
      //   this.data.is_login = (this.global.isLogin) ? true : false;

      //   if ((this.data.cst_crs_id == undefined || this.data.cst_crs_id == 0 || this.data.cst_crs_id == null) && this.data.is_login == true) 
      //   {
      //     this.loading.dismiss();
      //     return this.global.toastDanger("Harap Pilih Mobil", 1500);
      //   }
      // }

      /* Additional */
      this.data.srv_t_hours_start = moment().format("HH:mm:ss");
      this.data.srv_t_date = moment().format("YYYY-MM-DD");
      this.data.srv_t_estimated_time = "00:00";

      this.data.emergency_type = 'order_by_store';
      this.data.srv_t_cat = 3;

      this.rest.saveService(this.data)
      .then(result => {
        this.loading.dismiss();
        if(typeof result['status'] !== undefined && result['status'] == 200)
        {
          this.data_uuid = result['data']['services_transaction']['srv_t_uuid'];
          this.page = "orderSubmitted";
          this.socket.emit('new-order', result['data'])
        }
        else
        {
          this.global.toastDefault("Something when wrong!");
        }
      })  
    }
    
  }

  submitTowing()
  {
    if (this.tosAccepted !== true) 
    {
      this.reqTOS = true;
      return this.global.toastDanger("Harap Checklist TOS", 1500);
    }
    this.page = 'orderSubmitted';
    this.data.emergency_type = 'call_by_towing';
    this.data.is_login = (this.global.isLogin) ? true : false;
  }

  submitOrderTowing()
  {
    this.callTowing(this.data);
    this.data.srv_t_cat = 3;

    this.rest.saveService(this.data)
    .then(result => {
      // this.loading.dismiss();
      if(typeof result['status'] !== undefined && result['status'] == 200)
      {
        this.page = "orderSubmitted";
        this.socket.emit('new-order', result['data'])
      }
      else
      {
        this.global.toastDefault("Something when wrong!");
      }
    })  
  }

  validateForm()
  {
    if(this.global.isLogin)
    {
      let result = true;
      this.reqTOS = false;

      if (this.tosAccepted !== true) 
      {
        result = false;
        this.reqTOS = true;
        this.global.toastDanger("Harap Checklist TOS", 1500);
      }

      return result;
    }
  }

  validateUserInfo()
  {
    let fieldsUserInfo = ['srv_fullname', 'srv_phone'];

    fieldsUserInfo.forEach(item => {
      if (this.data[item] == "" || this.data[item] == undefined) 
      {
        this.data[item] = this.dataUser[item];  
      }
    });
  }

  dismiss()
  {
    if(this.orderByCall && this.page == 'orderSubmitted' && !this.alreadyCall)
    {
      return this.global.toastDanger("Anda harus melakukan panggilan ke salah satu nomor dibawah agar pesanan di sistem kami.")
    }

    if (this.orderByCall || this.orderByStore) 
    {
      this.page = 'order';
      this.orderByStore = false;
      this.orderByCall = false;
      this.isPickedOrderType = false;
      this.data = {};
      this.vehicle = undefined;
      this.currentVehicle = undefined;
    }
    else
    {
      this.viewCtrl.dismiss();
    }
  }

  orderDetail()
  {
    let modal = this.modalCtrl.create(OrderDetailPage, {data_uuid : this.data_uuid});
    modal.present();
    modal.onDidDismiss(data => {

    });
  }

  addVehicle()
  {
    let modal = this.modalCtrl.create(VehicleAddPage);
    modal.present();
    modal.onDidDismiss(data => {
      if (data !== undefined && data == 'saved') 
      {
        this.getMyVehicle();
      }
    });
  }

  onChangeUserPick()
  {
    if (this.takeBy == 'self') 
    {
      this.data.srv_t_fullname = this.dataUser.usr_fullname;
      this.data.srv_t_phone = this.dataUser.usr_phone;
    }
    else
    {
      this.data.srv_t_fullname = "";
      this.data.srv_t_phone = "";
    }
    
  }

  openMaps()
  {
    let modal = this.modalCtrl.create(MapsPage);
    
    modal.present();
    this.global.openMaps = true;

    modal.onDidDismiss(data => {
      this.global.openMaps = false;
      if (data !== undefined) 
      {
        this.data.srv_t_maps = `Lokasi Tersimpan. Lat : ${data.lat}, Lng : ${data.lng}`;  
      }
    });
  }

  chooseOrderType(type)
  {
    this.isPickedOrderType = false;
    this.orderByStore = false;
    this.orderByCall = false;
    this.page = 'order';

    if (type == 'store') 
    {
      this.isPickedOrderType = true;
      this.orderByStore = true;
      this.getMyVehicle();
      
      this.data.srv_t_fullname = (this.dataUser.usr_fullname !== null && this.dataUser.usr_fullname !== undefined) ? this.dataUser.usr_fullname : '';
      this.data.srv_t_phone = (this.dataUser.usr_phone !== null && this.dataUser.usr_phone !== undefined) ? this.dataUser.usr_phone : '';
    }
    else if (type == 'towing')
    {
      this.isPickedOrderType = true;
      this.orderByCall = true;

      if (this.global.isLogin) 
      {
        this.getMyVehicle();
        
        this.data.srv_t_fullname = (this.dataUser.usr_fullname !== null && this.dataUser.usr_fullname !== undefined) ? this.dataUser.usr_fullname : '';
        this.data.srv_t_phone = (this.dataUser.usr_phone !== null && this.dataUser.usr_phone !== undefined) ? this.dataUser.usr_phone : '';
      }
    }
  }

  callTowing(data)
  {
    this.callNumber.callNumber(data.towing_number, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

  sAutoComplete(fields_name, fields_label)
  {
    let params:any = {
      fields_name   : fields_name,
      fields_label  : fields_label, 
    }

    let modal = this.modalCtrl.create(SAutoCompletePage, params);

    modal.present();

    modal.onDidDismiss(data => {
      if (data !== undefined) 
      {
        if (fields_name == 'str_id') 
        {
          this.data.str_id = data.value;  
          this.data_label.str_id_label = data.label;  
        }  
      }
    })
  }
}
