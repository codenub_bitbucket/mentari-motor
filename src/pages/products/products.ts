import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { ProductsDetailPage } from './_detail/detail';
import { ProductsListPage } from './_list/products_list';

@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {

  constructor(
    public navCtrl:       NavController, 
    public navParams:     NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
  ) {
  }

  listProducts(list_type:string="")
  {
    let modal = this.modalCtrl.create(ProductsListPage, {list_type:list_type});
    modal.present()
    modal.onDidDismiss(data => {
      
    })
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

}
