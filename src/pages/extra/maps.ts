import { Component, ElementRef, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Geolocation, PositionError} from '@ionic-native/geolocation';
import { GlobalProvider } from '../../providers/global';
import { GoogleMap, GoogleMapOptions, GoogleMaps, GoogleMapsEvent, LatLng, Marker, MarkerOptions, MyLocation, MyLocationOptions } from '@ionic-native/google-maps';

declare var google:any;
@Component({
  selector: 'page-extra',
  templateUrl: 'maps.html',
  providers     : [Geolocation]
})
export class MapsPage {

  @ViewChild('map') mapElement: ElementRef;

  map:GoogleMap; 
  coordsLat:number        = 0;
  coordsLon:number        = 0;

  n:number      = 10;
  totalCurPositon:number = 0;
  markers               = [];

  mapReady:boolean = false;
  spinner:boolean = false;

  constructor(
    public navCtrl:       NavController, 
    public navParams:     NavParams,
    public viewCtrl: ViewController,
    public global : GlobalProvider,
    public geolocation : Geolocation,
  ) {
    this.getUserPosition();
  }

  getUserPosition()
  {
    let options = {
      maximumAge  : 300000, 
      timeout     : 10000, // 10s
      enableHighAccuracy  : true,
    };

    this.geolocation.getCurrentPosition(options).then((resp) => 
    {
      // set latitude and longitude for submit : part 1 of 4              
      this.coordsLat 	= resp.coords.latitude;
      this.coordsLon 	= resp.coords.longitude;
      this.addMap(); // add map 

    }).catch((error: PositionError) => 
    {
      console.log(error);
      
      // this.totalCurPositon++; // try to-
      // console.log(this.totalCurPositon);
      
      // if (this.totalCurPositon > this.n) 
      // {
      //   this.global.toastDanger(error.message + ", try again");
      // }else
      // {
      //   this.getUserPosition();
      // }
    });
  }

  refreshPosition()
  {
    this.spinner = true;
    
    let opt_location: MyLocationOptions = {
      enableHighAccuracy: true
    };

    this.map.getMyLocation(opt_location).then((location: MyLocation) =>
    {
      this.coordsLat 	  = location["latLng"]["lat"];
      this.coordsLon 	  = location["latLng"]["lng"];

      var opt_cam = {
        target: location.latLng,
        duration: 200,
        zoom: 18,
      }

      this.map.clear();
      this.map.animateCamera(opt_cam).then(() =>
      {
        let options:MarkerOptions = {
          title: 'Lokasi Anda',
          position: {
            lat: this.coordsLat,
            lng: this.coordsLon,
          }
        }
        this.map.addMarker(options).then((marker: Marker) => {
          marker.showInfoWindow();
          this.spinner = false;
        });
      });

      });
  }

  addMap()
  {
    
    let position: LatLng = new LatLng(this.coordsLat, this.coordsLon);
    let mapOptions: GoogleMapOptions = {
      zoom        : 17, 
      camera: {
        target: position,
        zoom: 17
      },
    }
    this.map = GoogleMaps.create('map', mapOptions);

    this.map.on(GoogleMapsEvent.MAP_READY).subscribe((result) =>
    {
      this.mapReady = true;
      console.log('ready');
      let options:MarkerOptions = {
        draggable: true,
        title: 'Lokasi Anda',
        position: {
          lat: this.coordsLat,
          lng: this.coordsLon,
        }
      }
      this.map.addMarker(options).then((marker: Marker) => {
        marker.showInfoWindow();
      });
      
    });
  }

  saveLocation()
  {
    let cont = {
      lat : this.coordsLat,
      lng : this.coordsLon
    }

    this.dismiss(cont);
  }

  dismiss(data=undefined)
  {
    this.viewCtrl.dismiss(data);
  }

}
