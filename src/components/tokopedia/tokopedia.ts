import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the TokopediaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'tokopedia',
  templateUrl: 'tokopedia.html'
})
export class TokopediaComponent {

  text: string;

  constructor(
    public iab : InAppBrowser,
  ) {
  }

  openTokped()
  {
    let url = "https://www.google.com/url?sa=t&source=web&rct=j&url=https://m.tokopedia.com/mentarimotor&ved=2ahUKEwivqvPR_e3tAhUrlEsFHZdmBUgQjjgwAHoECAEQAQ&usg=AOvVaw00fCYkBfUSqOsFAxho0Lq_";
    this.iab.create(url, '_system')
  }

}
