import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest';
import { GlobalProvider } from '../../providers/global';

declare var snap;

@Component({
  selector: 'page-extra',
  templateUrl: 'payments_dp.html',
})
export class PaymentsDPPage {

  token:string = '';
  errorRequest:boolean = false;
  retrySubs:any;
  spinner:boolean = true;
  finish:boolean = false;

  dataPayment:any = {};
  dataService:any = {};

  processPay:boolean= false;

  serviceBulkList:any= [];
  serviceRegulerList:any= [];
  ProductList:any= [];

  constructor(
    public navCtrl      : NavController, 
    public navParams    : NavParams,
    public viewCtrl     : ViewController,
    public rest         : RestProvider,
    public global       : GlobalProvider,

  ) {
    this.dataPayment  = this.navParams.get("data");
    this.dataService     = this.navParams.get("data_service");

    console.log(this.dataPayment, this.dataService);
    
    if (this.dataService.srv_t_r.length > 0) 
    {
      this.dataService.srv_t_r.forEach(item => {
        if (item['srv_t_d_type'] == 1) 
        {
          this.serviceRegulerList.push(item);
        }
        else if (item['srv_t_d_type'] == 2) 
        {
          this.serviceBulkList.push(item);
        }
        else if (item['srv_t_d_type'] == 3) 
        {
          this.ProductList.push(item);
        }
      });  
    }
    
    this.retrySubs = this.global.retryEmitter.subscribe(data => {
      if (data == 'payments_dp') 
      {
        this.spinner = true;
        this.requestPayment(this.dataPayment);
      }
    })
    this.requestPayment(this.dataPayment);
  }

  requestPayment(dataPayment)
  {
    this.rest.requestPayment(dataPayment)
    .then((result:any) => {
      console.log(result);
      this.spinner = false;
      if (result.status !== undefined && (result.status == 400 || result.status == 500)) 
      {
        this.errorRequest = true;
      }
      else if (result.token !== undefined) 
      {
        this.errorRequest = false;
        this.token = result['token'];
      }
      else
      {
        this.errorRequest = true;
      }
      
    });
  }

  pay()
  {
    this.processPay = true;
    let that = this;
    console.log(snap);
    
    snap.pay(this.token, {
      onSuccess: function (result) {
        console.log('suc');
        that.processPay = false;
        that.redirectAfterPay(result, 'success');
      },
      onPending: function (result) {
        console.log('pen');
        that.processPay = false;
        that.redirectAfterPay(result, 'pending');
      },
      onError: function (result) {
        console.log('err');
        that.processPay = false;
        that.redirectAfterPay(result, 'error');
      },
      onClose: function(result){
        console.log('customer closed the popup without finishing the payment');
        that.redirectAfterPay(result, 'not_finished');
      }
    })
  }

  redirectAfterPay(data, status)
  {
    if (status !== 'error') 
    {
      this.spinner = true;
      data.srv_t_id = this.dataService.srv_t_id;
      data.payment_token = this.token;
      this.rest.savePayment(data)
      .then(result => {
        this.spinner = false;
        this.finish = true;

      })  
    }
    else if(status == 'not_finished')
    {
      this.global.toastDefault("Segera bayar tagihan anda.")
      this.dismiss();
    }
    else
    {
      this.global.toastDefault(this.global.config("errorMsg"))
      this.dismiss();
    }
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

  calculateTime(time)
  {
    let days = "0";
    let hours = "0";
    let minutes = "0";
    if(time !== '' && time !== null && time !== undefined)
    {
      let temp_hour = time.split(":")[0];
      if (parseInt(temp_hour) > 24) 
      {
        days = (Math.floor(temp_hour / 24)).toString();
        hours = ((parseInt(days) > 0) ? (parseInt(temp_hour) - (parseInt(days) * 24)) : parseInt(temp_hour)).toString();
        minutes = time.split(":")[1];
      }
      else
      {
        hours = parseInt(temp_hour).toString();
        minutes = time.split(":")[1];
      }
    }
    return `${days} Hari, ${hours} Jam, ${minutes} Menit`;
  }
  
}
