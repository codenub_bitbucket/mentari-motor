import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-activity',
  templateUrl: 'detail.html',
})
export class ActivityDetailPage {

  params:any = new Array();

  constructor(
    public navCtrl:       NavController, 
    public navParams:     NavParams,
    public viewCtrl: ViewController,

  ) {
    this.params = this.navParams.get("params");
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }


}
