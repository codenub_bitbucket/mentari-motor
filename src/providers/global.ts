import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter, Output } from '@angular/core';
import { ToastController } from 'ionic-angular';
import moment from 'moment';
import { global } from '@angular/core/src/util';
import { Clipboard } from '@ionic-native/clipboard';


@Injectable()
export class GlobalProvider {

  @Output() loginEmitter: EventEmitter<any> = new EventEmitter();
  @Output() profileEmitter: EventEmitter<any> = new EventEmitter();
  @Output() retryEmitter: EventEmitter<any> = new EventEmitter();

  isLogin:boolean = JSON.parse(window.localStorage.getItem("isLogin"));
  openContactUs:boolean = false;
  openMaps:boolean = false;
  showMaps:boolean = false;
  isForgot :boolean = false;

  orderHour:any = [
    { v : '08:00:00', label : '08:00' },
    { v : '09:00:00', label : '09:00' },
    { v : '10:00:00', label : '10:00' },
    { v : '11:00:00', label : '11:00' },
    { v : '12:00:00', label : '12:00' },
    { v : '13:00:00', label : '13:00' },
    { v : '14:00:00', label : '14:00' },
    { v : '15:00:00', label : '15:00' },
  ];

  currentComponent:string = 'home';

  @Output() imageUpdated: EventEmitter<any> = new EventEmitter();
  @Output() notifCountEmiter: EventEmitter<any> = new EventEmitter();
  @Output() orderReciever: EventEmitter<any> = new EventEmitter();
  @Output() onTriggerMapsStore: EventEmitter<any> = new EventEmitter();
  @Output() emitSuccessPayment: EventEmitter<any> = new EventEmitter();
  @Output() modalInfoEmitter: EventEmitter<any> = new EventEmitter();

  constructor(
    public http: HttpClient,
    private toastCtrl: ToastController,
    public clipboard: Clipboard,
  ) {
    console.log(moment('08:00:00').utc(true).format("HH"));
  }

  config(name:string)
  {
    var config = {
      "versionApp"      : "1.5.1",
      "build"           : "1",
      "apiUrlMode"      : 1,
      "apiUrlOffline"   : "http://192.168.0.107/api_chevicar/v1/",
      "apiUrlOnline"    : "https://api.mentarimotor.id/v1/", 
      "urlApi"          : "https://api.mentarimotor.id/", 
      "apiSocket"       : "https://socket.mentarimotor.id:30443", 
      "requestTimeout"  : 25000, // 25second
      "requestTimeoutUpload"  : 60000, // 60second
      "msgTimeout"      : "Request timeout, please try again!",
      "maxYear"         : moment().utc(true).add(1, 'year').format("YYYY"),
      "showPerPage"     : "15",
      "maxPayTime"      : "1",
      "errorMsg"        : "Ouch.. Sistem Error!",
    }

    return config[name];
  }

  checkLogin()
  {
    return new Promise((resolve, reject) => {
      let statusLogin = JSON.parse(window.localStorage.getItem("isLogin"))
      this.isLogin = statusLogin;
      this.loginEmitter.emit((this.isLogin) ? true : false)
      resolve(statusLogin);
    })
  }

  // logout()
  // {
  //   this.logoutEmitter.emit(true);
  // }

  toastDefaultTop(msg:string, dur:number=700000) 
  {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: dur,
      position: 'top',
      cssClass:'toast-default-top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  toastDefault(msg:string, dur:number=3000) 
  {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: dur,
      position: 'bottom',
      cssClass:'toast-default'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  toastDanger(msg:string, dur:number=3000) 
  {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: dur,
      position: 'bottom',
      cssClass:'toast-danger'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  validatorPattern(value)
  {
    var patterns = {
      alpha   : '[a-zA-Z ]+',
      numeric : '[0-9\.]+',
      integer : '[0-9]+',
      email   : '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'
    };
    if( value != null ) {
      var array = value.split('|');
      var pat = '';
      array.forEach((name)=>{
        if( name != '' ) {
          pat = name;
        }
      });
      if( patterns[pat] != undefined ) {
        return {
          regex: patterns[pat],
          name: pat
        };
      }
    }
    return false;
  }

  currencyFormat(num: any)
  {
    if(num > 0)
    {
      var number = +num;
      let result = number.toFixed(0).replace(".", ",").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") // use . as a separator
      return result;
    }

    return 0;
  }

  getHours(dateString:any)
  {
    let result = 0;
    if(dateString != "00:00:00" && dateString != "" && dateString != null)
    {
      result = moment(dateString, 'HH:mm').hours();
    }
    return result;
  }

  getStringHours(dateString:any)
  {
    let result = 0;
    if(dateString != "" && dateString != null)
    {
      result = dateString.split(':')[0];
    }
    return result;
  }

  getMinutes(dateString:any)
  {
    let result = 0;
    if(dateString != "00:00:00" && dateString != "" && dateString != null)
    {
      result = moment(dateString, 'HH:mm').minutes();
    }
    return result;
  }

  // date, hours, minutes
	convertDateHM(dateString:any, formatGmt:boolean= true)
	{
    let result = "";
    if(dateString != "0000-00-00 00:00:00" && dateString != "" && dateString != null)
    {
      if (dateString.indexOf("T") && dateString.indexOf("Z")) 
      {
        result = moment(dateString).format("DD MMM YYYY HH:mm").toString();
      }
      else
      {
        if (formatGmt == true) 
        {
          result = moment(dateString).add(7, 'hours').format("DD MMM YYYY HH:mm").toString();
        }
        else
        {
          result = moment(dateString).format("DD MMM YYYY HH:mm").toString();
        }
      }
    }
		return result;
  }

  convertDate(dateString:any)
	{
    let result = "";
    if(dateString != "0000-00-00 00:00:00" && dateString != "" && dateString != null)
    {
      if (dateString.indexOf("T") && dateString.indexOf("Z")) 
      {
        result = moment(dateString).format("DD MMM YYYY").toString();
      }
      else
      {
        result = moment(dateString).add(7, 'hours').format("DD MMM YYYY").toString();
      }
    }
		return result;
  }

  convertHM(dateString:any)
	{
    let result = "";
    if(dateString != "0000-00-00 00:00:00" && dateString != "" && dateString != null)
    {
      if (dateString.indexOf("T") && dateString.indexOf("Z")) 
      {
        result = moment(dateString).format("HH:mm").toString();
      }
      else
      {
        result = moment(dateString).add(7, 'hours').format("HH:mm").toString();
      }
    }
		return result;
  }

  convertToImage(base64File: string, mime_type:string="image/jpeg")
  {
    var binary  = atob(base64File.split(',')[1]);
    var array   = [];
    for (var i = 0; i < binary.length; i++)
    {
      array.push(binary.charCodeAt(i));
    }

    return new Blob([new Uint8Array(array)],
    {
      type: mime_type
    });
  }

  deleteSecond(timeString)
  {
    let result = timeString.split(":");
    let last_key = result.length - 1;
    delete result[last_key];
    result = result.filter(r => {
      return r !== null; 
    })
    result = result.join(":");
    return result;
  }

  updateImageDirect(data)
  {
    this.imageUpdated.emit(data)
  }

  limitBlogDesc(data:string)
  {
    if (data.length == 0) {return data};
    return data.substr(0, 200) + "...";
  }

  copyData(data)
  {
    this.clipboard.copy(data)
    .then((ok)=>{
      console.log(ok);
      this.toastDefault("Text copied to clipboard!", 1000)
    }).catch(err=>console.log(err))
  }

}
