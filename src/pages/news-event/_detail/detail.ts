import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { GlobalProvider } from '../../../providers/global';
import { RestProvider } from '../../../providers/rest';

@Component({
  selector: 'page-news-event',
  templateUrl: 'detail.html',
})
export class NewsEventDetailPage {

  data_uuid:string = "";

  content:any = {};

  spinner:boolean = false;
  errorGetData:boolean = false;

  constructor(
    public navCtrl        : NavController, 
    public navParams      : NavParams,
    public viewCtrl       : ViewController,
    public global         : GlobalProvider,
    public rest           : RestProvider,
  ) {
    this.data_uuid = navParams.get("data_uuid");
    this.getDetail();
  }

  doRefresh(refresher)
  {
    this.getDetail(refresher);
  }

  getDetail(refresher = undefined)
  {
    this.errorGetData = false;
    this.spinner = true;
    
    this.rest.getDetailBlog(this.data_uuid)
    .then(result => {
      this.spinner = false;
      if (refresher !== undefined) 
      {
        refresher.complete();  
      }
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.content = result['data'];
      }
      else
      {
        this.errorGetData = true;
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

}
