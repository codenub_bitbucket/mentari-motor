import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalProvider } from './global';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';


@Injectable()
export class RestProvider {

	apiUrl 			= this.global.config("apiUrlOnline");
	apiUrlWeb 	= this.global.config("apiUrlWeb");

	constructor(
		public http			: HttpClient, 
		public global		: GlobalProvider,
		public transfer	: FileTransfer, 
		public file			: File
	) {
		var apiUrlMode 	= this.global.config("apiUrlMode");
		if(apiUrlMode == 0)
		{
			this.apiUrl = this.global.config("apiUrlOffline");
    }
	}
	
	getHeader()
	{
		let headers = new HttpHeaders({
			'Authorization':'Bearer '+JSON.parse(window.localStorage.getItem("_token")),
			'Content-Type' : 'application/json',
		});

		return headers;
	}

	/* AUTHENTICATION REST API */
	login(criteria:any)
  {
    return new Promise( (resolve, reject) =>
		{
			this.http.post(this.apiUrl + 'users/login', JSON.stringify(criteria))
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    });
	}

	register(criteria:any)
  {
    return new Promise( (resolve, reject) =>
		{
			this.http.post(this.apiUrl + 'users/register', JSON.stringify(criteria))
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    });
	}

	logout()
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.get(this.apiUrl + 'users/logout', {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    });
	}
	/* END AUTH */
	
	/* SERVICES REST API */
	saveService(criteria, additional:any=undefined)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.post(this.apiUrl + 'services_transaction/save', JSON.stringify(criteria) ,{headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}
	/* END SERVICES REST API */


	/* VEHICLE REST API */
	getVehicle(criteria:any)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			let bodyReq = `?show=${criteria.show}&order_by=${criteria.order_by}&type_sort=${criteria.type_sort}&page=${criteria.page}`;

			this.http.get(this.apiUrl + 'customers_cars'+bodyReq, {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}

	getFormVehicle()
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.get(this.apiUrl + 'customers_cars/create', {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}

	saveVehicle(criteria)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.post(this.apiUrl + 'customers_cars/save', JSON.stringify(criteria) ,{headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}

	saveEditVehicle(criteria,data_uuid)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.post(this.apiUrl + `customers_cars/update/${data_uuid}`, JSON.stringify(criteria) ,{headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}

	getCarsDetail(data_uuid)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.get(this.apiUrl + `customers_cars/detail/${data_uuid}`, {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}

	carsEdit(data_uuid)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.get(this.apiUrl + `customers_cars/edit/${data_uuid}`, {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}
	/* END VEHICLE */

	/* DROPDOWN */
	getDropdown(dropdown_name, criteria:any, filter='', search='')
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			let bodyReq = `?show=${criteria.show}&order_by=${criteria.order_by}&type_sort=${criteria.type_sort}&page=${criteria.page}${filter}${search}`;
			// if (dropdown_name == 'crs_model_id') 
			// {
			// 	bodyReq = `?filter[crs_brand_id]=${criteria.brand_id}&show=${criteria.show}&order_by=${criteria.order_by}&type_sort=${criteria.type_sort}&page=${criteria.page}`;
			// }
			this.http.get(this.apiUrl + `dropdown/autocomplete/${dropdown_name}${bodyReq}`, {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}

	/* Profile */
	getProfile()
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.get(this.apiUrl + `users/detail`, {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}

	updateProfile(criteria)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.post(this.apiUrl + `users/update`, JSON.stringify(criteria) ,{headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}

	updatePassword(criteria:any)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.post(this.apiUrl + 'users/reset-password', criteria,{headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    });
	}

	/* Transaction */
	transaction(criteria:any, status=null)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			let filter = '';
			if (status !== null) 
			{
				if (Array.isArray(status)) 
				{
					status.forEach(s => {
						filter += `&filter[srv_t_status][]=${s}`;	
					});	
				}
				else
				{
					filter = `&filter[srv_t_status][]=${status}`;	
				}
			}
			let bodyReq = `?show=${criteria.show}&order_by=${criteria.order_by}&type_sort=${criteria.type_sort}&page=${criteria.page}${filter}`;
    
			this.http.get(this.apiUrl + `services_transaction${bodyReq}`, {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				console.log(err);
				
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}

	transactionDetail(uuid:any)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.get(this.apiUrl + `services_transaction/detail/${uuid}`, {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				console.log(err);
				
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}

	/* Products */
	products(criteria:any, group=null)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			let filter = '';
			if (group !== null) 
			{
				filter = `&filter[prd_c_group_id]=${group}`;	
			}
			let bodyReq = `?show=${criteria.show}&order_by=${criteria.order_by}&type_sort=${criteria.type_sort}&page=${criteria.page}${filter}`;
    
			this.http.get(this.apiUrl + `products${bodyReq}`, {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				console.log(err);
				
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}

	requestPayment(criteria)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.post(`${this.global.config("apiSocket")}/payment-gateway/request-payment`, JSON.stringify(criteria), {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	savePayment(criteria)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.post(this.apiUrl + `services_transaction/payment/save`, JSON.stringify(criteria), {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	uploadImage(imageData)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.post(this.apiUrl + `uploadImage`, JSON.stringify(imageData), {headers: headers})
			.timeout(this.global.config("requestTimeoutUpload"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	getNotification(criteria:any, filter='')
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			console.log(criteria);
			
			let bodyReq = `?show=${criteria.show}&order_by=${criteria.order_by}&type_sort=${criteria.type_sort}&page=${criteria.page}${filter}${(criteria.get !== undefined && criteria.get !== null) ? "&get="+criteria.get : ''}`;
    
			this.http.get(this.apiUrl + `users/notification${bodyReq}`, {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				console.log(err);
				
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
		}); 
	}

	detailNotification(uuid)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.get(this.apiUrl + `users/notification/detail/${uuid}`, {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}

	updateNotificationRead(uuid)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.post(this.apiUrl + `users/notification/update_read`, JSON.stringify(uuid), {headers: headers})
			.timeout(this.global.config("requestTimeoutUpload"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	getListBlog(criteria, filter)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();
			if (JSON.parse(window.localStorage.getItem("_token")) == null && JSON.parse(window.localStorage.getItem("_token")) == undefined) 
			{
				headers = new HttpHeaders();
			}

			let bodyReq = `?show=${criteria.show}&order_by=${criteria.order_by}&type_sort=${criteria.type_sort}&page=${criteria.page}${filter}`;

			this.http.get(this.apiUrl + `blog${bodyReq}`, {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}
	
	getDetailBlog(uuid)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();
			if (JSON.parse(window.localStorage.getItem("_token")) == null && JSON.parse(window.localStorage.getItem("_token")) == undefined) 
			{
				headers = new HttpHeaders();
			}

			this.http.get(this.apiUrl + `blog/detail/${uuid}`, {headers: headers})
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err.error);
				}
			});
    }); 
	}

	getSchedule(criteria)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.post(this.apiUrl + `services_transaction/checkSchedule`, JSON.stringify(criteria), {headers: headers})
			.timeout(this.global.config("requestTimeoutUpload"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	checkLimit(criteria)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.post(this.apiUrl + `services_transaction/check_limit`, JSON.stringify(criteria), {headers: headers})
			.timeout(this.global.config("requestTimeoutUpload"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	getStore(criteria)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = new HttpHeaders();

			let bodyReq = `?show=${criteria.show}&order_by=${criteria.order_by}&type_sort=${criteria.type_sort}&page=${criteria.page}`;

			this.http.get(this.apiUrl + `store${bodyReq}`, {headers: headers})
			.timeout(this.global.config("requestTimeoutUpload"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	charge_midtrans(token, payment_type)
	{
		return new Promise( (resolve, reject) =>
		{
			let cont = {
				payment_type : payment_type,
				token : token
			}
			this.http.post(`https://socket.mentarimotor.id:30443/payment-gateway/payNow`, cont)
			.timeout(this.global.config("requestTimeoutUpload"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	createPayment(criteria)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();
			this.http.post(`${this.global.config("apiSocket")}/payment-gateway/create-payment`, JSON.stringify(criteria), {headers: headers})
			.timeout(this.global.config("requestTimeoutUpload"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	deleteCars(data_uuid)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.post(this.apiUrl + `customers_cars/delete/${data_uuid}`, JSON.stringify({}), {headers: headers})
			.timeout(this.global.config("requestTimeoutUpload"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	getQr(url)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = this.getHeader();

			this.http.get(url, {headers: headers})
			.timeout(this.global.config("requestTimeoutUpload"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	forgotPassword(criteria)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = new HttpHeaders();

			this.http.post(this.apiUrl + `users/request-reset-password`, JSON.stringify(criteria), {headers: headers})
			.timeout(this.global.config("requestTimeoutUpload"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	checkCode(criteria)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = new HttpHeaders();

			this.http.post(this.apiUrl + `users/check-token-password`, JSON.stringify(criteria), {headers: headers})
			.timeout(this.global.config("requestTimeoutUpload"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	resetPassword(criteria)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = new HttpHeaders();

			this.http.post(this.apiUrl + `users/customers-reset-password`, JSON.stringify(criteria), {headers: headers})
			.timeout(this.global.config("requestTimeoutUpload"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}

	cancelOrder(criteria)
	{
		return new Promise( (resolve, reject) =>
		{
			let headers = new HttpHeaders();

			this.http.post(this.apiUrl + `service_transaction/cancel-order`, JSON.stringify(criteria), {headers: headers})
			.timeout(this.global.config("requestTimeoutUpload"))
			.subscribe(result => // responseW after API
			{
				resolve(result);
			}, (err) => // response before API
			{
				if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					this.global.toastDanger(this.global.config("msgTimeout"));
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
	}
	
}
