webpackJsonp([0],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmergencyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__extra_add_vehicle__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__emergency_order_success__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EmergencyPage = /** @class */ (function () {
    function EmergencyPage(navCtrl, navParams, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.prev = false;
        this.next = true;
    }
    EmergencyPage.prototype.setVehicle = function (k) {
        this.vehicle = k;
        this.currentVehicle = k;
    };
    EmergencyPage.prototype.slideChanged = function () {
        var k = this.slides.getActiveIndex();
        this.currentVehicle = k;
    };
    EmergencyPage.prototype.slidePrev = function () {
        this.slides.slidePrev(1000);
        if (this.slides.isBeginning()) {
            this.prev = false;
        }
        else {
            this.prev = true;
        }
    };
    EmergencyPage.prototype.slideNext = function () {
        this.slides.slideNext(1000);
        if (this.slides.isEnd()) {
            this.next = false;
        }
        else {
            this.next = true;
        }
    };
    EmergencyPage.prototype.add_vehicle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__extra_add_vehicle__["a" /* AddVehiclePage */]);
        modal.present();
        modal.onDidDismiss(function (data) {
            console.log('refresh kendaraan');
        });
    };
    EmergencyPage.prototype.submit_order = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__emergency_order_success__["a" /* EmergencyOrderSuccessPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Slides */])
    ], EmergencyPage.prototype, "slides", void 0);
    EmergencyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-emergency',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/emergency/emergency.html"*/'<ion-content padding>\n\n  <div class="d-block text-center">\n    <h4>Layanan Emergency</h4>\n  </div>\n\n  <div class="main-content mt-5">\n    <div class="choose-vehicle">\n      <hr>\n      <h4>Pilih Kendaraan Anda</h4>\n      <div class="step-1">\n        <div class="slider-vehicle d-flex" style="position: relative;">\n          <i class=\'bx bx-left-arrow-circle navigation-slider-left\' (click)="slidePrev()"></i>\n          <ion-slides style="height: 30vh;" (ionSlideDidChange)="slideChanged()">\n            <ion-slide>\n              <i class=\'bx bx-check vehicle-check\' *ngIf="vehicle == 0"></i>\n              <img src="../../assets/image/car.png" class="card-img" style="width: 40vw;" (click)="setVehicle(0)" >\n            </ion-slide>\n            <ion-slide>\n              <i class=\'bx bx-check vehicle-check\' *ngIf="vehicle == 1"></i>\n              <img src="../../assets/image/car2.png" class="card-img" style="width: 40vw;" (click)="setVehicle(1)">\n            </ion-slide>\n            <ion-slide>\n              <i class=\'bx bx-check vehicle-check\' *ngIf="vehicle == 2"></i>\n              <img src="../../assets/image/car3.png" class="card-img" style="width: 40vw;" (click)="setVehicle(2)">\n            </ion-slide>\n\n            <ion-slide class="add-vehicle-slide">\n              <div class="d-flex" (click)="add_vehicle()" >\n                <div class="m-auto">\n                  <h3><i class=\'bx bx-plus\'></i></h3>\n                  <h6>Tambah Kendaraan</h6>\n                </div>\n              </div>\n            </ion-slide>\n          </ion-slides>\n          <i class=\'bx bx-right-arrow-circle navigation-slider-right\' (click)="slideNext()"></i>\n        </div>\n      </div>\n    </div>\n\n    <div class="form-emergency-service" *ngIf="(currentVehicle == vehicle) && currentVehicle !== undefined && vehicle !== undefined">\n      <div class="keluhan">\n        <hr>\n        <h4>Keluhan/Kerusakan Kendaraan Anda</h4>\n        <div class="input-group">\n          <textarea class="form-control" aria-label="With textarea" rows="8" placeholder="Tulis Keluhan Atau Kerusakan Kendaraan Anda"></textarea>\n        </div>\n      </div>\n\n      <div class="alamat">\n        <hr>\n        <h4>Alamat</h4>\n        <div class="input-group">\n          <textarea class="form-control" aria-label="With textarea" rows="5" placeholder="Masukan Detail Alamat Anda"></textarea>\n        </div>\n      </div>\n\n      <div class="detail-user">\n        <hr>\n        <h4>Informasi Anda</h4>\n        <div class="form-group">\n          <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Nama Lengkap">\n        </div>\n\n        <div class="input-group mb-3">\n          <div class="input-group-prepend">\n            <span class="input-group-text" id="basic-addon1">+62</span>\n          </div>\n          <input type="text" class="form-control" placeholder="Nomor Telepon" aria-describedby="basic-addon1">\n        </div>\n\n        <div class="form-group">\n          <input type="text" class="form-control" id="exampleFormControlInput1" value="customer@gmail.com" disabled>\n        </div>\n\n        <div class="form-group form-check">\n          <input type="checkbox" class="form-check-input" id="exampleCheck1">\n          <label class="form-check-label ml-2" for="exampleCheck1">Saya setuju dengan <span style="color: red;">Syarat & Ketentuan</span> YOUR COMPANY</label>\n        </div>\n\n        <button type="button" class="btn btn-lg btn-block btn-outline-primary" style="border-radius: 15px;" (click)="submit_order()" >Pesan Sekarang</button>\n      </div>\n    </div>\n  </div>\n\n\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/emergency/emergency.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* ModalController */]])
    ], EmergencyPage);
    return EmergencyPage;
}());

//# sourceMappingURL=emergency.js.map

/***/ }),

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddVehiclePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AddVehiclePage = /** @class */ (function () {
    function AddVehiclePage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    AddVehiclePage.prototype.close = function (action) {
        if (action === void 0) { action = "cancel"; }
        this.viewCtrl.dismiss(action);
    };
    AddVehiclePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-extra',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/extra/add_vehicle.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>Tambah Kendaraan</ion-title>\n    <ion-buttons right>\n      <button ion-button (click)="close(\'cancel\')" clear color="white" icon-only>\n        <i class="bx bx-x-circle mr-3" style="font-size: 20px;"></i>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n\n  <div class="info-mobil">\n    <h2>Informasi Mobil</h2>\n    <div class="text-center">\n      <i class=\'bx bxs-car-garage\' style="font-size:70px; color: #999;"></i>\n    </div>\n\n    <div class="form-group">\n      <label for="exampleFormControlSelect1" class="text-muted">Pilih Model Mobil</label>\n      <select class="form-control" id="exampleFormControlSelect1">\n        <option disabled>Pilih Model Mobil</option>\n        <option>AVANSA</option>\n        <option>ALPHARD</option>\n        <option>AGYA</option>\n      </select>\n    </div>\n\n    <div class="form-group">\n      <label for="exampleFormControlSelect1" class="text-muted">Pilih Tipe Mobil</label>\n      <select class="form-control" id="exampleFormControlSelect1">\n        <option disabled>Pilih Tipe Mobil</option>\n        <option>AVANZA 1.3 E A/T</option>\n        <option>AVANZA 1.3 E M/T</option>\n        <option>AVANZA 1.3 G A/T</option>\n        <option>AVANZA 1.3 G A/T LUX</option>\n      </select>\n    </div>\n\n    <div class="form-group">\n      <label for="exampleFormControlSelect1" class="text-muted">Tahun Produksi</label>\n      <select class="form-control" id="exampleFormControlSelect1">\n        <option disabled>Tahun Produksi</option>\n        <option>2005</option>\n        <option>2006</option>\n        <option>2007</option>\n        <option>2008</option>\n      </select>\n    </div>\n\n    <div class="form-group">\n      <label for="exampleFormControlSelect1" class="text-muted">Warna</label>\n      <select class="form-control" id="exampleFormControlSelect1">\n        <option disabled>Warna</option>\n        <option>Putih</option>\n        <option>Silver</option>\n        <option>Hitam</option>\n        <option>Coklat</option>\n      </select>\n    </div>\n\n    <div class="form-group">\n      <label for="exampleFormControlInput1" class="text-muted">Nomor Polisi</label>\n      <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Nomor Polisi">\n    </div>\n\n    <div class="form-group">\n      <label for="exampleFormControlInput1" class="text-muted">Nomor Identifikasi Kendaraan</label>\n      <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Nomor Identifikasi Kendaraan">\n    </div>\n  </div>\n\n  <div class="detail-stnk">\n    <h3>STNK Berlaku Sampai</h3>\n\n    <div class="row">\n      <div class="col-3">\n        <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="DD">\n      </div>\n      <div class="col-3">\n        <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="MM">\n      </div>\n      <div class="col-6">\n        <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="YYYY">\n      </div>\n    </div>\n  </div>\n\n  <div class="mt-5">\n    <button type="button" class="btn btn-lg btn-block btn-outline-primary" style="border-radius: 15px;">Tambah Kendaraan</button>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/extra/add_vehicle.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ViewController */]])
    ], AddVehiclePage);
    return AddVehiclePage;
}());

//# sourceMappingURL=add_vehicle.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var GlobalProvider = /** @class */ (function () {
    function GlobalProvider(http) {
        this.http = http;
        this.logoutEmitter = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* EventEmitter */]();
        console.log('Hello GlobalProvider Provider');
    }
    GlobalProvider.prototype.logout = function () {
        this.logoutEmitter.emit(true);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["O" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* EventEmitter */])
    ], GlobalProvider.prototype, "logoutEmitter", void 0);
    GlobalProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], GlobalProvider);
    return GlobalProvider;
}());

//# sourceMappingURL=global.js.map

/***/ }),

/***/ 114:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 114;

/***/ }),

/***/ 156:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 156;

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmergencyOrderSuccessPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__extra_status_pemesanan__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__emergency__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EmergencyOrderSuccessPage = /** @class */ (function () {
    function EmergencyOrderSuccessPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    EmergencyOrderSuccessPage.prototype.check_order = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__extra_status_pemesanan__["a" /* StatusPemesananPage */]);
    };
    EmergencyOrderSuccessPage.prototype.done = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__emergency__["a" /* EmergencyPage */]);
    };
    EmergencyOrderSuccessPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-emergency',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/emergency/emergency_order_success.html"*/'<ion-content>\n  <div class="mt-5">\n    <div class="success-section text-center">\n      <ion-icon ion-text name="checkmark-circle-outline" style="font-size: 10vh;" color="primary"></ion-icon>\n\n      <div class="desc-success">\n        <h3>Layanan Emergency Anda Berhasil, Pesanan Silahkan Melakukan Pembayaran Booking Fee kami.</h3>\n      </div>\n    </div>\n\n    <div class="m-5">\n      <button type="button" class="btn btn-lg btn-block btn-outline-primary" style="border-radius: 15px; padding: 10px 0px;" (click)="check_order()">Lihat Status Pesanan</button>\n      <button type="button" class="btn btn-lg btn-block btn-info" style="border-radius: 15px; padding: 10px 0px;" (click)="done()">Selesai</button>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/emergency/emergency_order_success.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], EmergencyOrderSuccessPage);
    return EmergencyOrderSuccessPage;
}());

//# sourceMappingURL=emergency_order_success.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatusPemesananPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StatusPemesananPage = /** @class */ (function () {
    function StatusPemesananPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    StatusPemesananPage.prototype.close = function (action) {
        if (action === void 0) { action = "cancel"; }
        this.viewCtrl.dismiss(action);
    };
    StatusPemesananPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-extra',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/extra/status_pemesanan.html"*/'<ion-content padding>\n\n  <div class="d-block">\n    <button ion-button clear color="white" icon-only (click)="close()" >\n      <h4><i class="bx bx-arrow-back mr-3"></i>Status Pemesanan Anda</h4>\n    </button>\n  </div>\n  <hr>\n\n  <div class="content">\n    <div class="detail-header">\n      <div class="row">\n        <div class="col-6">\n          <span class="text-muted">Order ID</span>\n          <br>\n          <h6 class="p-0 m-0">MTRID00012345</h6>\n        </div>\n        <div class="col-6">\n          <span class="text-muted">Status Order</span>\n          <br>\n          <ion-badge><h6 class="p-0 m-0">Pesanan Diterima</h6></ion-badge>\n        </div>\n      </div>\n      <div class="row">\n        <div class="col-6">\n          <span class="text-muted">Tipe Pesanan</span>\n          <br>\n          <h6 class="p-0 m-0">Emergency Servis</h6>\n        </div>\n        <!-- <div class="col-6">\n          <span class="text-muted">Tanggal Servis</span>\n          <br>\n          <h6 class="p-0 m-0">Emergency</h6>\n        </div> -->\n      </div>\n    </div>\n\n    <hr>\n\n    <div class="detail-pipeline">\n      <div class="pipeline-progress">\n        <ul id="progressbar">\n          <li class="active" id="recieve"></li>\n          <li class="active" id="process"></li>\n          <li class="active" id="waiting"></li>\n          <li class="active" id="ontheway"></li>\n          <li id="onprogress"></li>\n          <li id="done"></li>\n        </ul>\n        <h6 class="p-0 m-0 text-center">Staff Dalam Perjalanan</h6>\n      </div>\n\n      <br>\n      <hr>\n\n      <h3>Detail Pesanan</h3>\n      <div class="detail-pesanan">\n\n        <h5>Servis Berkala</h5>\n        <div>\n          <div class="d-block">\n            <span class="text-muted">\n              Servis Berkala 1000 KM\n            </span>\n            <br>\n            <span>Rp 1.999.000</span>\n          </div>\n          <hr>\n          <div class="d-block">\n            <span class="text-muted">\n              Perkiraan Harga\n            </span>\n            <br>\n            <h6>Rp 1.999.000</h6>\n            <small class="text-muted">* Harga hanya estimasi belum dengan tambahan-tambahan lain.</small>\n          </div>\n          <hr>\n          <div class="d-block">\n            <span class="text-muted">\n              Perkiraan Waktu Servis (Jam)\n            </span>\n            <br>\n            <h6>1.5</h6>\n          </div>\n          <hr>\n          <h4>Kendaraan yang diservis</h4>\n          <i class=\'bx bxs-car-mechanic\' style="font-size:70px; color: #999;"></i>\n          <div class="d-block">\n            <span class="text-muted">\n              Jenis Mobil\n            </span>\n            <br>\n            <h6>AVANZA</h6>\n          </div>\n          <div class="d-block">\n            <span class="text-muted">\n              Nomor Polisi\n            </span>\n            <br>\n            <h6>1234</h6>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/extra/status_pemesanan.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ViewController */]])
    ], StatusPemesananPage);
    return StatusPemesananPage;
}());

//# sourceMappingURL=status_pemesanan.js.map

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_detail__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.profile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__profile_detail__["a" /* ProfileDetailPage */]);
    };
    ProfilePage.prototype.notification = function () {
    };
    ProfilePage.prototype.booking_service = function () {
    };
    ProfilePage.prototype.my_vehicle = function () {
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/profile/profile.html"*/'<ion-content padding>\n\n  <ion-list>\n    <ion-item (click)="profile()" >\n      <ion-icon name="person" item-start></ion-icon>\n        Profil Saya\n      <i class=\'bx bxs-chevron-right\' ion-text item-end style="font-size: 28px;"></i>\n    </ion-item>\n\n    <ion-item (click)="notification()">\n      <ion-icon name="notifications-outline" item-start></ion-icon>\n        Notifikasi\n      <ion-badge item-end>2</ion-badge>\n      <i class=\'bx bxs-chevron-right\' ion-text item-end style="font-size: 28px;"></i>\n    </ion-item>\n\n    <ion-item (click)="booking_service()">\n      <ion-icon name="list" item-start></ion-icon>\n        Layanan Booking\n      <ion-badge item-end>2</ion-badge>\n      <i class=\'bx bxs-chevron-right\' ion-text item-end style="font-size: 28px;"></i>\n    </ion-item>\n\n    <ion-item (click)="my_vehicle()">\n      <ion-icon name="car" item-start></ion-icon>\n        Kendaraan Saya\n      <i class=\'bx bxs-chevron-right\' ion-text item-end style="font-size: 28px;"></i>\n    </ion-item>\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_global__ = __webpack_require__(102);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProfileDetailPage = /** @class */ (function () {
    function ProfileDetailPage(navCtrl, navParams, globalProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.globalProvider = globalProvider;
    }
    ProfileDetailPage.prototype.logout = function () {
        this.globalProvider.logout();
    };
    ProfileDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/profile/profile_detail.html"*/'<ion-content padding>\n\n  <div class="personal-info">\n    <h2>Informasi Pribadi</h2>\n    <div class="row">\n      <div class="col-12">\n        Nama Lengkap\n      </div>\n      <div class="col-12">\n        Customer Bogor\n      </div>\n    </div>\n\n    <div class="row">\n      <div class="col-12">\n        Nama Panggilan\n      </div>\n      <div class="col-12">\n        Mas Cust\n      </div>\n    </div>\n\n    <div class="row">\n      <div class="col-12">\n        Alamat\n      </div>\n      <div class="col-12">\n        Jalan Raya Bogor\n      </div>\n    </div>\n\n  </div>\n\n  <hr>\n\n  <div class="account-detail">\n    <h3>Detail Akun</h3>\n    <div class="row">\n      <div class="col-12">\n        Email\n      </div>\n      <div class="col-12">\n        customer@gmail.com\n      </div>\n    </div>\n\n    <div class="row">\n      <div class="col-12">\n        Password\n      </div>\n      <div class="col-12">\n        **********\n      </div>\n    </div>\n\n    <div class="row">\n      <div class="col-12">\n        Telepon\n      </div>\n      <div class="col-12">\n        +62 893216489126\n      </div>\n    </div>\n\n  </div>\n\n  <button type="button" class="btn btn-lg btn-block btn-outline-primary" style="border-radius: 15px;" (click)="logout()">Logout</button>\n\n\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/profile/profile_detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_global__["a" /* GlobalProvider */]])
    ], ProfileDetailPage);
    return ProfileDetailPage;
}());

//# sourceMappingURL=profile_detail.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__order_order_service__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__toko_toko__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__promo_promo__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__acara_berita_acara_berita__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__katalog_product_katalog_product__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__about_us_about_us__ = __webpack_require__(212);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MenuPage = /** @class */ (function () {
    function MenuPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.slider = [
            {
                text: 'Slide 1',
                img: '../../assets/image/banner.jpg'
            },
            {
                text: 'Slide 2',
                img: '../../assets/image/banner.jpg'
            },
            {
                text: 'Slide 3',
                img: '../../assets/image/banner.jpg'
            }
        ];
    }
    MenuPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        setInterval(function () {
            if (_this.slides.getActiveIndex() == (_this.slider.length - 1)) {
                _this.slides.slideTo(0, 1000);
            }
            else {
                _this.slides.slideNext(1000);
            }
        }, 3000);
    };
    MenuPage.prototype.booking_service = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__order_order_service__["a" /* OrderServicePage */]);
    };
    MenuPage.prototype.toko = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__toko_toko__["a" /* TokoPage */]);
    };
    MenuPage.prototype.promo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__promo_promo__["a" /* PromoPage */]);
    };
    MenuPage.prototype.acara_berita = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__acara_berita_acara_berita__["a" /* AcaraBeritaPage */]);
    };
    MenuPage.prototype.katalog_product = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__katalog_product_katalog_product__["a" /* KatalogProductPage */]);
    };
    MenuPage.prototype.about_us = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__about_us_about_us__["a" /* AboutUsPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Slides */]),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Slides */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Slides */]) === "function" && _a || Object)
    ], MenuPage.prototype, "slides", void 0);
    MenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-menu',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/menu/menu.html"*/'<ion-content>\n  <ion-slides class="h-auto">\n    <ion-slide *ngFor="let s of slider" >\n      <img src="{{s.img}}" alt="">\n    </ion-slide>\n  </ion-slides>\n\n  <div padding>\n    <div class="row">\n      <div class="col text-center">\n          <h5 class="subtitle mb-0">Most Exciting Feature</h5>\n          <p class="text-secondary">Take a look at our services</p>\n      </div>\n    </div>\n    <div class="row text-center mt-4 mb-3">\n      <div class="col-4 col-md-3 p-1">\n          <div class="card shadow border-0 mb-4">\n              <div class="card-body">\n                  <i class="bx bx-wrench mb-3 md-36 text-template" ion-text color="primary"></i>\n                  <p class="text-secondary text-mute p-menu">Workshop Service</p>\n              </div>\n          </div>\n      </div>\n      <div class="col-4 col-md-3 p-1">\n          <div class="card shadow border-0 mb-4">\n              <div class="card-body">\n                  <i class="bx bx-shopping-bag mb-3 md-36 text-template" ion-text color="primary"></i>\n                  \n                  <p class="text-secondary text-mute p-menu">Product Catalogue</p>\n              </div>\n          </div>\n      </div>\n      \n      <div class="col-4 col-md-3 p-1">\n          <div class="card shadow border-0 mb-4">\n              <div class="card-body">\n                  <i class="bx bx-phone-call mb-3 md-36 text-template" ion-text color="primary" ></i>\n                  <p class="text-secondary text-mute p-menu">Emergency Services</p>\n              </div>\n          </div>\n      </div>\n    </div>\n\n    <h6 class="subtitle ">News & Event <a href="" class="float-right small">View All</a></h6>\n    <div class="row">\n      <div class="col-6 col-md-4 p-2">\n        <div class="card mb-2 shadow-light border-0 card-news-update shadow-sm">\n          <div class="card-header text-center position-relative">\n              <div class="background-news" style="background-image: url(\'https://maxartkiller.com/wp-content/uploads/2020/05/businessdomain1.jpg\');">\n                  <img src="https://maxartkiller.com/wp-content/uploads/2020/05/businessdomain1.jpg" alt="" style="display: none;">\n              </div>\n          </div>\n          <div class="card-footer border-0 p-3">\n              <div class="media">\n                  <div class="media-body">\n                      <h6 class="mb-2">Komunitas Civron Peduli Korona</h6>\n                      <p class="mb-0 text-mute small ">\n                        Lorem ipsum dolor sit amet consectetur adipisicing elit\n                      </p>\n                  </div>\n              </div>\n          </div>\n        </div>\n      </div>\n      <div class="col-6 col-md-4 p-2">\n        <div class="card mb-2 shadow-light border-0 card-news-update shadow-sm">\n          <div class="card-header text-center position-relative">\n              <div class="background-news" style="background-image: url(\'https://maxartkiller.com/wp-content/uploads/2020/05/businessdomain1.jpg\');">\n                  <img src="https://maxartkiller.com/wp-content/uploads/2020/05/businessdomain1.jpg" alt="" style="display: none;">\n              </div>\n          </div>\n          <div class="card-footer border-0 p-3">\n              <div class="media">\n                  <div class="media-body">\n                      <h6 class="mb-2">Komunitas Civron Peduli Korona</h6>\n                      <p class="mb-0 text-mute small ">\n                        Lorem ipsum dolor sit amet consectetur adipisicing elit\n                      </p>\n                  </div>\n              </div>\n          </div>\n        </div>\n      </div>\n      <div class="col-6 col-md-4 p-2">\n        <div class="card mb-2 shadow-light border-0 card-news-update shadow-sm">\n          <div class="card-header text-center position-relative">\n              <div class="background-news" style="background-image: url(\'https://maxartkiller.com/wp-content/uploads/2020/05/businessdomain1.jpg\');">\n                  <img src="https://maxartkiller.com/wp-content/uploads/2020/05/businessdomain1.jpg" alt="" style="display: none;">\n              </div>\n          </div>\n          <div class="card-footer border-0 p-3">\n              <div class="media">\n                  <div class="media-body">\n                      <h6 class="mb-2">Komunitas Civron Peduli Korona</h6>\n                      <p class="mb-0 text-mute small ">\n                        Lorem ipsum dolor sit amet consectetur adipisicing elit\n                      </p>\n                  </div>\n              </div>\n          </div>\n        </div>\n      </div>\n      <div class="col-6 col-md-4 p-2">\n        <div class="card mb-2 shadow-light border-0 card-news-update shadow-sm">\n          <div class="card-header text-center position-relative">\n              <div class="background-news" style="background-image: url(\'https://maxartkiller.com/wp-content/uploads/2020/05/businessdomain1.jpg\');">\n                  <img src="https://maxartkiller.com/wp-content/uploads/2020/05/businessdomain1.jpg" alt="" style="display: none;">\n              </div>\n          </div>\n          <div class="card-footer border-0 p-3">\n              <div class="media">\n                  <div class="media-body">\n                      <h6 class="mb-2">Komunitas Civron Peduli Korona</h6>\n                      <p class="mb-0 text-mute small ">\n                        Lorem ipsum dolor sit amet consectetur adipisicing elit\n                      </p>\n                  </div>\n              </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  \n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/menu/menu.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]) === "function" && _c || Object])
    ], MenuPage);
    return MenuPage;
    var _a, _b, _c;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderServicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__order_success__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__extra_add_vehicle__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OrderServicePage = /** @class */ (function () {
    function OrderServicePage(navCtrl, navParams, viewCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.modalCtrl = modalCtrl;
        this.prev = false;
        this.next = true;
        this.changed = false;
        this.routine_service = false;
        this.usual_service = false;
        this.section1 = true;
        this.section2 = false;
        this.section3 = false;
        this.currentSection = 1;
    }
    OrderServicePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OrderPage');
    };
    OrderServicePage.prototype.slideChanged = function () {
        this.changed = true;
        var k = this.slides.getActiveIndex();
        this.currentVehicle = k;
    };
    OrderServicePage.prototype.setVehicle = function (k) {
        this.vehicle = k;
        this.currentVehicle = k;
    };
    OrderServicePage.prototype.slidePrev = function () {
        this.slides.slidePrev(1000);
        if (this.slides.isBeginning()) {
            this.prev = false;
        }
        else {
            this.prev = true;
        }
    };
    OrderServicePage.prototype.slideNext = function () {
        this.slides.slideNext(1000);
        if (this.slides.isEnd()) {
            this.next = false;
        }
        else {
            this.next = true;
        }
    };
    OrderServicePage.prototype.finish_section1 = function () {
        this.section1 = false;
        this.section2 = true;
        this.currentSection = 2;
    };
    OrderServicePage.prototype.finish_section2 = function () {
        this.section2 = false;
        this.section3 = true;
        this.currentSection = 3;
    };
    OrderServicePage.prototype.finish_section3 = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__order_success__["a" /* OrderSuccessPage */]);
    };
    OrderServicePage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    OrderServicePage.prototype.activateSection = function (sk) {
        console.log(sk);
        if (sk < this.currentSection) {
            if (this.currentSection == 2) {
                this.section2 = false;
                this.section1 = true;
            }
            else if (this.currentSection == 3) {
                this.section3 = false;
                this.section2 = true;
            }
        }
    };
    OrderServicePage.prototype.add_vehicle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__extra_add_vehicle__["a" /* AddVehiclePage */]);
        modal.present();
        modal.onDidDismiss(function (data) {
            console.log('refresh kendaraan');
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Slides */])
    ], OrderServicePage.prototype, "slides", void 0);
    OrderServicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-order',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/order/order_service.html"*/'<ion-content padding>\n\n  <div class="d-block">\n    <button ion-button clear color="white" icon-only (click)="close()" >\n      <h4><i class="bx bx-arrow-back mr-3"></i>Layanan Bengkel</h4>\n    </button>\n  </div>\n\n\n  <div class="main-content">\n\n    <div class="section-1">\n\n      <div class="header-section-detail" style="border-bottom: 1px solid #eee;" (click)="activateSection(1)" >\n        <h2>Kendaraan & Layanan</h2>\n      </div>\n\n      <div class="section1-detail" *ngIf="section1">\n        <div class="step-1">\n          <div class="slider-vehicle d-flex" style="position: relative;">\n            <i class=\'bx bx-left-arrow-circle navigation-slider-left\' (click)="slidePrev()"></i>\n            <ion-slides style="height: 30vh;" (ionSlideDidChange)="slideChanged()">\n              <ion-slide>\n                <i class=\'bx bx-check vehicle-check\' *ngIf="vehicle == 0"></i>\n                <img src="../../assets/image/car.png" class="card-img" style="width: 40vw;" (click)="setVehicle(0)" >\n              </ion-slide>\n              <ion-slide>\n                <i class=\'bx bx-check vehicle-check\' *ngIf="vehicle == 1"></i>\n                <img src="../../assets/image/car2.png" class="card-img" style="width: 40vw;" (click)="setVehicle(1)">\n              </ion-slide>\n              <ion-slide>\n                <i class=\'bx bx-check vehicle-check\' *ngIf="vehicle == 2"></i>\n                <img src="../../assets/image/car3.png" class="card-img" style="width: 40vw;" (click)="setVehicle(2)">\n              </ion-slide>\n\n              <ion-slide class="add-vehicle-slide">\n                <div class="d-flex" (click)="add_vehicle()">\n                  <div class="m-auto">\n                    <h3><i class=\'bx bx-plus\'></i></h3>\n                    <h6>Tambah Kendaraan</h6>\n                  </div>\n                </div>\n              </ion-slide>\n            </ion-slides>\n            <i class=\'bx bx-right-arrow-circle navigation-slider-right\' (click)="slideNext()"></i>\n          </div>\n        </div>\n\n        <hr>\n\n        <div class="step-2" *ngIf="(currentVehicle == vehicle) && currentVehicle !== undefined && vehicle !== undefined">\n          <p>Pilih layanan yg anda perlukan</p>\n\n          <div class="form-group form-check">\n            <input type="checkbox" class="form-check-input" [(ngModel)]="routine_service" [ngModelOptions]="{standalone:true}">\n            <label class="form-check-label">Servis Berkala</label>\n          </div>\n\n          <div class="form-group" *ngIf="routine_service">\n            <select class="form-control" id="exampleFormControlSelect1">\n              <option disabled> <i>Pilih Jarak Tempuh</i> </option>\n              <option> Servis Berkala 1000 KM</option>\n              <option> Servis Berkala 1000 KM</option>\n              <option> Servis Berkala 1000 KM</option>\n              <option> Servis Berkala 1000 KM</option>\n              <option> Servis Berkala 1000 KM</option>\n              <option> Servis Berkala 1000 KM</option>\n            </select>\n          </div>\n\n          <div class="form-group form-check">\n            <input type="checkbox" class="form-check-input" [(ngModel)]="usual_service" [ngModelOptions]="{standalone:true}">\n            <label class="form-check-label">Servis Biasa</label>\n          </div>\n\n          <div class="usual-service" *ngIf="usual_service">\n            <div class="form-group form-check list-usual-service">\n              <input type="checkbox" class="form-check-input mt-2" id="e1">\n              <div class="detail-service pl-3">\n                <label class="form-check-label">Ganti Oli</label>\n                <br>\n                <label class="form-check-label">Rp 900.000</label>\n              </div>\n            </div>\n\n            <div class="form-group form-check list-usual-service">\n              <input type="checkbox" class="form-check-input mt-2">\n              <div class="detail-service pl-3">\n                <label class="form-check-label">Servis Rem</label>\n                <br>\n                <label class="form-check-label">Rp 700.000</label>\n              </div>\n            </div>\n\n            <div class="form-group form-check list-usual-service">\n              <input type="checkbox" class="form-check-input mt-2">\n              <div class="detail-service pl-3">\n                <label class="form-check-label">Ganti Kanvas Rem</label>\n                <br>\n                <label class="form-check-label">Rp 800.000</label>\n              </div>\n            </div>\n          </div>\n\n          <!-- Estimasi waktu dan harga -->\n          <div class="estimate" *ngIf="usual_service || routine_service">\n            <div class="card text-white bg-secondary mb-3">\n              <div class="card-header">Rp 9.000.000</div>\n              <div class="card-body">\n                <h5 class="card-title">Perkiraan Harga</h5>\n                <p class="card-text">Harga hanya estimasi belum dengan tambahan lain lain</p>\n              </div>\n            </div>\n\n            <div class="card text-white bg-secondary mb-3">\n              <div class="card-header">5.5 Jam Estimasi</div>\n              <div class="card-body">\n                <h5 class="card-title">Estimasi Waktu Servis</h5>\n                <p class="card-text">Hanya estimasi</p>\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <div class="step-3">\n          <button type="button" class="btn btn-lg btn-block {{ (!usual_service && !routine_service) ? \'btn-outline-danger\' : \'btn-outline-primary\'}}" style="border-radius: 15px;" [disabled]="!usual_service && !routine_service" (click)="finish_section1()">Lanjut</button>\n        </div>\n      </div>\n    </div>\n\n    <!-- Waktu Booking -->\n    <div class="section-2" >\n      <div class="header-section-detail" style="border-bottom: 1px solid #eee;" (click)="activateSection(2)">\n        <h2>Waktu</h2>\n      </div>\n\n      <div class="section2-detail" *ngIf="section2">\n\n        <div class="form-group">\n          <label for="exampleFormControlSelect1">Pilih Tempat</label>\n          <select class="form-control" id="exampleFormControlSelect1">\n            <option disabled>Pilih Kota</option>\n            <option>Mentari 1</option>\n          </select>\n        </div>\n\n        <div class="form-group">\n          <label for="exampleFormControlSelect1">Pilih Tanggal</label>\n          <select class="form-control" id="exampleFormControlSelect1">\n            <option disabled>Pilih Tanggal</option>\n            <option>2020-08-12</option>\n            <option>2020-08-13</option>\n            <option>2020-08-14</option>\n            <option>2020-08-15</option>\n            <option>2020-08-16</option>\n          </select>\n        </div>\n\n        <div class="form-group">\n          <label for="exampleFormControlSelect1">Pilih Waktu</label>\n          <select class="form-control" id="exampleFormControlSelect1">\n            <option disabled>Pilih Waktu</option>\n            <option>08:00</option>\n            <option>08:30</option>\n            <option>09:00</option>\n            <option>09:30</option>\n            <option>10:00</option>\n            <option>10:30</option>\n            <option>11:00</option>\n            <option>11:30</option>\n          </select>\n        </div>\n\n        <button type="button" class="btn btn-lg btn-block btn-outline-primary" style="border-radius: 15px;" (click)="finish_section2()">Lanjut</button>\n\n      </div>\n    </div>\n\n    <!-- Info User Pemesan -->\n    <div class="section-3">\n      <div class="header-section-detail" style="border-bottom: 1px solid #eee;" (click)="activateSection(3)">\n        <h2>Infomasi Anda</h2>\n      </div>\n\n      <div class="section2-detail" *ngIf="section3">\n        <div class="form-group">\n          <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Nama Lengkap">\n        </div>\n\n        <div class="input-group mb-3">\n          <div class="input-group-prepend">\n            <span class="input-group-text" id="basic-addon1">+62</span>\n          </div>\n          <input type="text" class="form-control" placeholder="Nomor Telepon" aria-describedby="basic-addon1">\n        </div>\n\n        <div class="form-group">\n          <input type="text" class="form-control" id="exampleFormControlInput1" value="customer@gmail.com" disabled>\n        </div>\n\n        <div class="form-group form-check">\n          <input type="checkbox" class="form-check-input" id="exampleCheck1">\n          <label class="form-check-label ml-2" for="exampleCheck1">Saya setuju dengan <span style="color: red;">Syarat & Ketentuan</span> YOUR COMPANY</label>\n        </div>\n\n        <button type="button" class="btn btn-lg btn-block btn-outline-primary" style="border-radius: 15px;" (click)="finish_section3()">Pesan Sekarang</button>\n\n      </div>\n    </div>\n\n  </div>\n\n\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/order/order_service.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* ModalController */]])
    ], OrderServicePage);
    return OrderServicePage;
}());

//# sourceMappingURL=order_service.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderSuccessPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OrderSuccessPage = /** @class */ (function () {
    function OrderSuccessPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    OrderSuccessPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OrderPage');
    };
    OrderSuccessPage.prototype.check_order = function () {
    };
    OrderSuccessPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-order',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/order/order_success.html"*/'<ion-content padding>\n\n<div class="mb-5">\n  <div class="success-section text-center">\n    <ion-icon ion-text name="checkmark-circle-outline" style="font-size: 10vh;" color="primary"></ion-icon>\n\n    <div class="desc-success">\n      <h3>Booking layanan bengkel berhasil, pesanan Silahkan Melakukan Pembayaran Booking Fee kami.</h3>\n    </div>\n  </div>\n\n  <hr>\n\n  <h3> Rincian booking layanan yg dipilih</h3>\n\n  <div class="row">\n    <div class="col-12">\n      <p>Tanggal</p>\n    </div>\n    <div class="col-12">\n      <h6>02-02-2020</h6>\n    </div>\n  </div>\n  <br>\n  <div class="row">\n    <div class="col-12">\n      <p>Waktu</p>\n    </div>\n    <div class="col-12">\n      <h6>11:12</h6>\n    </div>\n  </div>\n\n  <hr>\n\n  <div class="detail-payment">\n    <h3>Detail Pembayaran</h3>\n\n    <p>Silahkan melakukan pembayaran ke rekening berikut:</p>\n\n    <div class="row">\n      <div class="col-12">\n        <p class="p-0 m-0">Nama Bank</p>\n        <h4 class="p-0 m-0">BCA</h4>\n      </div>\n    </div>\n    <br>\n    <div class="row">\n      <div class="col-12">\n        <p class="p-0 m-0">Atas nama</p>\n        <h4 class="p-0 m-0">PT YOUR COMPANY</h4>\n      </div>\n    </div>\n    <br>\n    <div class="row">\n      <div class="col-12">\n        <p class="p-0 m-0">Nomor Rekening</p>\n        <h4 class="p-0 m-0">123456789 <i class=\'bx bx-copy\' ></i></h4>\n      </div>\n    </div>\n    <br>\n\n    <div class="row">\n      <div class="col-12">\n        <p class="p-0 m-0">Total Pembayaran</p>\n        <h4 class="p-0 m-0">Rp 1.999.900</h4>\n      </div>\n    </div>\n\n  </div>\n\n  <hr>\n\n  <button type="button" class="btn btn-lg btn-block btn-outline-primary" style="border-radius: 15px;" (click)="check_order()">Lihat Riwayat Pesanan</button>\n\n</div>\n\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/order/order_success.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], OrderSuccessPage);
    return OrderSuccessPage;
}());

//# sourceMappingURL=order_success.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TokoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TokoPage = /** @class */ (function () {
    function TokoPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    TokoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TokoPage');
    };
    TokoPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    TokoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-toko',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/toko/toko.html"*/'<ion-content padding>\n  <div class="d-block">\n    <button ion-button clear color="white" icon-only (click)="close()" >\n      <h4><i class="bx bx-arrow-back mr-3"></i>Toko</h4>\n    </button>\n  </div>\n\n  <div class="content">\n    <div class="row">\n      <div class="col-6">\n        <small class="text-muted">Total Harga (1 item)</small>\n        <h6 class="p-0 m-0"><b>Rp 1.999.009</b></h6>\n      </div>\n      <div class="col-6">\n        <button type="button" class="btn btn-lg btn-block btn-outline-primary" style="border-radius: 15px; padding: 10px 5px;" >Check Out</button>\n      </div>\n    </div>\n\n    <hr>\n\n    <div class="products-list">\n      <div class="row">\n        <div class="col-6">\n          <div class="card">\n            <img src="../../assets/image/car.png" class="card-img-top">\n            <div class="card-body text-center">\n              <h5 class="card-title text-muted">Mobil Avanza 1.3 E A/T</h5>\n              <p class="card-text"><b>Rp 100.000.000</b></p>\n            </div>\n            <div class="card-footer text-muted">\n              <button type="button" class="btn btn-lg btn-block btn-outline-primary" style="border-radius: 15px; padding: 5px 5px;" >Tambahkan</button>\n            </div>\n          </div>\n        </div>\n\n        <div class="col-6">\n          <div class="card">\n            <img src="../../assets/image/car.png" class="card-img-top">\n            <div class="card-body text-center">\n              <h5 class="card-title text-muted">Mobil Avanza 1.3 E A/T</h5>\n              <p class="card-text"><b>Rp 100.000.000</b></p>\n            </div>\n            <div class="card-footer text-muted">\n              <button type="button" class="btn btn-lg btn-block btn-outline-primary" style="border-radius: 15px; padding: 5px 5px;" >Tambahkan</button>\n            </div>\n          </div>\n        </div>\n\n        <div class="col-6">\n          <div class="card">\n            <img src="../../assets/image/car.png" class="card-img-top">\n            <div class="card-body text-center">\n              <h5 class="card-title text-muted">Mobil Avanza 1.3 E A/T</h5>\n              <p class="card-text"><b>Rp 100.000.000</b></p>\n            </div>\n            <div class="card-footer text-muted">\n              <button type="button" class="btn btn-lg btn-block btn-outline-primary" style="border-radius: 15px; padding: 5px 5px;" >Tambahkan</button>\n            </div>\n          </div>\n        </div>\n\n        <div class="col-6">\n          <div class="card">\n            <img src="../../assets/image/car.png" class="card-img-top">\n            <div class="card-body text-center">\n              <h5 class="card-title text-muted">Mobil Avanza 1.3 E A/T</h5>\n              <p class="card-text"><b>Rp 100.000.000</b></p>\n            </div>\n            <div class="card-footer text-muted">\n              <button type="button" class="btn btn-lg btn-block btn-outline-primary" style="border-radius: 15px; padding: 5px 5px;" >Tambahkan</button>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/toko/toko.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ViewController */]])
    ], TokoPage);
    return TokoPage;
}());

//# sourceMappingURL=toko.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PromoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PromoPage = /** @class */ (function () {
    function PromoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PromoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PromoPage');
    };
    PromoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-promo',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/promo/promo.html"*/'<ion-content padding>\n  <div class="row">\n    <div class="col-12 mb-4">\n      <div class="card">\n        <img src="../../assets/image/promo.png" class="card-img-top">\n        <div class="card-body">\n          <h5 class="card-title text-muted">Promo 50%</h5>\n          <p class="card-text">\n            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Temporibus fugit eos quos nobis consequatur sapiente, dignissimos quam sed autem obcaecati a voluptate! Deleniti, magni. Qui quod tempora pariatur ea saepe!\n          </p>\n        </div>\n      </div>\n    </div>\n\n    <div class="col-12 mb-4">\n      <div class="card">\n        <img src="../../assets/image/promo2.png" class="card-img-top">\n        <div class="card-body">\n          <h5 class="card-title text-muted">Promo 70% Daihatsu</h5>\n          <p class="card-text">\n            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Temporibus fugit eos quos nobis consequatur sapiente, dignissimos quam sed autem obcaecati a voluptate! Deleniti, magni. Qui quod tempora pariatur ea saepe!\n          </p>\n        </div>\n      </div>\n    </div>\n\n    <div class="col-12 mb-4">\n      <div class="card">\n        <img src="../../assets/image/promo3.png" class="card-img-top">\n        <div class="card-body">\n          <h5 class="card-title text-muted">Promo Super Maret</h5>\n          <p class="card-text">\n            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Temporibus fugit eos quos nobis consequatur sapiente, dignissimos quam sed autem obcaecati a voluptate! Deleniti, magni. Qui quod tempora pariatur ea saepe!\n          </p>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/promo/promo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], PromoPage);
    return PromoPage;
}());

//# sourceMappingURL=promo.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AcaraBeritaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AcaraBeritaPage = /** @class */ (function () {
    function AcaraBeritaPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    AcaraBeritaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PromoPage');
    };
    AcaraBeritaPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    AcaraBeritaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-acara-berita',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/acara_berita/acara_berita.html"*/'<ion-content padding>\n\n  <div class="d-block">\n    <button ion-button clear color="white" icon-only (click)="close()" >\n      <h4><i class="bx bx-arrow-back mr-3"></i>Acara & Berita</h4>\n    </button>\n  </div>\n\n  <div class="row">\n    <div class="col-12 mb-4">\n      <div class="card">\n        <img src="../../assets/image/acara.jpg" class="card-img-top">\n        <div class="card-body">\n          <h5 class="card-title text-muted">Acara Bakti Sosial</h5>\n          <p class="card-text">\n            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Temporibus fugit eos quos nobis consequatur sapiente, dignissimos quam sed autem obcaecati a voluptate! Deleniti, magni. Qui quod tempora pariatur ea saepe!\n          </p>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/acara_berita/acara_berita.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ViewController */]])
    ], AcaraBeritaPage);
    return AcaraBeritaPage;
}());

//# sourceMappingURL=acara_berita.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KatalogProductPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var KatalogProductPage = /** @class */ (function () {
    function KatalogProductPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    KatalogProductPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    KatalogProductPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-katalog-product',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/katalog-product/katalog-product.html"*/'<ion-content padding>\n\n  <div class="d-block">\n    <button ion-button clear color="white" icon-only (click)="close()" >\n      <h4><i class="bx bx-arrow-back mr-3"></i>Katalog Produk</h4>\n    </button>\n  </div>\n\n  <div class="products-list">\n    <div class="row">\n      <div class="col-6">\n        <div class="card">\n          <img src="../../assets/image/car.png" class="card-img-top">\n          <div class="card-body text-center">\n            <h5 class="card-title text-muted">Mobil Avanza 1.3 E A/T</h5>\n            <p class="card-text"><b>Rp 100.000.000</b></p>\n          </div>\n        </div>\n      </div>\n\n      <div class="col-6">\n        <div class="card">\n          <img src="../../assets/image/car.png" class="card-img-top">\n          <div class="card-body text-center">\n            <h5 class="card-title text-muted">Mobil Avanza 1.3 E A/T</h5>\n            <p class="card-text"><b>Rp 100.000.000</b></p>\n          </div>\n        </div>\n      </div>\n\n      <div class="col-6">\n        <div class="card">\n          <img src="../../assets/image/car.png" class="card-img-top">\n          <div class="card-body text-center">\n            <h5 class="card-title text-muted">Mobil Avanza 1.3 E A/T</h5>\n            <p class="card-text"><b>Rp 100.000.000</b></p>\n          </div>\n        </div>\n      </div>\n\n      <div class="col-6">\n        <div class="card">\n          <img src="../../assets/image/car.png" class="card-img-top">\n          <div class="card-body text-center">\n            <h5 class="card-title text-muted">Mobil Avanza 1.3 E A/T</h5>\n            <p class="card-text"><b>Rp 100.000.000</b></p>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/katalog-product/katalog-product.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ViewController */]])
    ], KatalogProductPage);
    return KatalogProductPage;
}());

//# sourceMappingURL=katalog-product.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutUsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AboutUsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AboutUsPage = /** @class */ (function () {
    function AboutUsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AboutUsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutUsPage');
    };
    AboutUsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about-us',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/about-us/about-us.html"*/'<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/about-us/about-us.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], AboutUsPage);
    return AboutUsPage;
}());

//# sourceMappingURL=about-us.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.login = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/login/login.html"*/'<ion-content>\n\n  <div class="row no-gutters vh-100 proh bg-template">\n    <img src="img/apple.png" alt="logo" class="apple right-image align-self-center">\n    <div class="col align-self-center px-3 text-center">\n        <img src="img/logo.png" alt="logo" class="logo-small">\n        <h2 class="text-white "><span class="font-weight-light">Sign</span>In</h2>\n        <form class="form-signin shadow">\n            <div class="form-group float-label">\n                <input type="email" id="inputEmail" class="form-control" required="" autofocus="">\n                <label for="inputEmail" class="form-control-label">Email address</label>\n            </div>\n\n            <div class="form-group float-label">\n                <input type="password" id="inputPassword" class="form-control" required="">\n                <label for="inputPassword" class="form-control-label">Password</label>\n            </div>\n\n            <div class="form-group my-4 text-left">\n                <div class="custom-control custom-checkbox">\n                    <input type="checkbox" class="custom-control-input" id="rememberme">\n                    <label class="custom-control-label" for="rememberme">Remember Me</label>\n                </div>\n            </div>\n\n            <div class="row">\n                <div class="col-auto">\n                    <a href="index-2.html" class="btn btn-lg btn-default btn-rounded shadow"><span>Sign in</span><i class="material-icons">arrow_forward</i></a>\n                </div>\n                <div class="col align-self-center text-right pl-0">\n                    <a href="forgot-password.html">Forgot Password?</a>\n                </div>\n            </div>\n        </form>\n        <p class="text-center text-white">\n            Don\'t have account yet?<br>\n            <a href="signup.html">Sign Up</a> here.\n        </p>\n    </div>\n</div>\n\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(235);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_menu_menu__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_profile_profile__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_emergency_emergency__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_order_order_service__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_global_global__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_order_order_success__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile_detail__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_login_login__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_common_http__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_extra_add_vehicle__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_emergency_emergency_order_success__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_extra_status_pemesanan__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_toko_toko__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_promo_promo__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_acara_berita_acara_berita__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_katalog_product_katalog_product__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_about_us_about_us__ = __webpack_require__(212);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_profile_profile__["a" /* ProfilePage */],
                // Order
                __WEBPACK_IMPORTED_MODULE_10__pages_order_order_service__["a" /* OrderServicePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_order_order_success__["a" /* OrderSuccessPage */],
                // Profile
                __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile_detail__["a" /* ProfileDetailPage */],
                // Login
                __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */],
                // EXTRA
                __WEBPACK_IMPORTED_MODULE_16__pages_extra_add_vehicle__["a" /* AddVehiclePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_extra_status_pemesanan__["a" /* StatusPemesananPage */],
                // EMERGENCY
                __WEBPACK_IMPORTED_MODULE_9__pages_emergency_emergency__["a" /* EmergencyPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_emergency_emergency_order_success__["a" /* EmergencyOrderSuccessPage */],
                // Toko
                __WEBPACK_IMPORTED_MODULE_19__pages_toko_toko__["a" /* TokoPage */],
                // Promo
                __WEBPACK_IMPORTED_MODULE_20__pages_promo_promo__["a" /* PromoPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_acara_berita_acara_berita__["a" /* AcaraBeritaPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_katalog_product_katalog_product__["a" /* KatalogProductPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_about_us_about_us__["a" /* AboutUsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_15__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], { scrollPadding: false }, {
                    links: [
                        { component: __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */], name: 'LoginPage', segment: 'login' },
                        { component: __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */], name: 'HomePage', segment: 'home' },
                        { component: __WEBPACK_IMPORTED_MODULE_23__pages_about_us_about_us__["a" /* AboutUsPage */], name: 'AboutUsPage', segment: 'aboutus' },
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_profile_profile__["a" /* ProfilePage */],
                // Order
                __WEBPACK_IMPORTED_MODULE_10__pages_order_order_service__["a" /* OrderServicePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_order_order_success__["a" /* OrderSuccessPage */],
                // Profile
                __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile_detail__["a" /* ProfileDetailPage */],
                // Login
                __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */],
                // EXTRA
                __WEBPACK_IMPORTED_MODULE_16__pages_extra_add_vehicle__["a" /* AddVehiclePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_extra_status_pemesanan__["a" /* StatusPemesananPage */],
                // EMERGENCY
                __WEBPACK_IMPORTED_MODULE_9__pages_emergency_emergency__["a" /* EmergencyPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_emergency_emergency_order_success__["a" /* EmergencyOrderSuccessPage */],
                // Toko
                __WEBPACK_IMPORTED_MODULE_19__pages_toko_toko__["a" /* TokoPage */],
                // promo
                __WEBPACK_IMPORTED_MODULE_20__pages_promo_promo__["a" /* PromoPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_acara_berita_acara_berita__["a" /* AcaraBeritaPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_katalog_product_katalog_product__["a" /* KatalogProductPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_about_us_about_us__["a" /* AboutUsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_11__providers_global_global__["a" /* GlobalProvider */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] },
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(99);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* enableProdMode */])();
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__emergency_emergency__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__profile_profile__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__menu_menu__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_global_global__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(213);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, globalProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.globalProvider = globalProvider;
        this.tabMenu = __WEBPACK_IMPORTED_MODULE_2__menu_menu__["a" /* MenuPage */];
        this.tabProfile = __WEBPACK_IMPORTED_MODULE_1__profile_profile__["a" /* ProfilePage */];
        this.tabEmergency = __WEBPACK_IMPORTED_MODULE_0__emergency_emergency__["a" /* EmergencyPage */];
        this.globalProvider.logoutEmitter.subscribe(function (data) {
            if (data == true) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* LoginPage */]);
            }
        });
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["A" /* Injectable */])(),
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/home/home.html"*/'<ion-header class="shadow">\n  <ion-navbar color="">\n    <ion-title start>\n      CHEVICARE OTO\n    </ion-title>\n    <ion-buttons end >\n      <button ion-button icon-only class="mr-2">\n        <i class="bx bx-info-circle h2 mb-0"></i>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-tabs color="primary">\n    <ion-tab tabIcon="home"  [root]="tabMenu" tabTitle="Home"></ion-tab>\n    <ion-tab tabIcon="paper" [root]="tabEmergency"  tabTitle="Activity"></ion-tab>\n    <ion-tab tabIcon="person" [root]="tabProfile"  tabTitle="Profile"></ion-tab>\n  </ion-tabs>`\n</ion-content>\n'/*ion-inline-end:"/Applications/XAMPP/xamppfiles/htdocs/mentari_motor/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_global_global__["a" /* GlobalProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

},[214]);
//# sourceMappingURL=main.js.map