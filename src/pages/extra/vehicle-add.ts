import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController, LoadingController, Loading } from 'ionic-angular';
import { RestProvider } from '../../providers/rest';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalProvider } from '../../providers/global';
import { SAutoCompletePage } from './s_autocomplete';
import { AttachmentPage } from './attachments';
import { PhotoViewer } from '@ionic-native/photo-viewer';

@Component({
  selector: 'page-extra',
  templateUrl: 'vehicle-add.html',
})
export class VehicleAddPage {

  fields:any = JSON.parse(window.localStorage.getItem("fields_vehicle"));

  @ViewChild('myForm') myForm: ElementRef;
  createForm:FormGroup;
  formFields:any            = {};
  required:any              = [];

  data:any                  = {};
  data_label:any            = {};
  loading : Loading;
  imageData:any             = undefined;

  constructor(
    public navCtrl        : NavController,
    public navParams      : NavParams,
    public viewCtrl       : ViewController,
    public rest           : RestProvider,
    public builder        : FormBuilder,
    public global         : GlobalProvider,
    public modalCtrl      : ModalController,
    public loadingCtrl    : LoadingController,
    public photoViewer    : PhotoViewer,
  ) {
    this.validateDefined();
    this.getForm();
  }

  validateDefined()
  {
    var fields = this.fields;
    var fieldName: string = "";
    var fieldValidation: string = "";

    if (fields != undefined)
    {
      for(let row of fields)
      {
        if (typeof row['cst_crs_f_name'] != "undefined" || row['cst_crs_f_name'] != null)
        {
         fieldName         = row['cst_crs_f_name'];
         fieldValidation   = row['cst_crs_f_validation'];
        }

        this.required = [];

        let temp_validation = [];
        if ( fieldValidation != null && fieldValidation.indexOf("required") > -1 ) {
          temp_validation.push(Validators.required);
        }
        let pattern = this.global.validatorPattern(fieldValidation);
        if( pattern != false ) {
          temp_validation.push(Validators.pattern(pattern.regex));
        }
        this.required = ['', Validators.compose(temp_validation)];

        this.formFields[fieldName] = this.required;
      }
    }
    this.createForm = this.builder.group(this.formFields);
    console.log(this.createForm.valid, this.createForm);
    
  }

  getForm()
  {
    this.rest.getFormVehicle()
    .then(result => {
      this.fields = result['data'];
      this.validateDefined();
    })
  }

  close(action="cancel")
  {
    this.viewCtrl.dismiss(action);
  }

  sAutoComplete(fields_name, fields_label)
  {
    let params:any = {
      fields_name   : fields_name,
      fields_label  : fields_label, 
    }

    if (fields_name == 'crs_model_id') 
    {
      params.module = 'crs_model';
    }
    else if(fields_name == 'crs_brand_id')
    {
      params.module = 'crs_brand';
    }

    if (fields_name == 'crs_model_id') 
    {
      params.brand_name = this.data_label['crs_brand_id_label'];  
      params.brand_id = this.data['crs_brand_id'];  
    }

    let modal = this.modalCtrl.create(SAutoCompletePage, params);

    modal.present();

    modal.onDidDismiss(data => {
      if (data !== undefined) 
      {
        if (fields_name == 'crs_brand_id') 
        {
          this.data.crs_brand_id = data.value;  
          this.data_label.crs_brand_id_label = data.label;  

          this.data.crs_model_id = null;  
          this.data_label.crs_model_id_label = "";  
        }
        if (fields_name == 'crs_model_id') 
        {
          this.data.crs_model_id = data.value;  
          this.data_label.crs_model_id_label = data.label;  
        }  
      }
    })
  }

  storeData()
  {
    this.loading = this.loadingCtrl.create({
      content : "Menyimpan..."
    });

    this.loading.present();
    this.rest.saveVehicle(this.data)
    .then(result => {
      this.loading.dismiss();
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        let uuid = result['data']['customers_cars']['cst_crs_uuid'];
        if (this.imageData !== undefined) 
        {
          this.rest.uploadImage(this.imageData)
          .then(res => {
            let cont = {
              cst_crs_photo : res['file'],
            }
            this.rest.saveEditVehicle(cont, uuid)
            .then(rest => {
              if (typeof rest['status'] !== undefined && rest['status'] == 200) 
              {
                this.global.toastDefault("Gambar Mobil Berhasil Di Upload");
                let data = {
                  url : res['file'],
                  uuid : uuid
                }
                this.global.updateImageDirect(data);
              }
              else
              {
                this.global.toastDanger("Gambar Mobil Gagal Di Upload!");
              }
            })
          });  
        }
        this.close('saved');
      }
      else
      {
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
    
  }

  addImage()
  {
    let modal = this.modalCtrl.create(AttachmentPage);

    modal.present();

    modal.onDidDismiss(data => {
      if (data !== undefined && data.action == 'submit_image' && data.imageData !== undefined) 
      {
         this.imageData = data.imageData;
      }
    })
  }

  removeImage()
  {
    this.imageData = undefined;
  }

}
