import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, Slides, ViewController, Loading, LoadingController } from 'ionic-angular';
import { RestProvider } from '../../../providers/rest';
import { GlobalProvider } from '../../../providers/global';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile_edit.html',
})
export class ProfileEditPage {

  data:any = {};

  loading : Loading;
  retrySubs:any;

  spinner:boolean = false;
  errorGetActivity:boolean = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public rest : RestProvider,
    public modalCtrl: ModalController,
    public global: GlobalProvider,
    public viewCtrl: ViewController,
    public loadingCtrl : LoadingController,

  ) {
    this.getMyProfile();
    this.retrySubs = this.global.retryEmitter.subscribe(data => {
      if (data == 'profile_edit') 
      {
        this.getMyProfile();
      }
    })
  }

  getMyProfile()
  {
    this.errorGetActivity = false;
    this.spinner = true;
    this.rest.getProfile()
    .then(result => {
      this.spinner = false;
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        window.localStorage.setItem("profile_data", JSON.stringify(result['data']));
        this.data.usr_address   = result['data']['usr_address'];
        this.data.usr_email     = result['data']['usr_email'];
        this.data.usr_fullname  = result['data']['usr_fullname'];
        this.data.usr_phone     = result['data']['usr_phone'];
      }
      else
      {
        this.errorGetActivity = true;
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
  }

  saveProfile()
  {
    this.loading = this.loadingCtrl.create({
      content : "Menyimpan..."
    });

    this.loading.present();
    
    this.rest.updateProfile(this.data)
    .then(result => {
      this.loading.dismiss();
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.dismiss("saved");
      }
      else
      {
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
  }

  dismiss(data)
  {
    this.viewCtrl.dismiss(data);
  }

}
