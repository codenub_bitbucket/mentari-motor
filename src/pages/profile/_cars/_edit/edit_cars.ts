import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ModalController, Slides, ViewController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { RestProvider } from '../../../../providers/rest';
import { GlobalProvider } from '../../../../providers/global';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { SAutoCompletePage } from '../../../extra/s_autocomplete';

@Component({
  selector: 'page-profile',
  templateUrl: 'edit_cars.html',
})
export class CarsEditPage {

  fields:any = JSON.parse(window.localStorage.getItem("fields_vehicle"));

  @ViewChild('myForm') myForm: ElementRef;
  createForm:FormGroup;
  formFields:any            = {};
  required:any              = [];

  
  data_uuid:string="";

  content:any= new Array();

  data:any = {};
  data_label:any                  = {};

  spinner:boolean = true;
  errorRequest:boolean = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public rest : RestProvider,
    public modalCtrl: ModalController,
    public global: GlobalProvider,
    public viewCtrl: ViewController,
    public builder            : FormBuilder,

  ) {

    this.data_uuid = this.navParams.get("data_uuid");

    this.detailCars();
    this.validateDefined();
    this.getForm();
    this.global.retryEmitter.subscribe(data => {
      if (data == 'edit_vars') 
      {
        this.detailCars();
        this.validateDefined();
        this.getForm();
      }
    })
  }

  ionViewDidEnter()
  {
    
  }

  ionViewWillLeave()
  {
  }

  validateDefined()
  {
    var fields = this.fields;
    var fieldName: string = "";
    var fieldValidation: string = "";

    if (fields != undefined)
    {
      for(let row of fields)
      {
        if (typeof row['cst_crs_f_name'] != "undefined" || row['cst_crs_f_name'] != null)
        {
         fieldName         = row['cst_crs_f_name'];
         fieldValidation   = row['cst_crs_f_validation'];
        }

        this.required = [];

        let temp_validation = [];
        if ( fieldValidation != null && fieldValidation.indexOf("required") > -1 ) {
          temp_validation.push(Validators.required);
        }
        let pattern = this.global.validatorPattern(fieldValidation);
        if( pattern != false ) {
          temp_validation.push(Validators.pattern(pattern.regex));
        }
        this.required = ['', Validators.compose(temp_validation)];

        this.formFields[fieldName] = this.required;
      }
    }
    this.createForm = this.builder.group(this.formFields);
  }

  detailCars()
  {
    this.rest.carsEdit(this.data_uuid)
    .then(result => {
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.content = result['data'];
        this.carsEditData(this.content);
      }
      else
      {
        this.spinner = false;
        this.global.toastDefault(this.global.config("errorMsg"));
        this.errorRequest = true;
      }
    })
  }

  carsEditData(content:any)
  {
    if (content.length > 0) 
    {
      content.forEach((item) => {
        if (item['cst_crs_f_name'] == 'crs_brand_id') 
        {
          this.data_label[item['cst_crs_f_name']+'_label'] = item['content']['crs_brand_name'];
          this.data[item['cst_crs_f_name']] = item['content']['crs_brand_id'];
        }
        else if (item['cst_crs_f_name'] == 'crs_model_id') 
        {
          this.data_label[item['cst_crs_f_name']+'_label'] = item['content']['crs_model_name'];
          this.data[item['cst_crs_f_name']] = item['content']['crs_model_id'];
        }
        else
        {
          this.data[item['cst_crs_f_name']] = item['content'];
        }
      });  
    }
  }

  getForm()
  {
    this.rest.getFormVehicle()
    .then(result => {
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.spinner = false;
        this.fields = result['data'];
        this.validateDefined();
      }
      else
      {
        this.spinner = false;
        this.global.toastDefault(this.global.config("errorMsg"));
        this.errorRequest = true;
      }
    })
  }

  sAutoComplete(fields_name, fields_label)
  {
    let params:any = {
      fields_name   : fields_name,
      fields_label  : fields_label, 
    }

    if (fields_name == 'crs_model_id') 
    {
      params.brand_name = this.data_label['crs_brand_id_label'];  
      params.brand_id = this.data['crs_brand_id'];  
    }

    let modal = this.modalCtrl.create(SAutoCompletePage, params);

    modal.present();

    modal.onDidDismiss(data => {
      if (fields_name == 'crs_brand_id') 
      {
        this.data.crs_brand_id = data.value;  
        this.data_label.crs_brand_id_label = data.label;  

        this.data.crs_model_id = null;  
        this.data_label.crs_model_id_label = "";  
      }
      if (fields_name == 'crs_model_id') 
      {
        this.data.crs_model_id = data.value;  
        this.data_label.crs_model_id_label = data.label;  
      }
    })
  }

  storeData()
  {
    console.log(this.data);
    this.rest.saveEditVehicle(this.data, this.data_uuid)
    .then(result => {
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.dismiss('saved');
      }
      else
      {
        this.global.toastDefault("Kesalahan Server");
      }
    })
    
  }

  dismiss(action="cancel")
  {
    this.viewCtrl.dismiss(action);
  }

  
}
