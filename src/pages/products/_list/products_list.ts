import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { ProductsDetailPage } from '../_detail/detail';
import { RestProvider } from '../../../providers/rest';
import { GlobalProvider } from '../../../providers/global';

@Component({
  selector: 'page-products',
  templateUrl: 'products_list.html',
})
export class ProductsListPage {

  criteria:any = {
    show : this.global.config("showPerPage"),
    order_by: 'created_at',
    type_sort: 'DESC',
    page: 1,
  }

  list_data:any = new Array();
  total_products:number = 0;

  nameHeader:string="";
  view:string = 'list';

  group_id:number = 1; // 1 sparepart 2 asesoris 

  retrySubs:any;

  spinner:boolean = false;
  errorGetProducts:boolean = false;

  constructor(
    public navCtrl:       NavController, 
    public navParams:     NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public rest : RestProvider,
    public global : GlobalProvider,
  ) {
    this.nameHeader = this.navParams.get("list_type");
    this.getProducts();
  }

  getProducts(refresher:any=undefined)
  {
    this.errorGetProducts = false;
    this.spinner = true;
    this.rest.products(this.criteria, this.group_id)
    .then(result => {
      this.spinner = false;
      if (refresher !== undefined) 
      {
        refresher.complete();  
      }
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.list_data = result['data']['products']['data'];
        this.total_products = result['data']['products']['total_data'];
      }
      else if(typeof result['status'] !== undefined && result['status'] == 401)
      {
        window.localStorage.clear();
        this.global.isLogin = false;
      }
      else
      {
        this.errorGetProducts = true;
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
  }

  doRefresh(refresher)
  {
    this.getProducts(refresher);
  }

  detailProducts()
  {
    let modal = this.modalCtrl.create(ProductsDetailPage);
    modal.present()
    modal.onDidDismiss(data => {
      
    })
  }

  changeView(view)
  {
    this.view = view;
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

}
