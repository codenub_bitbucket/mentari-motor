import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController, LoadingController, Loading } from 'ionic-angular';
import { RestProvider } from '../../providers/rest';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalProvider } from '../../providers/global';
import { SAutoCompletePage } from './s_autocomplete';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import moment from 'moment';
import { File } from '@ionic-native/file';


declare var cordova:any;

@Component({
  selector: 'page-extra',
  templateUrl: 'attachments.html',
})
export class AttachmentPage {

  imageData:any = undefined;
  win: any = window;

  constructor(
    public navCtrl      : NavController,
    public navParams    : NavParams,
    public viewCtrl     : ViewController,
    public rest         : RestProvider,
    public builder      : FormBuilder,
    public global       : GlobalProvider,
    public modalCtrl    : ModalController,
    public loadingCtrl  : LoadingController,
    public camera       : Camera,
    public filePath     : FilePath,
    public file         : File,

  ) {
    
  }

  takePicture(sourceType)
  {
    const options: CameraOptions = {
      quality: 100,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType : this.camera.PictureSourceType[sourceType],
      correctOrientation : true,
      destinationType: this.camera.DestinationType.DATA_URL,
    }

    console.log(options);
    

    this.getPictureAndroid(options);
  }

  getPictureAndroid(options)
  {
    return new Promise((resolve, reject) => {
      this.camera.getPicture(options).then((imageData) => {
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        var imageType    = "jpg";
        var imageName    = this.createFileName(imageType);
        var result = {
          base64Image     : base64Image,
          imageName       : imageName,
          imageType       : imageType,
          date            : moment().utc(false).format("YYYY-MM-DD HH:mm"),
        }

        this.imageData = result;
        resolve(true);
        // console.log('masuk 1', imagePath);
        
        // imagePath is either a base64 encoded string or a file URI
        // this.filePath.resolveNativePath(imagePath)
        // .then(filePath =>
        // {
        //   console.log('masuk 2', filePath);
        //   var imageType    = "jpg";
        //   var imageName    = this.createFileName(imageType);

        //   let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
        //   let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1);

        //   let temp = this.win.Ionic.WebView.convertFileSrc(filePath);

        //   this.file.copyFile(correctPath, currentName, cordova.file.dataDirectory, imageName)
        //   .then(success => {
        //     console.log("copyGaleryToLocalDir",success);

        //     var base64Image  = cordova.file.dataDirectory + imageName;

        //     var result = {
        //       base64Image     : temp,
        //       imageName       : imageName,
        //       imageType       : imageType,
        //       imageUrl        : base64Image,
        //       date            : moment().utc(false).format("YYYY-MM-DD HH:mm"),
        //     }
  
        //     this.imageData = result;
        //     resolve(true);
        //   }, error => {
        //     console.log(error);
        //   });
        // });
      }, (err) => {
      // Handle error
      });
    })
  }

  createFileName(type:any="jpg")
  {
    if(type == "jpeg" || type == "png")
    {
      type = "jpg";
    }

    let n           = "CHEVYCARE-" + moment().utc(false).format("YYYYMMDDHHmm")
    let newFileName =  n + "." + type;
    return newFileName;
  }

  close(action="cancel")
  {
    let data:any = {};
    data.action = action;
    if (action == 'submit_image') 
    {
      data.imageData = this.imageData;
    }

    this.viewCtrl.dismiss(data);
  }

}
