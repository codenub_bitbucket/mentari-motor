import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController, Slides } from 'ionic-angular';
import { WorkshopServicePage } from '../workshop-service/workshop-service';
import { HomeServicePage } from '../home-service/home-service';
import { GlobalProvider } from '../../providers/global';

@Component({
  selector: 'page-booking-service',
  templateUrl: 'booking-service.html',
})
export class BookingServicePage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public global : GlobalProvider
  ) {
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

  workShopService()
  {
    let modal = this.modalCtrl.create(WorkshopServicePage);

    modal.present();

    modal.onDidDismiss(data => {

    })
  }

  homeService()
  {
    let modal = this.modalCtrl.create(HomeServicePage);

    modal.present();

    modal.onDidDismiss(data => {

    })
  }



}
