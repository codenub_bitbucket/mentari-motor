import { Component, Input } from '@angular/core';

@Component({
  selector: 'towing-list',
  templateUrl: 'towing-list.html'
})
export class TowingListComponent {

  @Input('moduleClass') moduleClass: any;

  towingList:any = [
    {
      name : 'Djava Towing',
      number : '081929414111',
      area : 'Jakarta'
    },
    {
      name : 'Ade derek',
      number : '082315890768',
      area : 'Bandung'
    },
    {
      name : 'Agus Derek Solo',
      number : '081329939263',
      area : 'Solo'
    },
    {
      name : 'Adi Brata Towing',
      number : '081931777727',
      area : 'Yogyakarta'
    },
    {
      name : 'Pak Sofyan Derek',
      number : '082112218482',
      area : 'Jatim Malang'
    },
    {
      name : 'Angkasa Towing',
      number : '085322913319',
      area : 'Cirebon'
    },
  ]

  constructor() {
  }

  formatPhone(item)
  {
    let phone = "+" + item.split()[0].replace("0", "62").toString();
    console.log(phone);
    
    return phone = phone.slice(0, 4) + " " + phone.slice(4, 8)  + " " + phone.slice(8, 12)  + " " + phone.slice(12, 16) 
  }

  callTowing(data)
  {
    this.moduleClass.data.towing_number = data.towing_number;
    this.moduleClass.data.towing_name = data.towing_name;
    this.moduleClass.alreadyCall = true;
    this.moduleClass.submitOrderTowing();
  }

}
