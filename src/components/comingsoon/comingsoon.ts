import { Component } from '@angular/core';

/**
 * Generated class for the ComingsoonComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'comingsoon',
  templateUrl: 'comingsoon.html'
})
export class ComingsoonComponent {

  text: string;

  constructor() {
    console.log('Hello ComingsoonComponent Component');
    this.text = 'Hello World';
  }

}
