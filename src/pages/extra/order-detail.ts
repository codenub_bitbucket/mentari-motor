import { Component, ElementRef, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, Platform, ModalController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global';
import { RestProvider } from '../../providers/rest';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { PaymentsDPPage } from './payments_dp';
import moment from 'moment';
import { PaymentPage } from '../payment/payment';
import { Socket } from 'ng-socket-io';
import { DomSanitizer } from '@angular/platform-browser';
import { AppAvailability } from '@ionic-native/app-availability';

declare var startApp:any;

@Component({
  selector: 'page-extra',
  templateUrl: 'order-detail.html',
})
export class OrderDetailPage {

  users_id : any = JSON.parse(window.localStorage.getItem('users_id'));

  data_uuid : string = "";

  content:any = new Array();
  retrySubs:any;

  spinner:boolean = false;
  errorGetTrx:boolean = false;

  expiredOrder:boolean = false;

  serviceBulkList:any= [];
  serviceRegulerList:any= [];
  ProductList:any= [];

  /* Gopay Payment Variable */
  QRUrl:string = "";
  DeeplinkGojek:any;
  showQR:boolean = false;
  
  /* Mandiri Bill / echannel */
  echannel:any = {};

  orderReciever:any;

  constructor(
    public navCtrl:       NavController, 
    public navParams:     NavParams,
    public viewCtrl: ViewController,
    public global: GlobalProvider,
    public rest : RestProvider,
    private iab: InAppBrowser,
    public platform         : Platform,
    public modalCtrl  : ModalController,
    public socket              : Socket,
    private sanitizer: DomSanitizer,
    private appAvailability: AppAvailability
  ) {
    this.data_uuid = this.navParams.get("data_uuid");

    this.getTransaction();
    this.retrySubs = this.global.retryEmitter.subscribe(data => {
      if (data == 'order_detail') 
      {
        this.getTransaction();
      }
    })
  }

  ionViewWillEnter()
  {
    this.orderReciever = this.global.orderReciever.subscribe(res => {
      let data = res.data;
      let to_user = res.to_user;

      if (to_user == this.users_id) 
      {
        if (data.srv_t_status == 3 && data.srv_t_uuid == this.data_uuid) 
        {
          this.global.toastDefault("Pembayaran Telah Diterima");
          this.getTransaction();
        }
      }

    })
  }

  getTransaction(refresher = undefined)
  {
    this.errorGetTrx = false;
    this.spinner = true;
    this.rest.transactionDetail(this.data_uuid)
    .then(result => {
      this.spinner = false;
      if (refresher !== undefined) 
      {
        refresher.complete();
      }
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.content = result['data'];
        this.content.pm_origin_data = JSON.parse(this.content.pm_origin_data);
        console.log(this.content);
        if (this.content.pm_type == 'gopay' && this.content.srv_t_status == 0) 
        {
          try {
            this.content.pm_origin_data.actions = JSON.parse(this.content.pm_origin_data.actions);
            console.log("🚀 ~ file: order-detail.ts ~ line 101 ~ OrderDetailPage ~ this.content.pm_origin_data.actions", this.content.pm_origin_data.actions)
            this.QRUrl = this.content.pm_origin_data.actions.find(r => r['name'] == 'generate-qr-code')['url'];
            // this.DeeplinkGojek = this.sanitizer.bypassSecurityTrustUrl(this.content.pm_origin_data.actions.find(r => r['name'] == 'deeplink-redirect')['url']);
            this.DeeplinkGojek = this.content.pm_origin_data.actions.find(r => r['name'] == 'deeplink-redirect')['url'];
          } catch (error) {
            console.log(error);
          }
        }

        if(this.content.pm_type == 'echannel' && this.content.srv_t_status == 0)
        {
          try {
            this.echannel = JSON.parse(this.content.pm_origin_data.echannel);
            console.log("🚀 ~ file: order-detail.ts ~ line 122 ~ OrderDetailPage ~ echannel", this.echannel)
          } catch (error) {
            console.log(error);
          }
        }
        

        /* 
          * type 1 : Reguler
          * type 2 : Bulk
          * type 3 : Product
        */
        if (this.content.srv_t_r.length > 0) 
        {
          this.content.srv_t_r.forEach(item => {
            if (item['srv_t_d_type'] == 1) 
            {
              this.serviceRegulerList.push(item);
            }
            else if (item['srv_t_d_type'] == 2) 
            {
              this.serviceBulkList.push(item);
            }
            else if (item['srv_t_d_type'] == 3) 
            {
              this.ProductList.push(item);
            }
          });  
        }
      }
      else
      {
        this.errorGetTrx = true;
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
  }

  doRefresh(refresher)
  {
    this.serviceBulkList = [];
    this.serviceRegulerList = [];
    this.ProductList = [];
    this.getTransaction(refresher);
  }

  ionViewWillLeave()
  {
    try {
      this.retrySubs.unsubscribe();
      this.orderReciever.unsubscribe();
    } catch (error) {
      console.log(error);
    }
  }

  calculateTime(time)
  {
    let days = "0";
    let hours = "0";
    let minutes = "0";
    if(time !== '' && time !== null && time !== undefined)
    {
      let temp_hour = time.split(":")[0];
      if (parseInt(temp_hour) > 24) 
      {
        days = (Math.floor(temp_hour / 24)).toString();
        hours = ((parseInt(days) > 0) ? (parseInt(temp_hour) - (parseInt(days) * 24)) : parseInt(temp_hour)).toString();
        minutes = time.split(":")[1];
      }
      else
      {
        hours = parseInt(temp_hour).toString();
        minutes = time.split(":")[1];
      }
    }
    return `${days} Hari, ${hours} Jam, ${minutes} Menit`;
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

  // generateDetailVA(content)
  // {
  //   let result = '';

  //   let temp = JSON.parse(content);
    
  //   if (temp['va_numbers'].length > 0) 
  //   {
  //     temp['va_numbers'].forEach(item => {
  //       result += `<h6 class="mb-0 mt-0"><b>Bank : </b>${(item['bank']).toUpperCase()}</h6>
  //                   <h6 (click)="global.copyData(${item['va_number']})" class="mb-0 mt-0"><b>Virtual Account : </b>${item['va_number']} <i class='bx bx-copy'></i></h6>`;  
  //     });
  //   }

  //   return result;
  // }

  continueDP()
  {
    if (!this.checkExpiredPaymentRequest()) 
    {
      let itemOrder = {
        transaction_details : {
          "order_id": this.content.srv_t_code,
          "gross_amount": this.content.srv_t_dp
        },
        item_details : [{
          "id": this.content.srv_t_code,
          "price": this.content.srv_t_dp,
          "name": "Booking Fee Servis "+this.content.srv_t_code,
          "quantity": 1
        }],
        customer_details : {
          "first_name": this.content.usr_fullname,
          "email": this.content.usr_email,
          "phone": this.content.usr_phone,
        },
        uuid : this.content.srv_t_uuid,
      };
      let myModal = this.modalCtrl.create(PaymentsDPPage, {data : itemOrder, data_service : this.content});
  
      myModal.present();
  
      myModal.onDidDismiss(data => {
        this.serviceBulkList = [];
        this.serviceRegulerList = [];
        this.ProductList = [];
        this.getTransaction();
      });
    }
  }

  checkExpiredPaymentRequest()
  {
    this.expiredOrder = false; 
    var now = moment(new Date());
    var end = moment(this.content.srv_t_created_at).add(7, "hours");
    var duration = moment.duration(now.diff(end));
    var hours = duration.asHours();

    if (hours > parseInt(this.global.config("maxPayTime"))) 
    {
      this.expiredOrder = true;
      this.global.toastDanger("Pesanan Sudah Melebihi Batas Maksimal Pembayaran Booking Fee. (max "+ this.global.config("maxPayTime") +" jam)");
    }

    return this.expiredOrder;
  }

  openGojek()
  {
    console.log(this.DeeplinkGojek);

    let app;

    if (this.platform.is('ios')) {
      app = 'gojek://';
    } else if (this.platform.is('android')) {
      app = 'com.gojek.app';
    }

    this.appAvailability.check(app)
    .then(
      (yes: boolean) => {
        this.iab.create(this.DeeplinkGojek, '_system')
      },
      (no: boolean) => {
        this.iab.create("https://play.google.com/store/apps/details?id=com.gojek.app&hl=in&gl=US", '_system')
      }
    );
    
    // startApp.set({ /* params */
    //     "action": "ACTION_VIEW",
    //     "uri": "gojek://gopay/merchanttransfer?tref=0120210507172014MIQ9zRooUpID&amount=100&activity=GP:RR&callback_url=chevycare%3A%2F%2Fpayment_success%2F7e52ca9e-c43f-4337-80ab-d488d323a7a8%3Forder_id%3DINV-10705212052228501"
    // }).start();

    // var sApp = startApp.set({ /* params */
    //   "action":"ACTION_VIEW",
    //   "uri": this.DeeplinkGojek
    // });
    // sApp.start(function() { /* success */
    //   console.log("OK");
    // }, function(error) { /* fail */
    //   alert(error);
    // }, function() { // optional broadcast and forResult callback
    //   console.log(arguments);
    // });
  }

  continuePayment()
  {
    if (!this.checkExpiredPaymentRequest()) 
    {
      let itemOrder = {
        transaction_details : {
          "order_id": this.content.srv_t_code,
          "gross_amount": this.content.srv_t_dp
        },
        item_details : [{
          "id": this.content.srv_t_code,
          "price": this.content.srv_t_dp,
          "name": "Booking Fee Servis "+this.content.srv_t_code,
          "quantity": 1
        }],
        customer_details : {
          "first_name": this.content.usr_fullname,
          "email": this.content.usr_email,
          "phone": this.content.usr_phone,
        },
      };

      let myModal = this.modalCtrl.create(PaymentPage, {data : itemOrder, data_service : this.content});
  
      myModal.present();
  
      myModal.onDidDismiss(data => {
        this.serviceBulkList = [];
        this.serviceRegulerList = [];
        this.ProductList = [];
        this.getTransaction();
      });

    }
  }

  scrollToQR()
  {
    if (this.showQR) 
    {
      setTimeout(() => {
        let todayItem = document.getElementById('qrimage');
        todayItem.scrollIntoView(true);
      }, 500);  
    }
  }

  cancel()
  {
    this.rest.cancelOrder(this.data_uuid)
    .then((result) => {
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.global.toastDefault(result['msg']);
      }
      else
      {
        this.errorGetTrx = true;
        this.global.toastDefault(this.global.config("errorMsg"))
      }
    })
  }

}
