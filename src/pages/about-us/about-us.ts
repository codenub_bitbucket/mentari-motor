import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-about-us',
  templateUrl: 'about-us.html',
})
export class AboutUsPage {

  constructor(
    public navCtrl:       NavController, 
    public navParams:     NavParams,
    public viewCtrl: ViewController,

  ) {
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

}
