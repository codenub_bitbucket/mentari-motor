import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-extra',
  templateUrl: 'cart.html',
})
export class CartPage {

  constructor(
    public navCtrl:       NavController, 
    public navParams:     NavParams,
    public viewCtrl: ViewController,

  ) {
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

}
