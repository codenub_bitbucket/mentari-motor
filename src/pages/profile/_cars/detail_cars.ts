import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, Slides, ViewController, AlertController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { RestProvider } from '../../../providers/rest';
import { GlobalProvider } from '../../../providers/global';
import { CarsEditPage } from './_edit/edit_cars';
import { AttachmentPage } from '../../extra/attachments';
import { ActionSheet, ActionSheetOptions } from '@ionic-native/action-sheet';
import { PhotoViewer } from '@ionic-native/photo-viewer';


@Component({
  selector: 'page-profile',
  templateUrl: 'detail_cars.html',
})
export class DetailCarsPage {

  data_uuid:string="";

  content:any= new Array();

  cars_brand:string = "";
  cars_model:string = "";

  spinner:boolean = true;
  errorRequest:boolean = false;
  fields:any = JSON.parse(window.localStorage.getItem("fields_vehicle"));

  imageCarsUpdated:Subscription;

  loadingImage:boolean = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public rest : RestProvider,
    public modalCtrl: ModalController,
    public global: GlobalProvider,
    public viewCtrl: ViewController,
    public actionSheet: ActionSheet,
    public photoViewer: PhotoViewer,
    public alertCtrl : AlertController,
  ) {

    this.data_uuid = this.navParams.get("data_uuid");

    this.detailCars();
    this.global.retryEmitter.subscribe(data => {
      if (data == 'detail_cars') 
      {
        this.detailCars()
      }
    })

    this.imageCarsUpdated = this.global.imageUpdated.subscribe(r => {
      if(r['uuid'] == this.content.cst_crs_uuid)
      {
        this.content.cst_crs_photo = r['url'];
      }
    })
  }

  ionViewDidEnter()
  {
    
  }

  ionViewWillLeave()
  {
  }

  detailCars()
  {
    this.spinner = true;
    this.errorRequest = false;
    this.rest.getCarsDetail(this.data_uuid)
    .then(result => {
      this.spinner = false;
      if (typeof result['status'] !== undefined && result['status'] == 200) 
      {
        this.content = result['data'];
        this.fields = result['form'];

        // /* Find Brand */
        // let brand = this.content.findIndex(r=>r['cst_crs_f_name'] == 'crs_brand_id');
        // if (brand >= 0) 
        // {
        //   this.cars_brand = this.content[brand]['content']['crs_brand_name']
        // }
        // let model = this.content.findIndex(r=>r['cst_crs_f_name'] == 'crs_model_id');
        // if (model >= 0) 
        // {
        //   this.cars_model = this.content[model]['content']['crs_model_transmission'] + ' ' + this.content[model]['content']['crs_model_type'];
        // }
      }
      else
      {
        this.global.toastDefault(this.global.config("errorMsg"));
        this.errorRequest = true;
      }
    })
  }

  dismiss(data:any=undefined)
  {
    this.viewCtrl.dismiss(data);
  }

  edit(data_uuid)
  {
    let modal = this.modalCtrl.create(CarsEditPage, {data_uuid: data_uuid});
    modal.present();
    modal.onDidDismiss(data => {

      if (data == 'saved') 
      {
        this.detailCars();
      }
    });
  }

  deleteCar(data_uuid)
  {
    this.alertCtrl.create({
      message : "Hapus Mobil?",
      buttons : [
        {
          text : "Batal",
          role : "cancel",
        },
        {
          text : "Hapus",
          handler : () => {
            this.rest.deleteCars(this.content.cst_crs_uuid)
            .then(rest => {
              if (typeof rest['status'] !== undefined && rest['status'] == 200) 
              {
                this.global.toastDefault("Mobil Berhasil Di Hapus");
                this.dismiss("deleted");
              }
              else
              {
                this.global.toastDanger("Mobil Gagal Di Hapus!");
              }
            })
          }
        }
      ]
    }).present();
  }

  addImage()
  {
    let modal = this.modalCtrl.create(AttachmentPage);

    modal.present();

    modal.onDidDismiss(data => {
      if (data !== undefined && data.action == 'submit_image' && data.imageData !== undefined) 
      {
        this.loadingImage = true;
        this.rest.uploadImage(data.imageData)
        .then(res => {
          let cont = {
            cst_crs_photo : res['file'],
          }
          this.rest.saveEditVehicle(cont, this.content.cst_crs_uuid)
          .then(rest => {
            this.loadingImage = false;

            if (typeof rest['status'] !== undefined && rest['status'] == 200) 
            {
              this.global.toastDefault("Gambar Mobil Berhasil Di Upload");
              let data = {
                url : res['file'],
                uuid : this.content.cst_crs_uuid
              }
              this.global.updateImageDirect(data);
            }
            else
            {
              this.global.toastDanger("Gambar Mobil Gagal Di Upload!");
            }
          })
        }); 
      }
    })
  }

  viewOption()
  {
    let buttonLabels = ['Ganti Foto', 'Lihat Foto'];

    const options: ActionSheetOptions = {
      buttonLabels: buttonLabels,
      addCancelButtonWithLabel: 'Cancel',
      destructiveButtonLast: true
    };

    this.actionSheet.show(options).then((buttonIndex: number) => {
      if (buttonIndex == 1) 
      {
        this.addImage();  
      }
      else if(buttonIndex == 2)
      {
        let imgName = this.content.cst_crs_photo.substring(this.content.cst_crs_photo.lastIndexOf('/') + 1)
        this.photoViewer.show(this.global.config('urlApi')+this.content.cst_crs_photo, imgName, {share: false});
      }
    });
  }

  
}
