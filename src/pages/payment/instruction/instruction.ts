import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { IonicPage, LoadingController, NavController, NavParams, ViewController } from 'ionic-angular';
import { Socket } from 'ng-socket-io';
import { Subscription } from 'rxjs/Subscription';
import { GlobalProvider } from '../../../providers/global';
import { RestProvider } from '../../../providers/rest';

@Component({
  selector: 'page-instruction',
  templateUrl: 'instruction.html',
})
export class InstructionPage {

  dataPayment:any = {};
  
  constructor(
    public navCtrl      : NavController, 
    public navParams    : NavParams,
    public viewCtrl     : ViewController,
    public rest         : RestProvider,
    public global       : GlobalProvider,
    private iab: InAppBrowser,
    public loadingCtrl : LoadingController,
    public socket              : Socket,

  ) {
    this.dataPayment  = this.navParams.get("data");
  }

  ionViewWillLeave()
  {
    
  }

  dismiss() 
  {
    this.viewCtrl.dismiss();
  }

}
