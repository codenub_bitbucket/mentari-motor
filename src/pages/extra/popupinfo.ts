import { Component } from '@angular/core';
import { ModalController, NavController, NavParams, ViewController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest';
import { GlobalProvider } from '../../providers/global';
import { ProfilePage } from '../profile/profile';

declare var snap;

@Component({
  selector: 'page-extra',
  templateUrl: 'popupinfo.html',
})
export class PopupInfoPage {

  constructor(
    public navCtrl      : NavController, 
    public navParams    : NavParams,
    public viewCtrl     : ViewController,
    public rest         : RestProvider,
    public global       : GlobalProvider,
  ) {
    
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }

  daftar()
  {
    this.viewCtrl.dismiss("daftar");
  }

  
}
